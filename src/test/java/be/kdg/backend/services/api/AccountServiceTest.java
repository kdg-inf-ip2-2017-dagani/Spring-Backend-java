package be.kdg.backend.services.api;

import be.kdg.backend.dom.account.AccessLevel;
import be.kdg.backend.dom.account.Account;
import be.kdg.backend.services.exceptions.AccountServiceException;
import be.kdg.backend.services.exceptions.AuthenticationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountServiceTest {

    @Value("testHost@gmail.com")
    private String realHostEmailAddress;

    @Value("testAdmin@gmail.com")
    private String realAdminEmailAddress;

    @Value("fake@l.c")
    private String fakeEmailAddress;

    @Value("testHost1")
    private String realHostUsername;

    @Value("test")
    private String password;

    @Autowired
    private AccountService accountService;

    @Test
    public void contextLoads() {
    }

    @Before
    public void setup() {
        accountService.addAccount(new Account(realHostUsername, password, realHostEmailAddress, AccessLevel.HOST));
        accountService.addAccount(new Account("testAdmin2", password, realAdminEmailAddress, AccessLevel.ADMIN));
    }

    @After
    public void clean() {
        /*accountService.deleteAccountByEmailAddress("testHost@gmail.com");
        accountService.deleteAccountByEmailAddress("testAdmin@gmail.com");
        accountService.deleteAccountByEmailAddress("jos@gmail.com");*/
        accountService.removeAllAccounts();
    }

    @Test
    public void testAddAccount() {
        Account account = new Account("Jos", "test", "jos@gmail.com", AccessLevel.HOST);
        accountService.addAccount(account);
        Account accountJos = accountService.findAccountByEmailAddress("jos@gmail.com");
        assertThat(accountJos, equalTo(account));
    }

    @Test
    public void testFindAccountByEmailAddress() {
        Account account = accountService.findAccountByEmailAddress(realHostEmailAddress);
        assertThat(account, notNullValue());
    }

    @Test
    public void testFindAccountByUsername() {
        Account account = accountService.findAccountByUsername(realHostUsername);
        assertThat(account, notNullValue());
    }

    @Test(expected = AccountServiceException.class)
    public void testFindFakeAccount() {
        Account account = accountService.findAccountByEmailAddress(fakeEmailAddress);
    }

    @Test(expected = AccountServiceException.class)
    public void testDeleteAccountByEmailAddress() {
        accountService.deleteAccountByEmailAddress(realHostEmailAddress);
        Account account = accountService.findAccountByEmailAddress(realHostEmailAddress);
    }

    @Test
    public void testUpdateAccessLevel() {
        Account account = accountService.findAccountByEmailAddress(realHostEmailAddress);
        accountService.updateAccessLevel(account.getId(), AccessLevel.ADMIN);
        Account updatedAccount = accountService.findAccountByEmailAddress(realHostEmailAddress);
        assertThat(updatedAccount.getAccessLevel(), equalTo(AccessLevel.ADMIN));
    }

    @Test
    public void testFindAccounts() {
        List<Account> accounts = accountService.findAccounts();
        assertThat(accounts, is(not(empty())));
    }

    @Test(expected = AuthenticationException.class)
    public void testCheckLogin() {
        Account account = accountService.findAccountByEmailAddress(realHostEmailAddress);
        accountService.checkLogin(account.getId(), "test");
        accountService.checkLogin(account.getId(), "wrong");
    }

    @Test(expected = AccountServiceException.class)
    public void testAddAccountWithExistingEmailAddress() {
        Account account = new Account("testHost2", "test", realHostEmailAddress, AccessLevel.HOST);
        accountService.addAccount(account);
    }

    @Test(expected = AccountServiceException.class)
    public void testAddAccountWithExistingUsername() {
        Account account = new Account(realHostUsername, "test", "testje@gmail.com", AccessLevel.HOST);
        accountService.addAccount(account);
    }
}
