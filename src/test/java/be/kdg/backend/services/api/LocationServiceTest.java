package be.kdg.backend.services.api;

import be.kdg.backend.dom.location.Point;
import be.kdg.backend.dom.location.TradePost;
import be.kdg.backend.services.exceptions.LocationNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.api
 *
 * @author Gaston
 * @version 1.0
 * @since 9/02/2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class LocationServiceTest {
    @Autowired
    private LocationService locationService;
    @Before
    public void setup(){

    }

    @Test
    public void testTrue(){
        assertTrue(true);
    }
/*
    @Test
    public void addAreaLocation() throws Exception {
        Point[] points = new Point[]{
                new Point(20.0, 20.0, 0),
                new Point(21.0, 52.0, 1),
                new Point(24.1, 23.0, 2)
        };
        String loc = locationService.addAreaLocation("TEST AREA", points, AreaType.DISTRICT_A);
        assertNotNull("Should return an ID string", loc);
    }

    @Test
    public void findAreaByID() throws Exception {
        String id = "589dbca9caf67642d30e5156";
        AreaLocation al = locationService.findAreaByID(id);
        assertNotNull("Area should be found", al);
    }

    @Test
    public void findAreas() throws Exception {
        AreaLocation[] locs = locationService.findAreas();
        assertNotNull(locs);
        assertNotEquals(0, locs.length);
    }

    @Test
    public void updateAreaByID() throws Exception {
        String id = "589dbca9caf67642d30e5156";
        AreaLocation loc = locationService.findAreaByID(id);
        assertNotNull(loc);
        locationService.updateAreaByID(id, "2A", loc.getPoints(), loc.getType());
        loc = locationService.findAreaByID(id);
        assertNotNull(loc);
        assertEquals("2A", loc.getName());
    }

    @Test
    public void deleteAreaByID() throws Exception {
        Point[] points = new Point[]{
                new Point(20.0, 20.0, 0),
                new Point(21.0, 52.0, 1),
                new Point(24.1, 23.0, 2)
        };
        String id = locationService.addAreaLocation("TEST AREA", points, AreaType.DISTRICT_A);
        assertNotNull("Should return an ID string", id);
        locationService.deleteAreaByID(id);
        AreaLocation loc = locationService.findAreaByID(id);
        assertNull(loc);
    }

    @Test
    public void addPointLocation() throws Exception {

    }

    @Test
    public void findPointByID() throws Exception {

    }

    @Test
    public void updatePointByID() throws Exception {

    }

    @Test
    public void deletePointByID() throws Exception {

    }

    @Test
    public void findPoints() throws Exception {
        PointLocation[] locs = locationService.findPoints();
        assertNotNull(locs);
        assertNotEquals(0, locs.length);
    }

*/

   @Test
    public void addTradePost() {
        String tradPostId=locationService.addTradePost("tradePost999",new Point(20.0,20.0),new ArrayList<>(), "FLAVORTEXT");
        TradePost tradePost= locationService.findTradepostById(tradPostId);
        assertNotNull(tradePost);
    }
    @Test(expected = LocationNotFoundException.class)
    public void findNonExistentTradePostById() {
        TradePost tradePost= locationService.findTradepostById("nonExistentId");
    }
}