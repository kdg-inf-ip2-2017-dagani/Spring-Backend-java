package be.kdg.backend.services.api;

import be.kdg.backend.dom.account.AccessLevel;
import be.kdg.backend.dom.account.Account;
import be.kdg.backend.dom.game.Game;
import be.kdg.backend.dom.game.Item;
import be.kdg.backend.dom.game.Player;
import be.kdg.backend.dom.game.Team;
import be.kdg.backend.dom.location.AreaLocation;
import be.kdg.backend.dom.location.AreaType;
import be.kdg.backend.dom.location.Point;
import be.kdg.backend.dom.location.PointType;
import be.kdg.backend.persistence.api.GameRepository;
import be.kdg.backend.services.exceptions.DuplicateUserException;
import be.kdg.backend.services.exceptions.GameException;
import be.kdg.backend.services.exceptions.InvalidTokenException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


/**
 * Created by Nikita on 10/02/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class GameServiceTest {

    private static final double DOUBLE_DELTA = 0.000001;
    @Value("testHost@gmail.com")
    private String realHostEmailAddress;

    @Value("testAdmin@gmail.com")
    private String realAdminEmailAddress;

    @Value("fake@l.c")
    private String fakeEmailAddress;

    @Value("testHost1")
    private String realHostUsername;

    @Value("test")
    private String password;

    @Value("defaultGame")
    private String defaultGameName;

    @Value("pw")
    private String playerGamePW;
    @Value("pwHOST")
    private String hostGamePW;

    @Value("defaultClientID")
    private String defaultClientID;

    @Value("ClientID")
    private String clientID;

    @Value("ClientID007")
    private String clientID007;

    @Value("defaultClientName")
    private String defaultClientName;

    @Value("clientName")
    private String clientName;

    @Value("snoepgoed")
    private String itemName;

    private String game1, game2, districtId, hostToken, hostToken2, defaultGame, tradePostId;

    @Autowired
    private GameService gameService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private LocationService locationService;
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private RunningGameService runningGameService;

    @Before
    public void setup() {
        accountService.addAccount(new Account(realHostUsername, password, realHostEmailAddress, AccessLevel.HOST));
        accountService.addAccount(new Account("2" + realHostUsername, password, "2" + realHostEmailAddress, AccessLevel.HOST));
        accountService.addAccount(new Account("testAdmin2", password, realAdminEmailAddress, AccessLevel.ADMIN));
        hostToken = accountService.checkLogin(accountService.findAccountByEmailAddress(realHostEmailAddress).getId(), password);
        hostToken2 = accountService.checkLogin(accountService.findAccountByEmailAddress("2" + realHostEmailAddress).getId(), password);
        districtId = locationService.addAreaLocation(
                "A1",
                new Point[]{
                        new Point(20.0, 20.0, 0),
                        new Point(21.0, 52.0, 1),
                        new Point(24.1, 23.0, 2)
                },
                AreaType.DISTRICT_A
        );
        districtId = locationService.addAreaLocation(
                "A2",
                new Point[]{
                        new Point(20.0, 20.0, 0),
                        new Point(21.0, 52.0, 1),
                        new Point(24.1, 23.0, 2)
                },
                AreaType.DISTRICT_A
        );
        districtId = locationService.addAreaLocation(
                "A3",
                new Point[]{
                        new Point(20.0, 20.0, 0),
                        new Point(21.0, 52.0, 1),
                        new Point(24.1, 23.0, 2)
                },
                AreaType.DISTRICT_A
        );
        districtId = locationService.addAreaLocation(
                "A4",
                new Point[]{
                        new Point(20.0, 20.0, 0),
                        new Point(21.0, 52.0, 1),
                        new Point(24.1, 23.0, 2)
                },
                AreaType.DISTRICT_A
        );
        districtId = locationService.addAreaLocation(
                "A5",
                new Point[]{
                        new Point(20.0, 20.0, 0),
                        new Point(21.0, 52.0, 1),
                        new Point(24.1, 23.0, 2)
                },
                AreaType.DISTRICT_A
        );
        districtId = locationService.addAreaLocation(
                "A6",
                new Point[]{
                        new Point(20.0, 20.0, 0),
                        new Point(21.0, 52.0, 1),
                        new Point(24.1, 23.0, 2)
                },
                AreaType.DISTRICT_A
        );
        districtId = locationService.addAreaLocation(
                "A7",
                new Point[]{
                        new Point(20.0, 20.0, 0),
                        new Point(21.0, 52.0, 1),
                        new Point(24.1, 23.0, 2)
                },
                AreaType.DISTRICT_A
        );

        Game g = new Game(null, "defaultGame", 5, 2, playerGamePW);
        defaultGame = gameRepository.insert(g).getId();
        Account host = accountService.validateToken(hostToken);
        game1 = gameService.addGame(host, "TEST_ROOM", 2, 5, playerGamePW);
        gameService.registerUser(game1, clientID, defaultClientName, playerGamePW);
        gameService.registerUser(game1, clientID007, defaultClientName + "2", playerGamePW);


        Account host2 = accountService.validateToken(hostToken2);
        game2 = gameService.addGame(host2, "TEST_ROOM", 2, 5, playerGamePW);

        List<Item> items = new ArrayList<>();
        items.add(new Item(itemName, 10, 8, 13, 11));
        tradePostId = locationService.addTradePost("test", new Point(51.158810, 4.144397, PointType.TRADE_POST), items, "FLAVORTEXT");
        gameService.addTradePost(hostToken, game1, tradePostId);
    }

    @After
    public void clean() {
        /*accountService.deleteAccountByEmailAddress(realHostEmailAddress);
        accountService.deleteAccountByEmailAddress(realAdminEmailAddress);
        gameService.deleteGameByRoomName(defaultGameName);*/

        gameService.removeAllGames();
        runningGameService.clearRunningGames();
        gameService.clearStagedGames();
        accountService.removeAllAccounts();
        locationService.removeAllLocations();
    }

    @Test
    public void contextLoads() {
    }

    //################################################   ADD DISTRICT
    @Test
    public void addDistrictWithCorrectToken() {
        String id = gameService.addDistrict(hostToken, game1, districtId);
        assertNotNull(id);
        assertEquals(districtId, id);
        gameService.deleteDistrict(hostToken, game1, id);
    }

    @Test(expected = InvalidTokenException.class)
    public void addDistrictWithFalseToken() {
        String id = gameService.addDistrict("WrongToken", game1, districtId);
        assertEquals(districtId, id);
    }

    @Test(expected = GameException.class)
    public void addDistrictForNonExistentGame() {
        String id = gameService.addDistrict(hostToken, "panda", districtId);
        assertEquals(districtId, id);
    }

    @Test(expected = GameException.class)
    public void addDistrictForWrongGame() {
        String id = gameService.addDistrict(hostToken, game2, districtId);
        assertEquals(districtId, id);
    }

    @Test(expected = GameException.class)
    public void addDistrictWithWrongDistrict() {
        String id = gameService.addDistrict(hostToken, game1, "panda");
        assertEquals(districtId, id);
    }

    //################################################   ADD PLAYER PASSWORD
    @Test
    public void addPlayerPassword() {
        String pw = "123Piano";
        gameService.addPlayerPassword(hostToken, game1, pw);
        Game g = gameService.findGameById(game1);
        String updated = g.getPlayerPassword();
        assertEquals(pw, updated);
    }

    @Test(expected = InvalidTokenException.class)
    public void addPlayerPasswordWithFalseToken() {
        String pw = "123Piano";
        gameService.addPlayerPassword("wrongToken", game1, pw);
        Game g = gameService.findGameById(game1);
        String updated = g.getPlayerPassword();
        assertNotEquals(pw, updated);
    }

    @Test(expected = GameException.class)
    public void addPlayerPasswordForNonexistentGame() {
        String pw = "123Piano";
        gameService.addPlayerPassword(hostToken, "no game", pw);
        Game g = gameService.findGameById(game1);
        String updated = g.getPlayerPassword();
        assertNotEquals(pw, updated);
    }

    @Test(expected = GameException.class)
    public void addPlayerPasswordForWrongGame() {
        String pw = "1234Piano";
        gameService.addPlayerPassword(hostToken, game2, pw);
        Game g = gameService.findGameById(game1);
        String updated = g.getPlayerPassword();
        assertNotEquals(pw, updated);
    }

    //################################################   ADD HOST PASSWORD
    @Test
    public void addHostPassword() {
        String pw = "123Piano";
        gameService.addHostPassword(hostToken, game1, pw);
        Game g = gameService.findGameById(game1);
        String updated = g.getHostPassword();
        assertEquals(pw, updated);
    }

    @Test(expected = InvalidTokenException.class)
    public void addHostPasswordWithFalseToken() {
        String pw = "123Piano";
        gameService.addHostPassword("wrongToken", game1, pw);
        Game g = gameService.findGameById(game1);
        String updated = g.getHostPassword();
        assertNotEquals(pw, updated);
    }

    @Test(expected = GameException.class)
    public void addHostPasswordForNonexistentGame() {
        String pw = "123Piano";
        gameService.addHostPassword(hostToken, "no game", pw);
        Game g = gameService.findGameById(game1);
        String updated = g.getHostPassword();
        assertNotEquals(pw, updated);
    }

    @Test(expected = GameException.class)
    public void addHostPasswordForWrongGame() {
        String pw = "1234Piano";
        gameService.addHostPassword(hostToken, game2, pw);
        Game g = gameService.findGameById(game1);
        String updated = g.getHostPassword();
        assertNotEquals(pw, updated);
    }


    //################################################   LOAD DEFAULT GAME
    @Test
    public void loadDefaultGame() {
        Game game = gameService.findGameByRoomName(defaultGameName);
        assertNotNull(game);
        assertEquals(defaultGame, game.getId());
    }


    //################################################   ADD GAME
    @Test
    public void addGameWithCorrectToken() {
        Account host = accountService.validateToken(hostToken);
        String newGameID = gameService.addGame(host, "testRoom", 5, 3, "hostpw");
        Game g1 = gameService.findGameById(newGameID);
        Game g2 = gameService.findGameByRoomName("testRoom");
        assertNotNull(g1);
        assertNotNull(g2);
        assertEquals(newGameID, g2.getId());
        assertEquals("testRoom", g1.getRoomName());

        gameService.deleteGameById(hostToken, newGameID);
        Game g3 = gameService.findGameById(newGameID);
        assertNull(g3);
    }

    @Test
    public void addGameNonExistentTemplate() {
        gameService.removeAllGames();
        gameService.addGame(accountService.findAccountByUsername(realHostUsername), "test", 1, 1, "test");
    }

    @Test(expected = InvalidTokenException.class)
    public void addGameWithFalseToken() {
        Account host = accountService.validateToken("tokens en token tokens");
        String id = gameService.addGame(host, "room name", 1, 1, "boo");
        assertEquals(districtId, id);
    }

    @Test
    public void addGameWithEmptyPassword() {
        Account host = accountService.validateToken(hostToken);
        String id = gameService.addGame(host, "room name", 1, 1, "");
        assertNotNull(id);
    }

    @Test(expected = GameException.class)
    public void addGameWithSpacePassword() {
        throw new GameException("");
        /* todo consult check needed?
        Account host = accountService.validateToken(hostToken);
        String id = gameService.addGame(host, "room name", 1, 1, " ");
        assertEquals(districtId, id);
        */
    }

    //################################################   FIND AREA WITH LOCATION
    @Test
    public void testFindAreaLocationById() {
        AreaLocation areaLocation = locationService.findAreaLocationById(districtId);
        assertNotNull(areaLocation);
    }

    //################################################   SAVE GAME SETTINGS
    @Test
    public void saveGameSettings() {
        Game g = gameService.findGameById(game1);
        assertNotNull(g);
        g.setRoomName("EditedRoomName");
        gameService.saveGameSettings(hostToken, g);
        g = gameService.findGameById(game1);
        assertNotNull(g);
        assertEquals("EditedRoomName", g.getRoomName());
    }

    //################################################   START GAME
    @Test
    public void startGame() {
        long time = System.currentTimeMillis();
        gameService.startGame(hostToken, game1);
        Game g = gameService.findGameById(game1);
        long end = g.getStartTime() + g.getDuration().toMillis();
        assertNotNull(g);
        assertNotEquals(0, g.getEndTime());
        assertNotEquals(0, g.getStartTime());
        assertTrue(g.getEndTime() > g.getStartTime());
        assertTrue(g.getStartTime() >= time);
        assertTrue(g.getEndTime() == end);
    }

    @Test(expected = GameException.class)
    public void banPlayerWithCorrectToken() {
        Player player = gameService.findPlayerByClientId(game1, defaultClientID);
        assertNotNull(player);
        gameService.banPlayer(hostToken, player.getClientID(), game1);
        Player p = gameService.findPlayerByClientId(game1, defaultClientID);
    }

    @Test(expected = InvalidTokenException.class)
    public void banPlayerWithWrongToken() {
        Player player = gameService.findPlayerByClientId(game1, clientID);
        assertNotNull(player);
        gameService.banPlayer("wrongToken", player.getClientID(), game1);
    }

    @Test(expected = GameException.class)
    public void banNonExistentPlayer() {
        gameService.banPlayer(hostToken, "wrong", game1);
    }

    //################################################   REGISTER PLAYER
    @Test
    public void registerPlayer() {
        gameService.registerUser(game1, clientID + "2", clientName, playerGamePW);
        Game g = gameService.findGameById(game1);
        assertNotNull(g);
        Player p = null;
        for (Team team : g.getTeams()) {
            if (team.getPlayers().containsKey(clientID + "2")) {
                p = team.getPlayers().get(clientID + "2");
            }
        }
        assertNotNull(p);
        assertEquals(clientID + "2", p.getClientID());
    }

    @Test()
    public void registerDuplicatePlayer() {
        gameService.registerUser(game1, clientID, clientName, playerGamePW);
        //gameService.registerUser(game1, clientID, clientName + "2", "");
    }

    @Test(expected = GameException.class)
    public void registerPlayerForNonExistentGame() {
        gameService.registerUser("nope", clientID, clientName, playerGamePW);
    }

    //################################################   REGISTER HOST
    @Test
    public void registerHost() {
        gameService.registerUser(game1, clientID, clientName, hostGamePW);
        Game g = gameService.findGameById(game1);
        assertNotNull(g);
        Player p = null;
        for (Player player : g.getHosts()) {
            if (player.getClientID().equals(clientID)) {
                p = player;
                break;
            }
        }
        assertNotNull(p);
        assertEquals(clientID, p.getClientID());
    }


    @Test(expected = DuplicateUserException.class)
    public void registerDuplicateHost() {
        gameService.registerUser(game1, clientID, clientName, hostGamePW);
        gameService.registerUser(game1, clientID, clientName + "2", hostGamePW);
    }

    @Test(expected = GameException.class)
    public void registerHostForNonExistentGame() {
        gameService.registerUser("nope", clientID, clientName, hostGamePW);
    }

    //################################################   UNREGISTER PLAYER
    @Test
    public void unregisterPlayer() {
        //gameService.registerUser(game1, clientID, clientName, "");
        gameService.unregisterUser(game1, clientID);
        Game g = gameService.findGameById(game1);
        assertNotNull(g);
        Player p = null;
        for (Team team : g.getTeams()) {
            if (team.getPlayers().containsKey(clientID)) {
                p = team.getPlayers().get(clientID);
            }
        }
        assertNull(p);
    }

    @Test(expected = GameException.class)
    public void unregisterNonExistentPlayer() {
        gameService.unregisterUser(game1, "Does not exist");
    }

    @Test(expected = GameException.class)
    public void unregisterPlayerForNonExistentGame() {
        gameService.unregisterUser("nope", clientID);
    }



    @Test
    public void deleteGameById() {
        gameService.startGame(hostToken, game1);
        gameService.deleteGameById(hostToken, game1);
        assertEquals(gameService.findGameById(game1), null);
        Account account = accountService.findAccountByUsername(realHostUsername);
        game2 = gameService.addGame(account, "TEST_ROOM33", 2, 5, playerGamePW);
        gameService.deleteGameById(hostToken, game2);
        assertEquals(gameService.findGameById(game2), null);
    }

    @Test()
    public void deleteDistrict() {
        gameService.deleteDistrict(hostToken, game1, districtId);
        Game game = gameService.findGameById(game1);
        boolean found = false;
        for (AreaLocation areaLocation : game.getDistricts()) {
            if (areaLocation.getId().equals(districtId)) {
                found = true;
            }
        }
        assertFalse(found);
    }

    @Test(expected = GameException.class)
    public void deleteNonExtistentDistrict() {
        gameService.deleteDistrict(hostToken, game1, "wrongId");
    }

    @Test(expected = GameException.class)
    public void findGameByRoomName() {
        Game game = gameService.findGameByRoomName(defaultGameName);
        assertNotNull(game);
        gameService.findGameByRoomName("wrongRoomName");
    }

    @Test()
    public void findGameByGameId() {
        //stagedGame
        assertNotNull(gameService.findGameById(game1));
        //runningGame
        gameService.startGame(hostToken, game1);
        assertNotNull(gameService.findGameById(game1));
        //databaseGame
        runningGameService.clearRunningGames();
        gameService.clearStagedGames();
        assertNotNull(gameService.findGameById(game1));
    }

    @Test(expected = GameException.class)
    public void addBank() {
        String testBankId = locationService.addPointLocation("testBank", new Point(30, 30), PointType.BANK);
        assertEquals(testBankId, gameService.addBank(hostToken, game1, testBankId));
        String testNotBank = locationService.addPointLocation("testBank", new Point(30, 30), PointType.TRADE_POST);
        gameService.addBank(hostToken, game1, testNotBank);
    }

    @Test
    public void findTeamByClientId() {
        assertNotNull(gameService.findTeamByClientId(game1, clientID));
    }
    @Test(expected = GameException.class)
    public void findTeamByClientIdNonExistentGame() {
        assertNotNull(gameService.findTeamByClientId("ff", clientID));
    }
    @Test(expected = GameException.class)
    public void findTeamByClientIdNonExistentClientID() {
        assertNotNull(gameService.findTeamByClientId(game1,"wrongClientID"));
    }
    @Test
    public void findPlayerByClientId() {
        assertNotNull(gameService.findPlayerByClientId(game1, clientID));
    }
    @Test(expected = GameException.class)
    public void findPlayerByClientIdNonExistentGame() {
        assertNotNull(gameService.findPlayerByClientId("ff", clientID));
    }
    @Test(expected = GameException.class)
    public void findPlayerByClientIdNonExistentClientID() {
        assertNotNull(gameService.findPlayerByClientId(game1,"wrongClientID"));
    }
    @Test
    public void getClientToken(){
        assertNotNull(gameService.getClientToken(game1,clientID));
    }
    @Test(expected = GameException.class)
    public void getNotExistentClientToken(){
        gameService.getClientToken(game1,"wrongClientId");
    }

    @Test
    public void isRunning(){
        gameService.startGame(hostToken,game1);
        assertTrue(gameService.isRunning(game1));
        runningGameService.stopGame(hostToken,game1);
        assertFalse(gameService.isRunning(game1));
    }
}