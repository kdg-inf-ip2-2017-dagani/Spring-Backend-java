package be.kdg.backend.restcontrollers;

import be.kdg.backend.controllers.resources.LoginResource;
import be.kdg.backend.dom.account.AccessLevel;
import be.kdg.backend.dom.account.Account;
import be.kdg.backend.services.api.AccountService;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Nikita on 24/02/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountRestController {
    @Autowired
    WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;
    private Gson gson;

    @Value("default@host.com")
    private String defaultEmailaddress;

    @Autowired
    private AccountService accountService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        gson = new Gson();
    }

    @Test
    public void contextLoads() {
    }

    @Test
    public void register() throws Exception {
        Account a = new Account("testHost", "test", "testHost@sintniklaas.com", AccessLevel.HOST);
        String json = gson.toJson(a);
        mockMvc.perform(post("/api/accounts/").
                content(json).
                contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isOk()).
                andDo(print());
        accountService.deleteAccountByEmailAddress("testHost@sintniklaas.com");
    }

    @Test
    public void registerExistentAccount() throws Exception {
        accountService.addAccount(new Account("testHost", "test", "test@gm.com", AccessLevel.HOST));
        Account a = new Account("test007", "test", "test@gm.com", AccessLevel.HOST);
        String json = gson.toJson(a);
        mockMvc.perform(post("/api/accounts/").
                content(json).
                contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isConflict()).
                andDo(print());
        a = new Account("testHost", "test", "test2@gm.com", AccessLevel.HOST);
        json = gson.toJson(a);
        mockMvc.perform(post("/api/accounts/").
                content(json).
                contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isConflict()).
                andDo(print());
        accountService.deleteAccountByEmailAddress("test@gm.com");
    }

    @Test
    public void login() throws Exception {
        Account a = new Account("testHost", "test", "testHost@sintniklaas.com", AccessLevel.HOST);
        accountService.addAccount(a);
        LoginResource loginResource = new LoginResource(a.getEmailAddress(), a.getPassword());
        Gson gson = new Gson();
        String json = gson.toJson(loginResource);
        mockMvc.perform(post("/api/accounts/login").
                content(json).
                contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isOk()).
                andDo(print());
        accountService.deleteAccountByEmailAddress("testHost@sintniklaas.com");
    }
    @Test
    public void loginNonexistentAccount() throws Exception {
        LoginResource loginResource = new LoginResource("wrong@sss.com","wrong");
        Gson gson = new Gson();
        String json = gson.toJson(loginResource);
        mockMvc.perform(post("/api/accounts/login").
                content(json).
                contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isNotFound()).
                andDo(print());
    }

    @Test
    public void getExistentAccountInfo() throws Exception {
        Account a = new Account("testHost", "test", "testHost@sintniklaas.com", AccessLevel.HOST);
        accountService.addAccount(a);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/accounts/" + "testHost@sintniklaas.com"))
                .andExpect(status().isOk());
        accountService.deleteAccountByEmailAddress("testHost@sintniklaas.com");
    }

    @Test
    public void getNonExistentAccountInfo() throws Exception {
        Thread.sleep(100);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/accounts/" + "wrong"))
                .andExpect(status().isNotFound());
    }
}
