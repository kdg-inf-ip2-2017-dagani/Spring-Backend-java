package be.kdg.backend.restcontrollers;

import be.kdg.backend.controllers.resources.GameResource;
import be.kdg.backend.controllers.resources.RegisterUserResource;
import be.kdg.backend.controllers.rest.GameController;
import be.kdg.backend.dom.account.AccessLevel;
import be.kdg.backend.dom.account.Account;
import be.kdg.backend.dom.game.Game;
import be.kdg.backend.dom.location.AreaType;
import be.kdg.backend.dom.location.Point;
import be.kdg.backend.persistence.api.GameRepository;
import be.kdg.backend.services.api.AccountService;
import be.kdg.backend.services.api.GameService;
import be.kdg.backend.services.api.LocationService;
import be.kdg.backend.services.api.RunningGameService;
import be.kdg.backend.services.exceptions.TokenExpiredException;
import com.google.gson.Gson;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static be.kdg.StniklaasbackendApplication.GSON;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * "C:\Program Files\MongoDB\Server\3.2\bin\mongod.exe" --configsvr --dbpath "C:\Users\gasto\Documents\mongo\data_config" --port 27017
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class GameRestController {

    @Autowired
    WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;
    private MockMvc mockMvcStandAlone;

    @Value("testHost@gmail.com")
    private String realHostEmailAddress;

    @Value("testAdmin@gmail.com")
    private String realAdminEmailAddress;

    @Value("fake@l.c")
    private String fakeEmailAddress;

    @Value("testHost1")
    private String realHostUsername;

    @Value("test")
    private String password;

    @Value("default")
    private String defaultGameName;

    @Value("pw")
    private String playerGamePw;
    @Value("pwHOST")
    private String hostGamePw;

    @Value("clientID")
    private String clientID;

    @Value("defaultClientName")
    private String defaultClientName;

    @Value("clientName")
    private String clientName;

    private String game1, game2, districtId, hostToken, defaultGame, districtId2;

    @Autowired
    private GameService gameService;
    @Autowired
    private AccountService accountService;
    @Mock
    private GameService gameServiceMock;
    @Mock
    private RunningGameService runningGameServiceMock;
    @Mock
    private AccountService accountServiceMock;
    @InjectMocks
    private GameController gameController;
    @Autowired
    private LocationService locationService;

    @Autowired
    private RunningGameService runningGameService;

    @Autowired
    private GameRepository gameRepository;

    @Before
    public void setup() {
        clean();
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        accountService.addAccount(new Account(realHostUsername, password, realHostEmailAddress, AccessLevel.HOST));
        accountService.addAccount(new Account("testAdmin2", password, realAdminEmailAddress, AccessLevel.ADMIN));
        hostToken = accountService.checkLogin(accountService.findAccountByEmailAddress(realHostEmailAddress).getId(), password);
        Game g = new Game(null, "defaultGame", 5, 2, playerGamePw);
        defaultGame = gameRepository.insert(g).getId();
        Account host = accountService.validateToken(hostToken);
        game1 = gameService.addGame(host, "TEST_ROOM", 2, 5, playerGamePw);
        gameService.registerUser(game1, clientID, defaultClientName, playerGamePw);

        game2 = gameService.addGame(host, "TEST_ROOM", 2, 5, playerGamePw);
        districtId = locationService.addAreaLocation(
                "A1",
                new Point[]{
                        new Point(51.164404, 4.138955, 0),
                        new Point(51.165199, 4.139859, 1),
                        new Point(51.164937, 4.141007, 2),
                        new Point(51.163301, 4.141691, 3),
                        new Point(51.163087, 4.141356, 4),
                },
                AreaType.DISTRICT_A
        );
        districtId = locationService.addAreaLocation(
                "A2",
                new Point[]{
                        new Point(51.164404, 4.138955, 0),
                        new Point(51.165199, 4.139859, 1),
                        new Point(51.164937, 4.141007, 2),
                        new Point(51.163301, 4.141691, 3),
                        new Point(51.163087, 4.141356, 4),
                },
                AreaType.DISTRICT_A
        );
        districtId = locationService.addAreaLocation(
                "A3",
                new Point[]{
                        new Point(51.164404, 4.138955, 0),
                        new Point(51.165199, 4.139859, 1),
                        new Point(51.164937, 4.141007, 2),
                        new Point(51.163301, 4.141691, 3),
                        new Point(51.163087, 4.141356, 4),
                },
                AreaType.DISTRICT_A
        );
        districtId = locationService.addAreaLocation(
                "A4",
                new Point[]{
                        new Point(51.164404, 4.138955, 0),
                        new Point(51.165199, 4.139859, 1),
                        new Point(51.164937, 4.141007, 2),
                        new Point(51.163301, 4.141691, 3),
                        new Point(51.163087, 4.141356, 4),
                },
                AreaType.DISTRICT_A
        );
        districtId = locationService.addAreaLocation(
                "A5",
                new Point[]{
                        new Point(51.164404, 4.138955, 0),
                        new Point(51.165199, 4.139859, 1),
                        new Point(51.164937, 4.141007, 2),
                        new Point(51.163301, 4.141691, 3),
                        new Point(51.163087, 4.141356, 4),
                },
                AreaType.DISTRICT_A
        );
        districtId = locationService.addAreaLocation(
                "A6",
                new Point[]{
                        new Point(51.164404, 4.138955, 0),
                        new Point(51.165199, 4.139859, 1),
                        new Point(51.164937, 4.141007, 2),
                        new Point(51.163301, 4.141691, 3),
                        new Point(51.163087, 4.141356, 4),
                },
                AreaType.DISTRICT_A
        );
        districtId = locationService.addAreaLocation(
                "A7",
                new Point[]{
                        new Point(51.164404, 4.138955, 0),
                        new Point(51.165199, 4.139859, 1),
                        new Point(51.164937, 4.141007, 2),
                        new Point(51.163301, 4.141691, 3),
                        new Point(51.163087, 4.141356, 4),
                },
                AreaType.DISTRICT_A
        );
    }

    @After
    public void clean() {
        /*gameService.deleteGameByRoomName("TESTROOM");
        gameService.deleteGameByRoomName("TEST_ROOM");
        gameService.deleteGameByRoomName("TEST_ROOM2");
        accountService.deleteAccountByEmailAddress(realAdminEmailAddress);
        accountService.deleteAccountByEmailAddress(realHostEmailAddress);*/
        gameService.removeAllGames();
        gameService.clearStagedGames();
        runningGameService.clearRunningGames();
        accountService.removeAllAccounts();
        locationService.removeAllLocations();
    }

    @Test
    public void contextLoads() {
    }

    //################################################   NEW GAME
    @Test
    public void newGameCorrectToken() throws Exception {
        GameResource gameResource = new GameResource(hostToken, "flappie", 3, 5, password);
        Gson gson = new Gson();
        String json = gson.toJson(gameResource);
        mockMvc.perform(post("/api/games").
                content(json).
                contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isOk()).
                andDo(print());
    }

    @Test
    public void newGameWrongToken() throws Exception {
        GameResource gameResource = new GameResource("wrong", "flappie", 3, 5, password);
        Gson gson = new Gson();
        String json = gson.toJson(gameResource);
        mockMvc.perform(post("/api/games").
                content(json).
                contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isUnauthorized()).
                andDo(print());
    }

    //################################################   UPDATE GAME
    @Test
    public void saveGameSettingsWithCorrectToken() throws Exception {
        Game g = gameService.findGameById(game1);
        g.setRoomName("EditedRoomName");
        Gson gson = new Gson();
        String json = gson.toJson(g);
        mockMvc.perform(put("/api/games/updategame/" + hostToken).
                content(json).
                contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isOk()).
                andDo(print());
    }

    @Test
    public void saveGameSettingsWithWrongToken() throws Exception {
        Game g = gameService.findGameById(game1);
        g.setRoomName("EditedRoomName");
        Gson gson = new Gson();
        String json = gson.toJson(g);
        mockMvc.perform(put("/api/games/updategame/" + "wrongToken").
                content(json).
                contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isUnauthorized()).
                andDo(print());
    }

    //################################################ FIND GAMES BY ID
    @Test
    public void findExistentGame() throws Exception {
        mockMvc.perform(get("/api/games/" + game1))
                .andExpect(status().isOk());

    }

    @Test
    public void findNonExistentGame() throws Exception {
        mockMvc.perform(get("/api/games/" + "nonExistentId"))
                .andExpect(status().isNotFound());

    }


    //################################################ FIND STAGED GAMES
    @Test
    public void findStagedGames() throws Exception {
        Account host = accountService.validateToken(hostToken);
        String id = gameService.addGame(host, "TESTROOM", 5, 5, "1230");
        mockMvc.perform(get("/api/games/staged")).andExpect(status().isOk());
        gameService.cancelGame(hostToken, id);
    }

    @Test
    public void findStagedGamesWithNoGames() throws Exception {
        gameService.clearStagedGames();
        mockMvc.perform(get("/api/games/staged")).andExpect(status().isNoContent());
    }

    //################################################ FIND RUNNING GAMES
    @Test
    public void findRunningGames() throws Exception {
        Account host = accountService.validateToken(hostToken);
        String id = gameService.addGame(host, "TESTROOM", 5, 5, "1230");
        gameService.startGame(hostToken, id);
        mockMvc.perform(get("/api/games/running")).andExpect(status().isOk());
        runningGameService.endGame(hostToken, id);
    }

    @Test
    public void findRunningGamesWithNoGames() throws Exception {
        mockMvc.perform(get("/api/games/running")).andExpect(status().isNoContent());
    }


    //################################################# FIND ALL GAMES
    @Test
    public void findGames() throws Exception {
        mockMvc.perform(get("/api/games/")).andExpect(status().isOk());
    }

   @Test
    public void findGamesWithNoGames() throws Exception {
        gameService.removeAllGames();
        mockMvc.perform(get("/api/games/")).andExpect(status().isNoContent());
    }

    @Test
    public void findAllGamesListIsNull() throws Exception {
        when(gameServiceMock.findAllGames()).thenReturn(null);
        MockitoAnnotations.initMocks(this);
        mockMvcStandAlone = MockMvcBuilders.standaloneSetup(gameController).build();
        mockMvcStandAlone.perform(get("/api/games/")).andExpect(status().isNotFound());
    }


    //############################################### START GAME
    @Test
    public void startGame() throws Exception {
        mockMvc.perform(post("/api/games/" + game1 + "/startGame/" + hostToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void startGameWithWrongToken() throws Exception {
        mockMvc.perform(post("/api/games/" + game1 + "/startGame/" + "wrongToken")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    public void startGameExpiredToken() throws Exception {
        when(accountServiceMock.validateToken(anyString())).thenThrow(new TokenExpiredException());
        MockitoAnnotations.initMocks(this);
        mockMvcStandAlone = MockMvcBuilders.standaloneSetup(gameController).build();
        mockMvcStandAlone.perform(MockMvcRequestBuilders.post("/api/games/" + game1 + "/startGame/" + hostToken))
                .andExpect(status().isUnauthorized());
    }

    //############################################### STOP GAME
    @Test
    public void stopGame() throws Exception {
        gameService.startGame(hostToken, game1);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/games/" + game1 + "/stopGame/" + hostToken))
                .andExpect(status().isOk());

    }

    @Test
    public void stopGameExpiredToken() throws Exception {
        gameService.startGame(hostToken, game1);
        when(accountServiceMock.validateToken(anyString())).thenThrow(new TokenExpiredException());
        MockitoAnnotations.initMocks(this);
        mockMvcStandAlone = MockMvcBuilders.standaloneSetup(gameController).build();
        mockMvcStandAlone.perform(MockMvcRequestBuilders.post("/api/games/" + game1 + "/stopGame/" + hostToken))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void stopGameInvalidToken() throws Exception {
        gameService.startGame(hostToken, game1);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/games/" + game1 + "/stopGame/" + "invalid"))
                .andExpect(status().isNotFound());
    }

    //############################################### GET PLAYER BY CLIENT ID
    @Test
    public void findPlayerByClientID() throws Exception {
        mockMvc.perform(get("/api/games/" + game1 + "/players/" + clientID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void findPlayerByNonExistentClientID() throws Exception {
        mockMvc.perform(get("/api/games/" + game1 + "/players/nonExistentClientId")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    //############################################### REGISTER USER
    @Test
    public void registerPlayer() throws Exception {
        RegisterUserResource rur = new RegisterUserResource(clientID + "2", "player", playerGamePw);
        String json = GSON.toJson(rur, RegisterUserResource.class);
        ResultActions r = mockMvc.perform(post("/api/games/" + game1 + "/register")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void registerHost() throws Exception {
        RegisterUserResource rur = new RegisterUserResource(clientID, "host", hostGamePw);
        String json = GSON.toJson(rur, RegisterUserResource.class);
        ResultActions r = mockMvc.perform(post("/api/games/" + game1 + "/register")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void registerUserForNonExistentGame() throws Exception {
        RegisterUserResource rur = new RegisterUserResource(clientID, "player", "");
        String json = GSON.toJson(rur, RegisterUserResource.class);
        ResultActions r = mockMvc.perform(post("/api/games/1641/register")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    public void registerUserWithWrongPassword() throws Exception {
        RegisterUserResource rur = new RegisterUserResource(clientID, "player", "164411");
        String json = GSON.toJson(rur, RegisterUserResource.class);
        ResultActions r = mockMvc.perform(post("/api/games/" + game1 + "/register")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized())
                .andDo(print());
    }

    @Test
    public void registerUserWithDuplicateUser() throws Exception {
        RegisterUserResource rur = new RegisterUserResource(clientID, "player", playerGamePw);
        String json = GSON.toJson(rur, RegisterUserResource.class);
        ResultActions r = mockMvc.perform(post("/api/games/" + game1 + "/register")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void registerUserWithWrongJson() throws Exception {
        RegisterUserResource rur = new RegisterUserResource(clientID, "player", "");
        String json = GSON.toJson(rur, RegisterUserResource.class);
        List<Character> chars = new ArrayList<>();
        for (int i = 0; i < json.toCharArray().length; i++) {
            chars.add(json.toCharArray()[i]);
        }
        Collections.shuffle(chars);
        Collections.shuffle(chars);
        StringBuilder sb = new StringBuilder();
        for (Character aChar : chars) {
            sb.append(aChar);
        }
        ResultActions r = mockMvc.perform(post("/api/games/" + game1 + "/register")
                .content(sb.toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andDo(print());
    }

    //############################################### UNREGISTER USER
    @Test
    public void unregisterUser() throws Exception {
        gameService.registerUser(game1, "12345", "clientje", playerGamePw);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/games/" + game1 + "/unregister/" + "12345"))
                .andExpect(status().isOk());
    }

    @Test
    public void unregisterUserNonExistentGame() throws Exception {
        gameService.registerUser(game1, "12345", "clientje", playerGamePw);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/games/" + "wrong" + "/unregister/" + "12345"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void unregisterUserNonStagedGame() throws Exception {
        gameService.registerUser(game1, "12345", "clientje", playerGamePw);
        gameService.startGame(hostToken, game1);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/games/" + game1 + "/unregister/" + "12345"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void unregisterNonExistentUser() throws Exception {
        gameService.startGame(hostToken, game1);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/games/" + game1 + "/unregister/" + "12345"))
                .andExpect(status().isNotFound());
    }

    //############################################### REGISTER WEBAPP
    @Test
    public void registerWebApp() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/games/" + game1 + "/webreg/" + hostToken))
                .andExpect(status().isOk());
    }

    @Test
    public void registerWebAppInvalidToken() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/games/" + game1 + "/webreg/" + "invalid"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void registerWebAppNonExistentGame() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/games/" + "invalidId" + "/webreg/" + hostToken))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    public void registerWebAppWrongHost() throws Exception {
        String id = accountService.addAccount(new Account("testUser", "test", "t@test.com", AccessLevel.HOST));
        String token = accountService.checkLogin(id, "test");
        mockMvc.perform(MockMvcRequestBuilders.get("/api/games/" + game1 + "/webreg/" + token))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    public void registerWebAppExpiredToken() throws Exception {
        when(gameServiceMock.registerWebApp(anyString(), anyString())).thenThrow(new TokenExpiredException());
        MockitoAnnotations.initMocks(this);
        mockMvcStandAlone = MockMvcBuilders.standaloneSetup(gameController).build();
        mockMvcStandAlone.perform(MockMvcRequestBuilders.get("/api/games/" + game1 + "/webreg/" + hostToken))
                .andExpect(status().isUnauthorized());
    }
}
