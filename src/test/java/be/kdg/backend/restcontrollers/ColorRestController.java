package be.kdg.backend.restcontrollers;

import be.kdg.backend.dom.util.CustomColor;
import be.kdg.backend.services.api.ColorService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Nikita on 13/03/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ColorRestController {
    @Autowired
    WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;
    @Autowired
    private ColorService colorService;

    @Test
    public void contextLoads() {
    }

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
    @After
    public void clear() {
        colorService.removeAllColors();
    }
    @Test
    public void getColors() throws Exception {
        colorService.addColor(new CustomColor("#FFFFFF","white"));
        mockMvc.perform(get("/api/colors/")).andExpect(status().isOk());
    }

    @Test
    public void getColorsEmptyList() throws Exception {
        colorService.removeAllColors();
        mockMvc.perform(get("/api/colors/")).andExpect(status().isNotFound());
    }
}
