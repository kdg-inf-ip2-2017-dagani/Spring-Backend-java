package be.kdg.backend.restcontrollers;

import be.kdg.backend.dom.location.AreaType;
import be.kdg.backend.dom.location.Point;
import be.kdg.backend.dom.location.PointType;
import be.kdg.backend.services.api.LocationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Nikita on 20/02/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class LocationRestController {
    @Autowired
    WebApplicationContext webApplicationContext;
    @Autowired
    LocationService locationService;
    private MockMvc mockMvc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void FindAllAreaLocations() throws Exception {
        if (locationService.findAreas().size() > 0) {
            mockMvc.perform(get("/api/locations/arealocations"))
                    .andExpect(status().isOk());
        } else {
            mockMvc.perform(get("/api/locations/arealocations"))
                    .andExpect(status().isNotFound());
        }
    }

    @Test
    public void FindAllAreaLocationsWithDistrictAType() throws Exception {
        locationService.addAreaLocation("districtA",
                new Point[]{
                        new Point(51.164404, 4.138955, 0),
                        new Point(51.165199, 4.139859, 1),
                        new Point(51.164937, 4.141007, 2),
                        new Point(51.163301, 4.141691, 3),
                        new Point(51.163087, 4.141356, 4)
                }, AreaType.DISTRICT_A
        );
        mockMvc.perform(get("/api/locations/arealocations").param("type", String.valueOf(AreaType.DISTRICT_A)))
                .andExpect(status().isOk());
        locationService.removeAllAreaLocations();
        mockMvc.perform(get("/api/locations/arealocations").param("type", String.valueOf(AreaType.DISTRICT_A)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void FindAllAreaLocationsWithDistrictBtype() throws Exception {
        locationService.addAreaLocation("districtB",
                new Point[]{
                        new Point(51.164404, 4.138955, 0),
                        new Point(51.165199, 4.139859, 1),
                        new Point(51.164937, 4.141007, 2),
                        new Point(51.163301, 4.141691, 3),
                        new Point(51.163087, 4.141356, 4)
                }, AreaType.DISTRICT_B
        );
        mockMvc.perform(get("/api/locations/arealocations").param("type", String.valueOf(AreaType.DISTRICT_B)))
                .andExpect(status().isOk());
        locationService.removeAllAreaLocations();
        mockMvc.perform(get("/api/locations/arealocations").param("type", String.valueOf(AreaType.DISTRICT_B)))
                .andExpect(status().isNotFound());

    }

    @Test
    public void FindAllAreaLocationsWithMarkettype() throws Exception {
        locationService.addAreaLocation("market",
                new Point[]{
                        new Point(51.164404, 4.138955, 0),
                        new Point(51.165199, 4.139859, 1),
                        new Point(51.164937, 4.141007, 2),
                        new Point(51.163301, 4.141691, 3),
                        new Point(51.163087, 4.141356, 4)
                }, AreaType.MARKET
        );
        mockMvc.perform(get("/api/locations/arealocations").param("type", String.valueOf(AreaType.MARKET)))
                .andExpect(status().isOk());
        locationService.removeAllAreaLocations();
        mockMvc.perform(get("/api/locations/arealocations").param("type", String.valueOf(AreaType.MARKET)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void FindAllPoinLocationsWithBanktype() throws Exception {
        locationService.addPointLocation("bankTest",new Point(20,20),PointType.BANK);

        mockMvc.perform(get("/api/locations/pointlocations").param("type", String.valueOf(PointType.BANK)))
                .andExpect(status().isOk());
        locationService.removeAllPointLocations();
        mockMvc.perform(get("/api/locations/pointlocations").param("type", String.valueOf(PointType.BANK)))
                .andExpect(status().isNotFound());
    }
    @Test
    public void FindAllPoinLocationsWithTradePosttype() throws Exception {
        locationService.addPointLocation("tradePostTest",new Point(20,20),PointType.TRADE_POST);

        mockMvc.perform(get("/api/locations/pointlocations").param("type", String.valueOf(PointType.TRADE_POST)))
                .andExpect(status().isOk());
        locationService.removeAllPointLocations();
        mockMvc.perform(get("/api/locations/pointlocations").param("type", String.valueOf(PointType.TRADE_POST)))
                .andExpect(status().isNotFound());
    }

}
