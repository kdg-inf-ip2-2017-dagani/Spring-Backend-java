package be.kdg.backend.websockets;

import org.apache.log4j.Logger;

import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

/**
 * Created by Nikita on 14/03/2017.
 */
@ClientEndpoint

public class TestWebSocketClient {

    private final Logger logger = Logger.getLogger(TestWebSocketClient.class);
    protected WebSocketContainer container;
    protected Session userSession = null;
    ClientEndpointConfig clientEndpointConfig;

    public TestWebSocketClient() throws NoSuchAlgorithmException, KeyStoreException, IOException, CertificateException, UnrecoverableKeyException, KeyManagementException {
        /*
        KeyStore ks = KeyStore.getInstance("DES");
        ks.load(TestWebSocketClient.class.getClassLoader().getResourceAsStream("resources/tomcat.keystore"), "changeit".toCharArray());
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(ks);
        KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmf.init(ks, "changeit".toCharArray());
        SSLContext ctx = SSLContext.getInstance("TLS");
        ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
        SSLContext.setDefault(ctx);
        SSLContext sslContext = SSLContext.getInstance("TLS");
        clientEndpointConfig = ClientEndpointConfig.Builder.create().build();
        clientEndpointConfig.getUserProperties().put(Constants.SSL_CONTEXT_PROPERTY,
                sslContext);
        container = ContainerProvider.getWebSocketContainer();

        */
    }

    public void Connect(String sServer) {
        try {
            userSession = container.connectToServer(new Endpoint() {
                @Override
                public void onOpen(Session session, EndpointConfig config) {
                    System.out.println(session.isOpen());
                    config.getUserProperties().forEach((k, v) -> System.out.println("" + k + " " + v));
                }
            }, clientEndpointConfig, URI.create(sServer));
        } catch (DeploymentException | IOException e) {
            logger.debug("opening connection failed");
        }
    }

    public void SendMessage(String sMsg) throws IOException {
        userSession.getBasicRemote().sendText(sMsg);
    }

    @OnOpen
    public void onOpen(Session session) {
        System.out.println("Connected!!!");
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
    }

    @OnMessage
    public void onMessage(String msg) {
        System.out.println(msg);
    }

    public void Disconnect() throws IOException {
        if (userSession != null && userSession.isOpen()) {
            userSession.close();
        }
    }

    public boolean isOpen() {
        return userSession != null && userSession.isOpen();
    }
}

