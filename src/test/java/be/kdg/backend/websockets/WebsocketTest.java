package be.kdg.backend.websockets;


import be.kdg.backend.Seed;
import be.kdg.backend.dom.game.Game;
import be.kdg.backend.services.api.GameService;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import static org.junit.Assert.assertTrue;

/**
 * Created by Nikita on 14/03/2017.
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
//@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class WebsocketTest {
    static final String WEBSOCKET_URI = "wss://localhost:8090/user";

    @Autowired
    private GameService gameService;
    @Autowired
    private Seed seed;
    //private TestWebSocketClient client;
    private String clientToken;


    //@Before
    public void setup() throws NoSuchAlgorithmException, UnrecoverableKeyException, CertificateException, KeyManagementException, KeyStoreException, IOException {
        seed.init();
        Game game = gameService.findGameByRoomName("firstGame");
        clientToken = gameService.registerUser(game.getId(), "CLIENTID", "testUser", "pw");
        //client = new TestWebSocketClient();
    }


    //@Test
    public void success(){
        assertTrue(true);
    }

    /*
    @Test
    public void createSessionAfterOpenLogWebSocketHandler() throws Exception {
        Thread.sleep(100);
        for (int i = 0; i < 10; i++) {
            //client.Connect(WEBSOCKET_URI);
            if (client.isOpen()) {
                i = 1000;
            }
        }
        boolean open = true;
        //boolean open = client.isOpen();
        assertEquals(true, open);
        client.userSession.getBasicRemote().sendText("testen");
        Thread.sleep(1000);
        client.Disconnect();
        assertEquals(true, open);
    }
    */
/*
    @Test
    public void createSessionAfterOpenLogWebSocketHandler() throws Exception {
        client.Connect(WEBSOCKET_URI);
        boolean open = client.isOpen();
        Thread.sleep(1000);
        client.Disconnect();
        assertEquals(true,open);
    }*/


}