package be.kdg.backend.controllers.rest;

import be.kdg.backend.controllers.resources.AccountInfoResource;
import be.kdg.backend.controllers.resources.LoginResource;
import be.kdg.backend.dom.account.Account;
import be.kdg.backend.services.api.AccountService;
import be.kdg.backend.services.exceptions.AccountServiceException;
import be.kdg.backend.services.exceptions.AuthenticationException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static be.kdg.StniklaasbackendApplication.GSON;

/**
 * Created by Nikita on 23/02/2017.
 */
@RestController
@RequestMapping("api/accounts")
public class AccountController {

    private final AccountService accountService;
    private final Logger logger = Logger.getLogger(AccountController.class);

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> register(@RequestBody String account) {
        Account acc;
        String token;
        try {
            acc = GSON.fromJson(account, Account.class);
            String id = accountService.addAccount(acc);
            token = accountService.checkLogin(id, acc.getPassword());
        } catch (AccountServiceException e) {
            logger.debug(e.getMessage());
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch (AuthenticationException e) {
            logger.debug(e.getMessage());
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        logger.debug("New account registered");
        return new ResponseEntity<>(token, HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<String> login(@RequestBody LoginResource loginResource) {
        try {
            Account a = accountService.findAccountByEmailAddress(loginResource.getEmailaddress());
            String token = accountService.checkLogin(a.getId(), loginResource.getPassword());
            return new ResponseEntity<>(token, HttpStatus.OK);
        } catch (AccountServiceException e) {
            logger.debug("Account not found or login incorrect");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (AuthenticationException e) {
            logger.debug("Login incorrect");
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }


    @CrossOrigin
    @RequestMapping(value = "/login/{deviceId}", method = RequestMethod.POST)
    public ResponseEntity<String> login(@PathVariable String deviceId) {
        Account a;
        try {
            a = accountService.findAccountByCurrentDeviceId(deviceId);
        } catch (AccountServiceException e) {
            String id = accountService.addAccount(new Account(deviceId));
            a = accountService.findAccountByCurrentDeviceId(deviceId);
        }

        try{
            String token = accountService.checkLogin(a.getId(), a.getPassword());
            return new ResponseEntity<>(token, HttpStatus.OK);}
        catch (AuthenticationException e) {
            logger.debug("Login incorrect");
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/{emailaddress:.+}", method = RequestMethod.GET)
    public ResponseEntity<AccountInfoResource> getAccountInfo(@PathVariable String emailaddress) {
        AccountInfoResource accountInfoResource;
        try {
            Account a = accountService.findAccountByEmailAddress(emailaddress);
            accountInfoResource = new AccountInfoResource(a);
        } catch (AccountServiceException e) {
            logger.debug("Account not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(accountInfoResource, HttpStatus.OK);
    }
}
