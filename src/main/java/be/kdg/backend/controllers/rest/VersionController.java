package be.kdg.backend.controllers.rest;

import be.kdg.backend.controllers.resources.GameResource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by gasto on 23/06/2017.
 */


@RestController
@RequestMapping("api/version")
public class VersionController {

    @Value("${version.minimum.client}")
    private String minVersion;

    @Value("${version.server}")
    private String serverVersion;


    @CrossOrigin
    @RequestMapping(value = "/minimum", method = RequestMethod.GET)
    public ResponseEntity<String> getMinimumClientVersion() {
        return new ResponseEntity<String>(minVersion,HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/server", method = RequestMethod.GET)
    public ResponseEntity<String> getServerVersion() {
        return new ResponseEntity<String>(serverVersion,HttpStatus.OK);
    }

}
