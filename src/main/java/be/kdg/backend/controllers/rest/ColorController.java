package be.kdg.backend.controllers.rest;

import be.kdg.backend.dom.util.CustomColor;
import be.kdg.backend.services.api.ColorService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Nikita on 7/03/2017.
 */
@RestController
@RequestMapping("api/colors")
public class ColorController {
    private final ColorService colorService;
    private final Logger logger = Logger.getLogger(GameController.class);

    @Autowired
    public ColorController(ColorService colorService) {
        this.colorService=colorService;
    }

    @CrossOrigin
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<List<CustomColor>> getColors() {
        List<CustomColor> customColors =colorService.findColors();
        if (customColors == null || customColors.size()==0) {
            logger.debug("Colors not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(customColors, HttpStatus.OK);
    }
}
