package be.kdg.backend.controllers.rest;

import be.kdg.backend.dom.location.AreaLocation;
import be.kdg.backend.dom.location.AreaType;
import be.kdg.backend.dom.location.PointLocation;
import be.kdg.backend.dom.location.PointType;
import be.kdg.backend.services.api.LocationService;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Nikita on 20/02/2017.
 */

@RestController
@RequestMapping("api/locations")
public class LocationController {
    private final LocationService locationService;
    private final Logger logger = Logger.getLogger(LocationController.class);

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }


    @CrossOrigin
    @RequestMapping(value = "/arealocations", method = RequestMethod.GET)
    public ResponseEntity<List<AreaLocation>> getAreaLocation(@RequestParam(value = "type", required = false) AreaType areatype) {
        List<AreaLocation> areaLocations;
        if (areatype == null)
            areaLocations = locationService.findAreas();
        else
            areaLocations = locationService.findAreasByType(areatype);
        if (areaLocations.size() < 1) {
            logger.debug("No locations found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(areaLocations, HttpStatus.OK);
    }
    @CrossOrigin
    @RequestMapping(value = "/pointlocations", method = RequestMethod.GET)
    public ResponseEntity<List<PointLocation>> findPointLocation(@RequestParam(value = "type", required = false) PointType pointType) {
        List<PointLocation> pointLocations = locationService.findPointsByType(pointType);
        if (pointLocations.size() < 1) {
            logger.debug("No locations found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(pointLocations, HttpStatus.OK);
    }

}
