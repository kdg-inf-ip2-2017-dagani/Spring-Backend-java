package be.kdg.backend.controllers.rest;

import be.kdg.backend.controllers.resources.*;
import be.kdg.backend.dom.account.Account;
import be.kdg.backend.dom.game.Game;
import be.kdg.backend.dom.game.GameState;
import be.kdg.backend.dom.game.Player;
import be.kdg.backend.services.api.AccountService;
import be.kdg.backend.services.api.GameService;
import be.kdg.backend.services.api.RunningGameService;
import be.kdg.backend.services.exceptions.*;
import be.kdg.backend.services.runners.GameRunner;
import be.kdg.backend.services.runners.messages.WinningTeamMessage;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static be.kdg.StniklaasbackendApplication.GSON;

/**
 * Created by Nikita on 16/02/2017.
 */
@RestController
@RequestMapping("api/games")
public class GameController {
    public static final Gson GSON = new Gson();
    private final GameService gameService;
    private final AccountService accountService;
    private final Logger logger = Logger.getLogger(GameController.class);
    @Autowired
    private RunningGameService runningGameService;

    @Autowired
    public GameController(GameService gameService, AccountService accountService) {
        this.gameService = gameService;
        this.accountService = accountService;
    }

    @CrossOrigin
    @RequestMapping(value = "/{gameId}/duration/{token}", method = RequestMethod.POST)
    public ResponseEntity<Game> changeDuration(@PathVariable String gameId, @PathVariable String token, @RequestBody int minutes) {
        try {
            Account a = accountService.validateToken(token);
            gameService.changeDuration(token, gameId, minutes);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (TokenExpiredException e) {
            logger.debug("Token expired");
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (InvalidTokenException e) {
            logger.warn("Invalid token entered, or no account associated with token.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<List<Game>> getAllGames() {
        List<Game> games = gameService.findAllGames();
        if (games == null) {
            logger.debug("Game not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (games.size() == 0) {
            logger.debug("Games list is empty");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(games, HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/running", method = RequestMethod.GET)
    public ResponseEntity<List<GameListResource>> getAllRunningGames() {
        List<Game> games = gameService.findAllRunningGames();

        if (games == null) {
            logger.debug("Game not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (games.size() == 0) {
            logger.debug("Games list is empty");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        List<GameListResource> response = new ArrayList<>();
        for (Game game : games) {
            response.add(new GameListResource(game));
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/staged", method = RequestMethod.GET)
    public ResponseEntity<String> getAllStagedGames() {
        List<Game> games = gameService.findAllStagedGames();
        if (games == null) {
            logger.debug("Game not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (games.size() == 0) {
            logger.debug("Games list is empty");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        List<GameListResource> stagedGames = new ArrayList<>();
        for (Game game : games) {
            stagedGames.add(new GameListResource(game));
        }

        if (stagedGames.size() == 0) {
            logger.debug("Staged games list is empty");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        String response = GSON.toJson(stagedGames);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/{gameId}", method = RequestMethod.GET)
    public ResponseEntity<String> getGameByGameId(@PathVariable("gameId") String gameId) {
        Game game = gameService.findGameById(gameId);
        if (game == null) {
            logger.debug("Game not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(GSON.toJson(new ExtendedGameResource(game)), HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/state/{gameID}", method = RequestMethod.GET)
    public ResponseEntity<String> getGameState(@PathVariable String gameID) {
        GameState gameState = gameService.getGameState(gameID);
        if (gameState == null) return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(gameState.toString(), HttpStatus.OK);
    }

    //############################################### PLAYER CRUD
    @CrossOrigin
    @RequestMapping(value = "/{gameId}/players/{clientId}", method = RequestMethod.GET)
    public ResponseEntity<Player> getPlayerByClientId(@PathVariable String gameId, @PathVariable String clientId) {
        Player player;
        try {
            player = gameService.findPlayerByClientId(gameId, clientId);
        } catch (GameException e) {
            logger.debug("Player not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(player, HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/{gameId}/kick/{token}", method = RequestMethod.POST)
    public ResponseEntity kickPlayer(@PathVariable String gameId, @PathVariable String token, @RequestBody String playerToKick) {
        try {
            Account a = accountService.validateToken(token);
            gameService.kickPlayer(token, gameId, playerToKick);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (TokenExpiredException e) {
            logger.debug("Token expired");
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (InvalidTokenException e) {
            logger.warn("Invalid token entered, or no account associated with token.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //############################################### GAME CRUD
    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> newGame(@RequestBody GameResource newGameResource) {
        Account a;
        try {
            a = accountService.validateToken(newGameResource.getToken());
        } catch (InvalidTokenException e) {
            logger.debug("Token invalid");
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        try {
            runningGameService.findRunningGameByRoomName(newGameResource.getRoomName());
            //game name already exists, bad
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (GameException e) {
            // game doesn't exist yet
        }
        try {
            gameService.findStagedGameByRoomName(newGameResource.getRoomName());
            //game name already exists, bad
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (GameException e) {
            // game doesn't exist yet
        }

        try {
            //at this point game name is not staged or running
            String g = gameService.addGame(a, newGameResource.getRoomName(), newGameResource.getMaxTeams(), newGameResource.getMaxPlayersPerTeam(), newGameResource.getPassword());
            return new ResponseEntity<>(g, HttpStatus.OK);
        } catch (GameException e) {
            //some error during game creation
            logger.debug(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/{gameID}/register", method = RequestMethod.POST)
    @CrossOrigin
    public ResponseEntity<String> registerUser(@PathVariable String gameID, @RequestBody String userData) {
        if (!gameService.isStaged(gameID)) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        try {
            RegisterUserResource res = GSON.fromJson(userData, RegisterUserResource.class);
            String token = gameService.registerUser(gameID, res.getClientID(), res.getName(), res.getPassword());
            return new ResponseEntity<>(token, HttpStatus.OK);
        } catch (WrongPasswordException e) {
            logger.debug("Wrong password");
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (DuplicateUserException e) {
            logger.debug("User with given id already exists");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch (JsonSyntaxException | NullPointerException e) {
            logger.debug("json received was not parseable");
            logger.debug(userData);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    //############################################### WEBAPP OPERATIONS
    @CrossOrigin
    @RequestMapping(value = "/{gameID}/webreg/{token}", method = RequestMethod.GET)
    public ResponseEntity<String> registerWebapp(@PathVariable String gameID, @PathVariable String token) {
        try {
            String tok = gameService.registerWebApp(gameID, token);
            return new ResponseEntity<>(tok, HttpStatus.OK);
        } catch (GameException e) {
            logger.debug("Generic game exception", e);
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        } catch (TokenExpiredException e) {
            logger.debug("Token expired");
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (InvalidTokenException e) {
            logger.warn("Invalid token entered, or no account associated with token.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Any other error ", e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/{gameID}/webreg/", method = RequestMethod.GET)
    public ResponseEntity<String> registerWebapp(@PathVariable String gameID) {
        try {
            String tok = gameService.registerWebApp(gameID);
            return new ResponseEntity<>(tok, HttpStatus.OK);
        } catch (GameException e) {
            logger.debug("Generic game exception", e);
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        } catch (TokenExpiredException e) {
            logger.debug("Token expired");
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (InvalidTokenException e) {
            logger.warn("Invalid token entered, or no account associated with token.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Any other error ", e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/{gameId}/winner", method = RequestMethod.GET)
    public ResponseEntity<WinningTeamMessage> requestFinalGameStats(@PathVariable String gameId) {
        try {
            WinningTeamMessage winningTeamMessage = gameService.getFinalGameStats(gameId);
            return new ResponseEntity<>(winningTeamMessage, HttpStatus.OK);
        } catch (GameException e) {
            logger.debug("Game not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/{gameId}/treasury/{districtId}/{clientToken}", method = RequestMethod.GET)
    public ResponseEntity<Double> requestTreasuryById(@PathVariable String gameId, @PathVariable String districtId, @PathVariable String clientToken) {
        try {
            GameRunner gr = gameService.findGameRunner(gameId);
            double treasury = gr.getTreasuryByDistrict(clientToken, districtId);
            return new ResponseEntity<>(treasury, HttpStatus.OK);
        } catch (GameException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (InvalidTokenException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/restart/{gameId}/{token}/{seconds}", method = RequestMethod.POST)
    public ResponseEntity restartGame(@PathVariable String gameId, @PathVariable String token, @PathVariable int seconds) {
        Account a;
        try {
            a = accountService.validateToken(token);
        } catch (InvalidTokenException e) {
            logger.debug("Token invalid");
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        try{
        gameService.stopIfGameRunning(gameId);
        gameService.restartGame(a,gameId,seconds);
        } catch (GameException e) {
            //some error
            logger.warn(e.getMessage());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/updategame/{token}", method = RequestMethod.PUT)
    public ResponseEntity<Game> saveGameSettings(@PathVariable String token, @RequestBody String game) {
        Game g = GSON.fromJson(game, Game.class);
        try {
            gameService.saveGameSettings(token, g);
        } catch (InvalidTokenException e) {
            logger.debug("Token invalid");
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(g, HttpStatus.OK);
    }

    //############################################### GAME OPERATIONS
    @CrossOrigin
    @RequestMapping(value = "/{gameId}/startGame/{token}", method = RequestMethod.POST)
    public ResponseEntity<Game> startGame(@PathVariable String gameId, @PathVariable String token) {
        try {
            Account a = accountService.validateToken(token);
            gameService.startGame(token, gameId);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (TokenExpiredException e) {
            logger.debug("Token expired");
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (InvalidTokenException e) {
            logger.warn("Invalid token entered, or no account associated with token.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/{gameId}/stopGame/{token}", method = RequestMethod.POST)
    public ResponseEntity<Game> stopGame(@PathVariable String gameId, @PathVariable String token) {
        try {
            Account a = accountService.validateToken(token);
            runningGameService.stopGame(token, gameId);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (TokenExpiredException e) {
            logger.debug("Token expired");
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (InvalidTokenException e) {
            logger.warn("Invalid token entered, or no account associated with token.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{gameID}/unregister/{clientID}", method = RequestMethod.POST)
    @CrossOrigin
    public ResponseEntity<ConnectionResource> unregisterUser(@PathVariable String gameID, @PathVariable String clientID) {
        try {
            gameService.unregisterUser(gameID, clientID);
        } catch (GameException e) {
            logger.debug("Caught exception", e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

}