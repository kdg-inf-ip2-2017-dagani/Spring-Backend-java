package be.kdg.backend.controllers.resources;

import be.kdg.backend.dom.account.Account;

/**
 * Created by Nikita on 27/02/2017.
 */
public class AccountInfoResource {
    private String username;
    private String emailaddress;

    public AccountInfoResource(Account a) {
        this.username = a.getUsername();
        this.emailaddress = a.getEmailAddress();
    }

    public String getUsername() {
        return username;
    }


    public String getEmailaddress() {
        return emailaddress;
    }

}
