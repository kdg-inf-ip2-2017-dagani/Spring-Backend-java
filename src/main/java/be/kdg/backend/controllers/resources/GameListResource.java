package be.kdg.backend.controllers.resources;

import be.kdg.backend.dom.game.Game;
import be.kdg.backend.dom.game.Team;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.controllers.resources
 *
 * @author Gaston
 * @version 1.0
 * @since 21/02/2017
 */
public class GameListResource {
    private String id;
    private String name;
    private boolean hasPassword;
    private int players,maxPlayers;
    private long startTime,seconds;

    public GameListResource(Game g) {
        id = g.getId();
        name = g.getRoomName();
        hasPassword = !g.getPlayerPassword().isEmpty() || !g.getHostPassword().equalsIgnoreCase("host");
        maxPlayers = g.getMaxPlayersPerTeam()* g.getTeams().size();
        startTime = g.getStartTime();
        players=0;
        seconds = g.getDuration().getSeconds();
        for (Team team : g.getTeams()) {
            players += team.getPlayers().size();
        }
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean getHasPassword() {
        return hasPassword;
    }

    public boolean hasPassword() {
        return hasPassword;
    }

    public int getPlayers() {
        return players;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public boolean isHasPassword() {
        return hasPassword;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getSeconds() {
        return seconds;
    }
}