package be.kdg.backend.controllers.resources;

/**
 * Created by Nikita on 16/02/2017.
 */
public class GameResource {
    private String token;
    private String roomName;
    private int maxTeams;
    private int maxPlayersPerTeam;
    private String password;

    public GameResource() {
    }

    public GameResource(String token, String roomName, int maxTeams, int maxPlayersPerTeam, String password) {
        this.token = token;
        this.roomName = roomName;
        this.maxTeams = maxTeams;
        this.maxPlayersPerTeam = maxPlayersPerTeam;
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public int getMaxTeams() {
        return maxTeams;
    }

    public void setMaxTeams(int maxTeams) {
        this.maxTeams = maxTeams;
    }

    public int getMaxPlayersPerTeam() {
        return maxPlayersPerTeam;
    }

    public void setMaxPlayersPerTeam(int maxPlayersPerTeam) {
        this.maxPlayersPerTeam = maxPlayersPerTeam;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
