package be.kdg.backend.controllers.resources;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.controllers.resources
 *
 * @author Gaston
 * @version 1.0
 * @since 22/02/2017
 */
public class RegisterUserResource {
    private String clientID;
    private String name;
    private String password;

    public RegisterUserResource() {
    }

    public RegisterUserResource(String clientID, String name, String password) {
        this.clientID = clientID;
        this.name = name;
        this.password = password;
    }

    public String getClientID() {
        return clientID;
    }


    public String getName() {
        return name;
    }


    public String getPassword() {
        return password;
    }

}