package be.kdg.backend.controllers.resources;

/**
 * Created by Nikita on 24/02/2017.
 */
public class LoginResource {
    private String emailaddress;
    private String password;

    public LoginResource() {
    }

    public LoginResource(String emailaddress, String password) {
        this.emailaddress = emailaddress;
        this.password = password;
    }

    public String getEmailaddress() {
        return emailaddress;
    }

    public void setEmailaddress(String emailaddress) {
        this.emailaddress = emailaddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
