package be.kdg.backend.controllers.resources;

import be.kdg.backend.dom.game.Game;
import be.kdg.backend.dom.game.Player;
import be.kdg.backend.dom.game.Team;
import be.kdg.backend.dom.location.AreaLocation;
import be.kdg.backend.dom.location.PointLocation;
import be.kdg.backend.services.util.JsonTeam;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.controllers.resources
 *
 * @author Gaston
 * @version 1.0
 * @since 1/06/2017
 */
public class ExtendedGameResource {
    public String id;
    public String roomName;
    public List<AreaLocation> districts;
    public List<AreaLocation> markets;
    public List<PointLocation> tradePosts;
    public List<PointLocation> banks;
    public List<JsonTeam> teams;

    public List<Player> hosts;
    public int maxPlayersPerTeam;
    public int maxTeams;
    public long startTime;
    public long endTime;
    public boolean treasuriesOpen;

    public ExtendedGameResource(Game g) {
        id=g.getId();
        roomName=g.getRoomName();
        districts=g.getDistricts();
        markets=g.getMarkets();
        tradePosts=g.getTradePosts();
        banks=g.getBanks();
        hosts=g.getHosts();
        maxPlayersPerTeam=g.getMaxPlayersPerTeam();
        maxTeams=g.getMaxTeams();
        startTime=g.getStartTime();
        endTime=g.getEndTime();
        treasuriesOpen=g.isTreasuriesOpen();

        teams = new ArrayList<>();
        for (Team team : g.getTeams()) {
            teams.add(new JsonTeam(team));
        }
    }
}