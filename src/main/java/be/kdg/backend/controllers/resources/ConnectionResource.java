package be.kdg.backend.controllers.resources;


/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.runners.messages
 *
 * @author Gaston
 * @version 1.0
 * @since 22/02/2017
 */
public class ConnectionResource {
    private String clientToken;

    public ConnectionResource(String clientToken) {
        this.clientToken = clientToken;
    }

    public String getClientToken() {
        return clientToken;
    }
}