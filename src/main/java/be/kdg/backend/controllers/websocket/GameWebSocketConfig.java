package be.kdg.backend.controllers.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.config
 *
 * @author Gaston
 * @version 1.0
 * @since 4/03/2017
 */
@Configuration
@EnableWebSocket
@EnableScheduling
public class GameWebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private
    GameWebSocketHandler gameWebSocketHandler;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(gameWebSocketHandler,"/user").setAllowedOrigins("*");
    }
}