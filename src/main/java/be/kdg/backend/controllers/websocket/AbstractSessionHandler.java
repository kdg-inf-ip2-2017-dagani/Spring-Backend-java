package be.kdg.backend.controllers.websocket;

import be.kdg.backend.services.exceptions.GameException;
import be.kdg.backend.services.runners.messages.GameMessageType;
import be.kdg.backend.services.runners.messages.MessageWrapper;
import org.apache.log4j.Logger;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import static be.kdg.StniklaasbackendApplication.*;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.controllers.websocket
 *
 * @author Gaston
 * @version 1.0
 * @since 5/03/2017
 */
public abstract class AbstractSessionHandler {
    private final Logger logger = Logger.getLogger(AbstractSessionHandler.class);
    WebSocketSession session;
    String gameID;
    SessionLevel level;
    Queue<TextMessage> messageBuffer;

    AbstractSessionHandler() {
        messageBuffer = new ConcurrentLinkedQueue<>();
    }

    //ABSTRACT METHODS
    public abstract void send(GameMessageType type, String message);

    public abstract void handle(MessageWrapper message);

    //DEFAULT METHODS
    public SessionLevel getLevel() {
        return level;
    }

    public void close() {
        try {
            if (session != null && session.isOpen()) {
                session.close(CloseStatus.NORMAL);
            }
        } catch (IOException e) {
            logger.warn("Failed to close session", e);
            throw new GameException("Failed to close session");
        }
    }

    public boolean hasQueuedMessages() {
        return messageBuffer.size() > 0;
    }

    public void send() {
        if (messageBuffer.size() > 0) {
            if (session != null && session.isOpen()) {
                try {
                    TextMessage message = messageBuffer.peek();
                    logPerformance(message);
                    session.sendMessage(messageBuffer.peek());
                    messageBuffer.poll();
                } catch (Exception e) {
                    logger.debug("Message could not be delivered, buffering", e);
                }
            } else {
                logger.debug("No open session to send to client, buffering message");
            }
        }
    }

    public WebSocketSession getSession() {
        return session;
    }

    public void setSession(WebSocketSession session) {
        this.session = session;
    }

    private void logPerformance(TextMessage message) {
        MessageWrapper messageWrapper = GSON.fromJson(message.getPayload(), MessageWrapper.class);
        if (messageWrapper.getClientID() != null && PERF_LOG) {
            //PERF_APPENDER.append(new String[]{messageWrapper.getMessageType().toString(), message.asBytes().length + "", "OUT"}, ':');
        }

    }

    public enum SessionLevel {
        PLAYER, HOST, WEB
    }
}
