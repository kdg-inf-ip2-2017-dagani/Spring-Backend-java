package be.kdg.backend.controllers.websocket;


import be.kdg.backend.services.runners.messages.ErrorExceptionMessage;
import be.kdg.backend.services.runners.messages.GameMessageType;
import be.kdg.backend.services.runners.messages.MessageWrapper;
import org.apache.log4j.Logger;
import org.springframework.web.socket.TextMessage;

import static be.kdg.StniklaasbackendApplication.GSON;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.controllers.websocket
 *
 * @author Gaston
 * @version 1.0
 * @since 5/03/2017
 */
public class WebAppSessionHandler extends AbstractSessionHandler {
    private final Logger logger = Logger.getLogger(WebAppSessionHandler.class);

    public WebAppSessionHandler(String gameID) {
        super();
        this.gameID = gameID;
        this.session = null;
        this.level = SessionLevel.WEB;
    }

    @Override
    public void handle(MessageWrapper message) {
        logger.warn("WEBAPP");
        try {
            switch (message.getMessageType()) {
                case WEB_CONNECT:
                    logger.debug("RECEIVED WEB_CONNECT FROM WEBAPP");
                    break;
                case HEARTBEAT:
                    logger.debug("RECEIVED HEARTBEAT FROM WEBAPP");
                    break;
                default:
                    logger.warn("WebApp is not allowed to send messages to server");
                    break;
            }
        } catch (Exception e) {
            logger.warn("Exception in handling message", e);
            send(GameMessageType.ERROR_EXCEPTION, GSON.toJson(new ErrorExceptionMessage<>(e)));
        }
    }

    @Override
    public void send(GameMessageType type, String message) {
        MessageWrapper messageWrapper = new MessageWrapper(type, message, gameID);
        TextMessage textMessage = new TextMessage(GSON.toJson(messageWrapper));

        if (!messageBuffer.offer(textMessage)) {
            logger.warn("Failed to enqueue message: package loss");
            return;
        }
        send();
    }
}