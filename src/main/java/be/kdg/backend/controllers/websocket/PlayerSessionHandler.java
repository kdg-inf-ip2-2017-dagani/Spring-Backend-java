package be.kdg.backend.controllers.websocket;

import be.kdg.backend.services.api.GameService;
import be.kdg.backend.services.api.RunningGameService;
import be.kdg.backend.services.exceptions.UserRunnerException;
import be.kdg.backend.services.runners.messages.*;
import org.apache.log4j.Logger;
import org.springframework.web.socket.TextMessage;

import static be.kdg.StniklaasbackendApplication.GSON;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.controllers.websocket
 *
 * @author Gaston
 * @version 1.0
 * @since 5/03/2017
 */
public class PlayerSessionHandler extends AbstractSessionHandler {
    private final Logger logger = Logger.getLogger(PlayerSessionHandler.class);

    private RunningGameService runningGameService;
    private GameService gameService;
    private String clientID;

    public PlayerSessionHandler(RunningGameService runningGameService, GameService gameService, String gameID, String clientID, SessionLevel level) {
        super();
        this.gameService = gameService;
        this.session = null;
        this.runningGameService = runningGameService;
        this.gameID = gameID;
        this.clientID = clientID;
        this.level = level;
    }


    @Override
    public void handle(MessageWrapper message) {
        logger.debug("handling message of type:" + message.getMessageType());
        try {
            switch (message.getMessageType()) {
                case EVENT:
                    handleEventMessage(message);
                    break;
                case HEARTBEAT:
                    logger.debug("received heartbeat");
                    break;
                case LOCATION:
                    handleLocationMessage(message);
                    break;
                case CONQUERING_START:
                    handleConquerStart(message);
                    break;
                case CONQUERING_END:
                    handleConquerEnd(message);
                    break;
                case PLAYER_UPDATE_NAME:
                    handlePlayerNameChange(message);
                    break;
                case PLAYER_UPDATE_TEAM:
                    handlePlayerTeamChange(message);
                    break;
                default:
                    throw new UserRunnerException("message not recognised");
            }
        } catch (Exception e) {
            logger.warn("Exception in handling message", e);
            send(GameMessageType.ERROR_EXCEPTION, GSON.toJson(new ErrorExceptionMessage<>(e)));
        }
    }

    private void handlePlayerNameChange(MessageWrapper message) {
        String newName = message.getMessage();
        gameService.handlePlayerNameChange(message.getGameID(), message.getClientID(), newName);
    }

    private void handlePlayerTeamChange(MessageWrapper message) {
        String newName = message.getMessage();
        gameService.handlePlayerTeamChange(message.getGameID(), message.getClientID(), newName);
    }

    @Override
    public void send(GameMessageType type, String innerMessage) {
        TextMessage textMessage = new TextMessage(GSON.toJson(new MessageWrapper(type, innerMessage, gameID, clientID)));
        if (!messageBuffer.offer(textMessage)) {
            logger.warn("Failed to enqueue message: package loss");
            return;
        }
        send();
        logger.debug("sent message to player of type " + type.name());
    }

    private void handleEventMessage(MessageWrapper message) {
        GameEventMessage gem = GSON.fromJson(message.getMessage(), GameEventMessage.class);
        logger.debug(gem.getTradePostId() + " tradepostid");

        runningGameService.postEvent(message.getGameID(), message.getClientID(), gem.getType(), gem.getPlayers(), gem.getMoneyTransferred(), gem.getItems(), gem.getTradePostId());
    }

    private void handleLocationMessage(MessageWrapper message) {//is automaticly broadcasted by game runner
        logger.debug("Received location message");
        LocationMessage lm = GSON.fromJson(message.getMessage(), LocationMessage.class);
        runningGameService.updateLocation(message.getGameID(), message.getClientID(), lm.getLatitude(), lm.getLongitude());
    }

    private void handleConquerStart(MessageWrapper message) {
        ConquerMessage cm = GSON.fromJson(message.getMessage(), ConquerMessage.class);
        runningGameService.addPlayerToDistrictConquer(message.getGameID(), message.getClientID(), cm.getLocationID());
    }

    private void handleConquerEnd(MessageWrapper message) {
        ConquerMessage cm = GSON.fromJson(message.getMessage(), ConquerMessage.class);
        runningGameService.removePlayerFromDistrictConquer(message.getGameID(), message.getClientID(), cm.getLocationID());
    }


    public String getPlayerID() {
        return clientID;
    }
}