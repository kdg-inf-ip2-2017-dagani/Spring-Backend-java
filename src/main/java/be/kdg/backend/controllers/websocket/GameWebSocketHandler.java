package be.kdg.backend.controllers.websocket;

import be.kdg.backend.services.api.GameService;
import be.kdg.backend.services.exceptions.GameException;
import be.kdg.backend.services.runners.GameRunner;
import be.kdg.backend.services.runners.messages.MessageWrapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.*;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static be.kdg.StniklaasbackendApplication.*;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.runners
 *
 * @author Gaston
 * @version 1.0
 * @since 4/03/2017
 */
@Component
public class GameWebSocketHandler extends AbstractWebSocketHandler {
    private final Logger logger = Logger.getLogger(GameWebSocketHandler.class);
    //private Map<String,AbstractSessionHandler> sessions;

    @Autowired
    private GameService gameService;

    public GameWebSocketHandler() {
        //sessions = new HashMap<>();
    }


    @Override
    protected void handlePongMessage(WebSocketSession session, PongMessage message){
        try {
            session.close(CloseStatus.NOT_ACCEPTABLE.withReason("Pong messages not supported"));
        }
        catch (IOException ex) {
            // ignore
        }
    }

    @Override
    protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws Exception {
        try{
        ByteBuffer bb = message.getPayload();
        handleTextMessage(session,new TextMessage(bb.array()));
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        if ("CLOSE".equalsIgnoreCase(message.getPayload())) {
            session.close();
        } else {
            logger.debug("Received message");
            logger.debug("Message content \n" + message.getPayload());
            logger.debug(String.format("Session data: %s %s",session.getUri(),session.getRemoteAddress()));
            MessageWrapper messageWrapper = GSON.fromJson(message.getPayload(), MessageWrapper.class);
            logPerformance(message,messageWrapper);
            GameRunner gameRunner = gameService.findGameRunner(messageWrapper.getGameID());
            if (gameRunner == null) {
                session.close();
                throw new GameException("No gamerunner found");
            }
            gameRunner.handle(session,messageWrapper);
        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        super.afterConnectionEstablished(session);
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        logger.trace("Transport error",exception);
        super.handleTransportError(session, exception);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        logger.warn("Socket closed "+ status + " " + status.getReason());
        super.afterConnectionClosed(session, status);
    }

    private void logPerformance(TextMessage message, MessageWrapper messageWrapper) {
        if (messageWrapper.getClientID()!=null && PERF_LOG) {
            //PERF_APPENDER.append(new String[]{messageWrapper.getMessageType().toString(),message.asBytes().length+"","IN"},':');
        }
    }

    //@Scheduled(fixedRate = 1800000)
    @Deprecated
    private void cleanup(){
        /*
        for (String token : sessions.keySet()) {
            if (sessions.get(token).session.isOpen()) continue;
            sessions.remove(token);
        }
        */
    }

}