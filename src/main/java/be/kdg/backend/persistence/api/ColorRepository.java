package be.kdg.backend.persistence.api;

import be.kdg.backend.dom.util.CustomColor;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.persistence.api
 *
 * @author gasto
 * @version 1.0
 * @since 22/02/2017
 */
public interface ColorRepository extends MongoRepository<CustomColor,String> {
}
