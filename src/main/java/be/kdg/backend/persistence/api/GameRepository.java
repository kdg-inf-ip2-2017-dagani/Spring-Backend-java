package be.kdg.backend.persistence.api;

import be.kdg.backend.dom.game.Game;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by gasto
 * 10/02/2017
 */
@Repository
public interface GameRepository extends MongoRepository<Game, String> {

    void deleteGameByRoomName(String roomName);

    Game findGameById(String id);

    Game findGameByRoomName(String roomName);

    List<Game> findGamesByRoomName(String roomName);

}
