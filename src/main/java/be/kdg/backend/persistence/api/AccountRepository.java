package be.kdg.backend.persistence.api;

import be.kdg.backend.dom.account.Account;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by Nikita on 9/02/2017.
 */
public interface AccountRepository extends MongoRepository<Account, String> {
    Account findByEmailAddress(String emailAddress);

    Long deleteAccountByEmailAddress(String emailAddress);

    Account findAccountByUsername(String username);

    Account findAccountByCurrentDeviceId(String currentDeviceId);

    Account findUserByTokenToken(String token);
}
