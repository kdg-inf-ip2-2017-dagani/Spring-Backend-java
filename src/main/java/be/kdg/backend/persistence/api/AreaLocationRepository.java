package be.kdg.backend.persistence.api;

import be.kdg.backend.dom.location.AreaLocation;
import be.kdg.backend.dom.location.AreaType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by gasto
 * 9/02/2017
 */
@Repository
public interface AreaLocationRepository extends MongoRepository<AreaLocation, String> {
    AreaLocation findAreaLocationById(String locationID);

    List<AreaLocation> findAreaLocationByType(AreaType type);
}
