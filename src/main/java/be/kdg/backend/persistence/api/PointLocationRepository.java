package be.kdg.backend.persistence.api;

import be.kdg.backend.dom.location.PointLocation;
import be.kdg.backend.dom.location.PointType;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by gasto
 * 10/02/2017
 */
public interface PointLocationRepository extends MongoRepository<PointLocation, String> {
    //TradePost[] findPointLocationsByType(PointType pointType);

    List<PointLocation> findPointLocationsByPoint_Type(PointType pointType);

}
