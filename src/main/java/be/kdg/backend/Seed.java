package be.kdg.backend;

import be.kdg.backend.dom.account.AccessLevel;
import be.kdg.backend.dom.account.Account;
import be.kdg.backend.dom.account.Token;
import be.kdg.backend.dom.game.Game;
import be.kdg.backend.dom.game.Item;
import be.kdg.backend.dom.location.*;
import be.kdg.backend.dom.util.CustomColor;
import be.kdg.backend.persistence.api.*;
import be.kdg.backend.services.api.ColorService;
import be.kdg.backend.services.api.GameService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikita on 20/02/2017.
 */

@Component
public class Seed {
    private final String EMPTY_FLAVOR_TEXT = "Er is geen wistjedatje";


    private final Logger logger = Logger.getLogger(Seed.class);
    @Autowired
    private ColorService colorService;
    @Autowired
    private ColorRepository colorRepository;
    @Autowired
    private PointLocationRepository pointLocationRepository;
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private GameService gameService;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private AreaLocationRepository areaLocationRepository;

    private Game defaultGame;

    private void insertBanks() {
        logger.debug("Inserting Banks");
        PointLocation bank1 = new PointLocation(new Point(51.159740, 4.149361), PointType.BANK, "KBC St-Niklaas Centrum");
        pointLocationRepository.save(bank1);
        PointLocation bank2 = new PointLocation(new Point(51.167187, 4.133138), PointType.BANK, "KBC Vitas Group");
        pointLocationRepository.save(bank2);
        PointLocation bank3 = new PointLocation(new Point(51.169030, 4.142257), PointType.BANK, "KBC Stationstraat");
        pointLocationRepository.save(bank3);
        List<PointLocation> banks = new ArrayList<>();
        banks.add(bank1);
        banks.add(bank2);
        banks.add(bank3);
        defaultGame.setBanks(banks);
    }

    private void insertCustomColors() {
        logger.debug("Inserting Colors");

        colorRepository.deleteAll();

        ArrayList<CustomColor> colors = new ArrayList<>();
        colors.add(new CustomColor("#3F51B5","blue"));
        colors.add(new CustomColor("#4CAF50","green"));
        colors.add(new CustomColor("#E91E63","magenta"));
        colors.add(new CustomColor("#FFC107","yellow"));
        colors.add(new CustomColor("#FF9800","orange"));
        colors.add(new CustomColor("#9C27B0","purple"));
        colors.add(new CustomColor("#E53935","red"));
        colors.add(new CustomColor("#E0E0E0","grey"));

        for (CustomColor color : colors) {
            colorRepository.insert(color);
        }
    }

    private void insertDefaultAccount() {
        logger.debug("Inserting Default Account");
        Account a = accountRepository.findByEmailAddress("default@host.com");
        if (a != null) {
            a.setPassword("test");
            a.setUsername("defaultHost");
            a.setAccessLevel(AccessLevel.HOST);
        } else {
            a = accountRepository.save(new Account("defaultHost", "test", "default@host.com", AccessLevel.HOST));
        }
        Token token = new Token(a);
        a.setToken(token);
        accountRepository.save(a);
    }

    private void insertDistricts() {
        logger.debug("Inserting Districts");
        AreaLocation districtGroteMarkt = areaLocationRepository.save(new AreaLocation(
                new Point[]{
                        new Point(51.164404, 4.138955, 0),
                        new Point(51.165199, 4.139859, 1),
                        new Point(51.164937, 4.141007, 2),
                        new Point(51.163301, 4.141691, 3),
                        new Point(51.163087, 4.141356, 4)
                },
                AreaType.MARKET,
                "Grote Markt"
        ));
        AreaLocation district1a = areaLocationRepository.save(new AreaLocation(
                new Point[]{
                        new Point(51.163204, 4.141742, 0),
                        new Point(51.164348, 4.145379, 1),
                        new Point(51.163655, 4.147578, 2),
                        new Point(51.162868, 4.148769, 3),
                        new Point(51.161791, 4.148029, 4),
                        new Point(51.160486, 4.150218, 5),
                        new Point(51.160076, 4.148844, 6),
                        new Point(51.161637, 4.145636, 7),
                        new Point(51.162747, 4.146580, PointType.TREASURE)
                },
                AreaType.DISTRICT_A,
                "1A"
        ));
        AreaLocation district2a = areaLocationRepository.save(new AreaLocation(
                new Point[]{
                        new Point(51.164903, 4.141451, 0),
                        new Point(51.168743, 4.142419, 1),
                        new Point(51.168470, 4.144602, 2),
                        new Point(51.168615, 4.144844, 3),
                        new Point(51.168605, 4.145192, 4),
                        new Point(51.168447, 4.145450, 5),
                        new Point(51.168353, 4.145461, 6),
                        new Point(51.167979, 4.148234, 7),
                        new Point(51.166062, 4.147134, 8),
                        new Point(51.168370, 4.145031, PointType.TREASURE)

                },
                AreaType.DISTRICT_A,
                "2A"
        ));
        AreaLocation district3a = areaLocationRepository.save(new AreaLocation(
                new Point[]{
                        new Point(51.163919, 4.139593, 0),
                        new Point(51.161489, 4.135498, 1),
                        new Point(51.161220, 4.132111, 2),
                        new Point(51.162270, 4.131950, 3),
                        new Point(51.163985, 4.133817, 4),
                        new Point(51.165122, 4.131253, 5),
                        new Point(51.166448, 4.133055, 6),
                        new Point(51.165257, 4.135415, 7),
                        new Point(51.165889, 4.136145, 8),
                        new Point(51.163743, 4.135104, PointType.TREASURE)

                },
                AreaType.DISTRICT_A,
                "3A"
        ));
        AreaLocation district4a = areaLocationRepository.save(new AreaLocation(
                new Point[]{
                        new Point(51.163919, 4.139593, 0),
                        new Point(51.163041, 4.141444, 1),
                        new Point(51.161477, 4.142002, 2),
                        new Point(51.160930, 4.138223, 3),
                        new Point(51.161489, 4.135498, 4),
                        new Point(51.161966, 4.138899, PointType.TREASURE)

                },
                AreaType.DISTRICT_A,
                "4A"
        ));
        AreaLocation district5a = areaLocationRepository.save(new AreaLocation(
                new Point[]{
                        new Point(51.169287, 4.138398, 0),
                        new Point(51.169065, 4.140297, 1),
                        new Point(51.168587, 4.142089, 2),
                        new Point(51.165089, 4.141123, 3),
                        new Point(51.165506, 4.138312, 4),
                        new Point(51.166017, 4.137486, 5),
                        new Point(51.166044, 4.136885, 6),
                        new Point(51.167315, 4.136993, 7),
                        new Point(51.169068, 4.139418, PointType.TREASURE)
                },
                AreaType.DISTRICT_A,
                "5A"
        ));
        AreaLocation district6a = areaLocationRepository.save(new AreaLocation(
                new Point[]{
                        new Point(51.163041, 4.141444, 0),
                        new Point(51.159855, 4.148423, 1),
                        new Point(51.158604, 4.144893, 2),
                        new Point(51.158166, 4.144475, 3),
                        new Point(51.157978, 4.144078, 4),
                        new Point(51.158139, 4.142823, 5),
                        new Point(51.157318, 4.139743, 6),
                        new Point(51.160881, 4.138408, 7),
                        new Point(51.161477, 4.142002, 8),
                        new Point(51.161376, 4.144105, PointType.TREASURE)
                },
                AreaType.DISTRICT_A,
                "6A"
        ));
        AreaLocation district1b = areaLocationRepository.save(new AreaLocation(
                new Point[]{
                        new Point(51.162868, 4.148769, 0),
                        new Point(51.163056, 4.148533, 1),
                        new Point(51.164336, 4.154726, 2),
                        new Point(51.163085, 4.155756, 3),
                        new Point(51.163865, 4.157735, 4),
                        new Point(51.162654, 4.158975, 5),
                        new Point(51.160340, 4.155177, 6),
                        new Point(51.162668, 4.148546, 7),
                        new Point(51.162533, 4.156400, PointType.TREASURE)
                },
                AreaType.DISTRICT_B,
                "1B"
        ));
        AreaLocation district2b = areaLocationRepository.save(new AreaLocation(
                new Point[]{
                        new Point(51.168615, 4.144844, 0),
                        new Point(51.171319, 4.145735, 1),
                        new Point(51.171158, 4.145220, 2),
                        new Point(51.171970, 4.144330, 3),
                        new Point(51.172276, 4.145982, 4),
                        new Point(51.172071, 4.147321, 5),
                        new Point(51.172525, 4.148828, 6),
                        new Point(51.172226, 4.150620, 7),
                        new Point(51.167979, 4.148234, 8),
                        new Point(51.168353, 4.145461, 9),
                        new Point(51.168447, 4.145450, 10),
                        new Point(51.171437, 4.145058, PointType.TREASURE)
                },
                AreaType.DISTRICT_B,
                "2B"
        ));
        AreaLocation district3b = areaLocationRepository.save(new AreaLocation(
                new Point[]{
                        new Point(51.165122, 4.131253, 0),
                        new Point(51.165593, 4.129375, 1),
                        new Point(51.166306, 4.128624, 2),
                        new Point(51.169603, 4.138076, 3),
                        new Point(51.169287, 4.138398, 4),
                        new Point(51.167315, 4.136993, 5),
                        new Point(51.166044, 4.136885, 6),
                        new Point(51.167490, 4.133441, 7),
                        new Point(51.167188, 4.132711, 8),
                        new Point(51.166448, 4.133055, 9),
                        new Point(51.166596, 4.131875, PointType.TREASURE)
                },
                AreaType.DISTRICT_B,
                "3B"
        ));
        AreaLocation district4b = areaLocationRepository.save(new AreaLocation(
                new Point[]{
                        new Point(51.160881, 4.138408, 0),
                        new Point(51.157318, 4.139743, 1),
                        new Point(51.156315, 4.139593, 2),
                        new Point(51.156087, 4.133177, 3),
                        new Point(51.161220, 4.132111, 4),
                        new Point(51.161489, 4.135498, 5),
                        new Point(51.158546, 4.135769, PointType.TREASURE)
                },
                AreaType.DISTRICT_B,
                "4B"
        ));
        AreaLocation district5b = areaLocationRepository.save(new AreaLocation(
                new Point[]{
                        new Point(51.169287, 4.138398, 0),
                        new Point(51.169603, 4.138076, 1),
                        new Point(51.171816, 4.143880, 2),
                        new Point(51.170881, 4.145275, 3),
                        new Point(51.169839, 4.145093, 4),
                        new Point(51.169993, 4.142443, 5),
                        new Point(51.168587, 4.142089, 6),
                        new Point(51.169065, 4.140297, 7),
                        new Point(51.170518, 4.142647, PointType.TREASURE)
                },
                AreaType.DISTRICT_B,
                "5B"
        ));
        AreaLocation district6b = areaLocationRepository.save(new AreaLocation(
                new Point[]{
                        new Point(51.159855, 4.148423, 0),
                        new Point(51.157283, 4.152573, 1),
                        new Point(51.156112, 4.149847, 2),
                        new Point(51.155742, 4.147391, 3),
                        new Point(51.155096, 4.139806, 4),
                        new Point(51.157318, 4.139743, 5),
                        new Point(51.158139, 4.142823, 6),
                        new Point(51.157978, 4.144078, 7),
                        new Point(51.158166, 4.144475, 8),
                        new Point(51.158604, 4.144893, 9),
                        new Point(51.156307, 4.146307, PointType.TREASURE)
                },
                AreaType.DISTRICT_B,
                "6B"
        ));
        defaultGame.getMarkets().add(districtGroteMarkt);
        List<AreaLocation> districts = defaultGame.getDistricts();
        //districts.add(districtGroteMarkt);
        districts.add(district1a);
        districts.add(district2a);
        districts.add(district3a);
        districts.add(district4a);
        districts.add(district5a);
        districts.add(district6a);
        districts.add(district1b);
        districts.add(district2b);
        districts.add(district3b);
        districts.add(district4b);
        districts.add(district5b);
        districts.add(district6b);
    }

    private void insertFirstGame() {
        logger.debug("Inserting First Game");
        Account account = accountRepository.findByEmailAddress("default@host.com");
        Game template = gameService.findGameByRoomName("defaultGame");
        String gameId = gameService.addGame(account, "firstGame", 4, 4, "pw");
    }

    private void insertTradePosts() {
        logger.debug("Inserting Trade Posts");

        Item ijs = new Item("Kg ijs", 13, 11, 10, 8);
        Item textiel = new Item("Rol textiel", 20, 17, 15, 12);
        Item bier = new Item("Vat bier", 10, 9, 8, 7);
        Item bloemen = new Item("Dozijn bloemen", 6, 5, 5, 4);
        Item bakstenen = new Item("Koets bakstenen", 28, 25, 20, 17);
        Item artDeco = new Item("Art deco", 40, 36, 29, 25);

        List<PointLocation> tradePostList = new ArrayList<>();

        String foubertText, fabrieckText, textielText, brouwerijText, villaText, francoisText, bloemistText, svkText, dirkenText, artdecoText;
        foubertText = "Vinyl in Sint-Niklaas\n" +
                "In 2010 begon Crèmerie Foubert als de dessertkeuken van ‘’t Centerke’. ‘t Centerke haalt zijn naam van de grootste platenzaak van België die vroeger in de gebouwen gehuisvest zat. De zaak liep van de Stationsstraat tot in de Prins Albertstraat. De naam komt van het schijfje dat gebruikt werd op een platenspeler om een ‘singeltje’ af te spelen, aangezien die een groter gat hadden in het midden dan een LP plaat. Dat schijfje heette een ‘centerken’. \n" +
                " \n" +
                "De koude oorlog op zijn Sinnekloases\n" +
                "In Sint-Niklaas is ijs zoals voetbal in andere steden. De rivaliteit tussen Cremerie Foubert en Cremerie François is als de vete tussen den Antwerp en den Beerschot in Antwerpen: het draait rond ballen. De ploeg met traditie is hier François met zijn meer dan 85 jarig bestaan. Deze wordt nu uitgedaagd door de jonge Wase wolven van Foubert. Gelukkig wordt in deze koude oorlog niet gevochten en hebben de slimme Sint-Niklazenaren de oplossing gevonden: van beide walletjes eten. Proef gerust mee en vel je oordeel. Teleurgesteld kun je amper zijn want het Sint-Niklase ijs is het lekkerste van België.\n";

        fabrieckText = "In stijl\n" +
                "Een verborgen Sint-Niklase parel waar je je even op een van de hipste plekken van Gent of Antwerpen waant. In deze voormalige textielfabriek/bonnetterie is de stadspelapp die je nu aan het spelen bent feestelijk gelanceerd. Het pand  is in het begin van de 20ste eeuw gebouwd, maar was een aantal decenia later al in ongebruik geraakt van zijn oorspronkelijke functie. Recent wordt het gebruikt door de Sint-Niklase ‘MAN-architecten’  die het gebouw hebben gerenoveerd tot hun eigen architectenbureau en een feestzaaltje ‘Fabrieck’ dat beschikbaar is voor allerlei activiteiten. Zeker de moeite om eens binnen een kijkje te nemen!\n" +
                "De Kalkstraat\n" +
                "De Kalkstraat is een van de oudere straten van de stad. In 1436 veranderde de straat van naam van de ‘Strate naer Temsche’ naar de Kalkstraat, vernoemd naar een zekere “Vrouwe van Calckenen”. Door een brand die hier in 1690 startte, lag de helft van het stadscentrum in as.\n" +
                "\n" +
                "Textielstad tot in het diepst van zijn vezel\n" +
                "Al van den beginne was Sint-Niklaas voorbestemd om een textielstad te worden. Het Waasland was een natte streek die ideaal was voor de vlasteelt. In vochtige putten werd het vlas gelegd om het te ‘roten’, een soort rottingsproces waardoor de bruikbare vezel vrijkwam. Een vies klusje dat vele arbeiders ziek maakte en de rivieren goud van de vervuiling kleurde.  Het verwerkte vlas werd dan verhandeld op de Grote Markt om in de vele Wase ateliers textiel te maken. Hierdoor bouwde Sint-Niklaas veel kennis op in de ambacht en werd  vlas ‘de rijkdom van het Waasland’ genoemd.\n";

        textielText = "De grote meneer\n" +
                "Hier ben je aanbeland bij een fabriek van een van de grote jongens in de Sint-Niklase textiel. Edmond Meert  begon in 1885 op deze plek met zijn fabriek. De fabriek specialiseerde zich voornamelijk in het ontwerpen, weven en verven van tapijten, maar deed nog heel wat meer. De fabriek  breidde fenomenaal uit en nam met zijn 148 weefgetouwen het volledige bouwblok tussen Lam-, Hogenakker- en Lindenstraat in.  Ook steeg aantal werknemers tot meer dan 1000. Samen met deze groei , steeg ook het kapitaal van de famillie Meert naar ongekende hoogtes. Met hun geld stichtte ze links en rechts nieuwe fabrieken, villa’s en kastelen. Hof Ter Saksen in Beveren maar ook ‘Het huis van de Sint’ (de Salons) in de Stationsstraat zijn met hun fortuin gebouwd. In de tweede wereldoorlog werden de tentzeilen van de gealieerden in deze fabriek gemaakt.  Momenteel zijn in de oude kantoorgebouwen van de fabriek de intercommunale van het Waasland gevestigd (InterWaas).\n" +
                " \n" +
                "Textiel: van dorp tot stad  \n" +
                "Door de opkomst van grote machines in de indrustriële revolutie werd in Sint-Niklaas de textielambacht vervormt tot een grootschalige industrie. De handel kreeg een extra boost door de aanleg van de trein tussen de industriesteden Antwerpen en Gent enerzijds en die tussen Mechelen en Terneuzen. Het is waarschijnlijk die snelle treinverbinding en kruising van spoorwegen die Sint-Niklaas écht groter hebben gemaakt dan pakweg Temse of Dendermonde.  Overal in en buiten het centrum schoten kleine en grote textielfabrieken/bonneterie zaken uit de grond als paddenstoelen. Grote textielfabrieken waren er tot diepst van de stad, zoals bv. zwijgershoek, de naam van een café bij een van de grootste fluweelweverijen van Europa die op het huidige plein stond, of de vts-site bij het OLV-plein waar een nieuwe woonwijk is gepland. Ook straten zoals de Stationsstraat en de Mercatorstraat gonste van de textielnijverheid.\n";

        brouwerijText = "“We zullen oa rijn de Nustoat in”\n" +
                "Zin uit een volksliedje. Als iemand dit tegen u zegt, dan wil dit niet zeggen dat hij/zij met u een pint wilt gaan drinken in ’t Stamcafeke of een koffie in de Brainfreeze comicstore. De persoon wil u duidelijk maken dat je van uw lotje getikt bent en dat ze u willen gaan afzetten bij de psychiatrisch centrum Sint-Hyronimus. Als je er om gelijk welke reden echt zou terecht komen, toch nog deze gerustelling: de instelling kreeg in De Morgen  recent nog een uitstekend rapport voor zijn werking. \n" +
                "\n" +
                "Bier in Sint-Niklaas\n" +
                "Je kent het wel: het gouden gerstennat. De duivel en koning vervat in een glas. Niet dat het bier anders smaakt in Sint-Niklaas dan elders, proef toch maar eens voor de zekerheid eentje in de vele cafés in het stadscentrum. De stad heeft helaas geen eigen bierbrouwerij meer. De deelgemeentes Belsele en Nieuwkerken hebben er gelukkig wel nog.  Geheel in de Sinterklaas traditie brouwt Brouwerij Boelens jaarlijks rond 6 december het Sint- en Pietenbier. Je mag zelf raden het welke het blonde bier is, en het welke het donkere. \n";

        villaText = "De bizarre link tussen kousen en brandweermannen\n" +
                "Jawel, dit is een van de klassieke toeristische hoogtepunten in Sint-Niklaas: Villa Verbreyt. Een van de parels van de Belgische Art-Deco architectuur. De famillie Verbreyt -die het huis liet zetten- vergaarde fortuin door hun kousenfabriek in de Nijverhijdstraat.  De voor die tijd hypermoderne fabriek met elektrische aansturing en vooruitstrevende architectuur was op zijn hoogtepunt de grootste kousenfabriek van het land met ongeveer 1000 werknemers.  In 1973 ging de fabriek onder de toenemende concurentie van het buitenland failliet. De fabriek werd verbouwd tot de hallen van de brandweerkazerne van de stad. \n" +
                "\n" +
                "Wonder van de art-deco\n" +
                "Nog niet overtuigd van de architecturale kwaliteit van de woning? Let dan goed op! Bewonder de sterk variërende gevelcompositie met in- en uitspringende partijen en verspringende bouwlagen afgelijnd met baksteenlijsten. Ervaar de straatzijde gemarkeerd door vensterregisters van vierlichten die een streling voor het oog zijn. Ervaar het vernuft van de centrale inspringende partij, met driezijdige erker met overkragende bovenverdieping met driehoekig balkon voor het centraal drielicht. Maar er is meer: een verzorgd baksteenmetselwerk met diverse verbanden als staand en recht blokverband en overhoeks geplaatste bakstenen voor een aantal schuine hoekafwerkingen. Alsof dit nog niet genoeg is , vormen deze samen met de schuin uitspringende rechterpartij met deur in links afgeschuinde hoek de kers op de rijkelijk  gevulde architecturale taart.  Nog altijd niet overtuigd? Jammer maar helaas!\n";

        francoisText = "De aller oudste\n" +
                "Een grote uitleg zou hier op zien plaats zijn, maar waarom moeilijk doen als het makkelijk gaat?\n" +
                "Crèmerie François is met zijn 89 jaarde het oudste ijssalon van het land, en is ook al drie keer verkozen tot dé beste crèmerie van het land. Eat that!\n" +
                "\n" +
                "Was het Anckerstraat of Lange Nieuwstraat?\n" +
                "Oorspronkelijk noemde de Ankerstraat de ‘Lange Nieuwstraat’. De straat liep eigenlijk door tot aan de andere kant van De Grote Markt, waar de straat ‘Korte Nieuwstraat’ heette. Twee Nieuwstraten, dat zorgde meer dan eens voor misverstanden FC De Kampioenen waardig. Daarom besloot men  dat er minimum een van naam moest veranderen. Omdat er in de Lange Nieuwstraat op de hoek met de  Zamenstraat een bekende stadsbrouwerij  ‘Den Ancker’ was, werd er daar voor de Anckerstraat gekozen. Op die zelfde hoek waar de brouwerij was, is nu trouwens nog een van de best bewaarde winkelinterieurs van Likeurhandel De Walvis. In hun vitrine staat het Anker waarnaar de Ankerstraat is vernoemd.\n" +
                "\n" +
                "De koude oorlog  op zijn Sint-Niklaases \n" +
                "In Sint-Niklaas is ijs zoals voetbal in andere steden. De rivaliteit tussen cremerie Foubert en Cremerie François is als de vete tussen den Antwerp en den Beerschot in Antwerpen: het draait rond ballen. De ploeg met traditie is hier François met zijn meer dan 85 jarig bestaan. Deze wordt nu uitgedaagd door de jonge Wase wolven van Foubert. Gelukkig wordt in deze koude oorlog niet gevochten en hebben de slimme Sint-Niklazenaren de oplossing gevonden: van beide walletjes eten. Proef gerust mee en vel je oordeel. Teleurgesteld kun je amper zijn want het Sint-Niklase ijs is het lekkerste ijs van België.\n";

        bloemistText = "Been there, done that!\n" +
                "De bloemenwinkel van Daniel en Nele Ost lijkt een stijlvolle maar redelijk gewone winkel in het Sint-Niklase stadscentrum. Niets is echter minder waar. Deze bloemenwinkel behoort tot de absolute wereldtop als het op bloemsierkunst aankomt. Grootse koninklijke huwelijken in het Midden Oosten, beschermd erfgoed in Japan, het ontwerp van een postzegel voor de Britse post, Catwalks in Parijs. Daniel en Nele Ost kunnen zeggen: been there, done that!\n";

        svkText = "‘Statie West’ en Gustave Eiffel\n" +
                "In 1877 werd hier op het Westerplein het strategisch belangrijke station ‘Sint-Niklaas-West’ geopend. Deze lag op de spoorverbinding Mechelen-Terneuzen  en had een dubbel doel. Enerzijds moest het de steenkool van Charleroi naar het noorden zien te krijgen tot aan de haven van Terneuzen. Anderzijds diende het ook om  het internationale transport van goederen aan te zwengelen. De lijn was voor Sint-Niklaas economisch gezien zo belangrijk dat het gemeentebestuur een belangrijk deel van de spoorweg financierde. Zelfs Gustave Eiffel (ja, die van die toren) kwam speciaal uit Frankrijk afgezakt om de spoorlijn over De Schelde te krijgen. Dit deed hij met zijn ontwerp voor de Temsebrug. De brug is tot op vandaag nog steeds de langste Belgische brug over water!\n" +
                "\n" +
                "De industriële doorzetter \n" +
                "Men zegt dat de Vlaming met een baksteen in de maag is geboren. In het geval van de Sint-Niklazenaren is de kans groot dat die bakstenen van Scheerders Van Kerchoven(SVK) zijn. Waar de textiel en metaalindustrie quasi volledig de stad hebben verlaten, is SVK is als producent en verhandelaar van bouwmaterialen nog een van de weinige industriële spelers die grote  stukken grond in het stadscentrum gebruikt voor zijn bedrijfsvoering.\n";

        dirkenText = "De straat: Breed = rijk\n" +
                "De Monseigneur Stillemansstraat is een van de weinige straten met grote historische architecturale kwaliteiten die in zijn geheel nog redelijk onaangetast is. Waar op De Markt het ene na het andere appartementsblok in de plaats komt van een historisch pand, is dit hier niet het geval. Deze straat is de plek bij uitstek waar de rijke Sint-Niklazenaren in het interbellum met hun rijkdom te koop liepen. Let op de breedte van de huizen in de Monseigneur Stillemansstraat en vergelijk deze met bijvoorbeeld met de huizen in de achterliggende Kongostraat of andere huizen in het centrum. Je merkt direct op waar de elite, en waar de arbeiders woonden. Als je die breedte van huizen  in stadcentra van bijvoorbeeld Mechelen, Gent of Antwerpen, vergelijkt met die van Sint-Niklaas, dan zal je merken dat Sint-Niklaas doorheen zijn geschiedenis minder rijke inwoners heeft gehad dan andere centrumsteden.\n" +
                "\n" +
                "\n" +
                "HET HUIS\n" +
                "Dit huis is gebouwd door August Waterschoot, die tussen 1896 en 1936 40(!) jaar stadsarchitect van Sint-Niklaas was. De gevel van de woning is quasi intact gebleven en de poort aan de rechtkant van de gevel doet vermoeden dat de eigenaar een garage én auto had, wat betekent dat de eigenaar van het huis  voor die tijd best rijk moet zijn geweest.\n";

        artdecoText = "Sint-Niklaas art-deco stad\n" +
                "Sint-Niklaas is de art-deco stad. Sint-Niklaas is een redelijk recent ontwikkelde stad die ondanks zijn 800 jaar bestaan toch vooral recent een bloei kende. In het bijzonder in het begin van de 20ste eeuw wanneer allerlei fabrieken komen in Sint-Niklaas. Met de bloei van de (textiel)handel in het interbellum (periode tussen de twee wereldoorlogen) in Sint-Niklaas, wilden de rijke handelaren met hun rijkdom uitpakken. Dit deden ze vaak door een mooie, modieuze art-deco woning te bouwen. Als er een grote poort in een art-decowoning zit, betekent dit meestal dat er vroeger handelswaren als textiel werden opgeslagen in de gebouwen. \n";


        List<Item> items = new ArrayList<>();
        items.add(ijs);
        PointLocation t1 = new TradePost(new Point(51.169864, 4.142617, PointType.TRADE_POST), "Foubert/centerken",
                items, foubertText);
        pointLocationRepository.save(t1);


        items = new ArrayList<>();
        items.add(textiel);
        PointLocation t2 = new TradePost(new Point(51.163496, 4.147791, PointType.TRADE_POST),
                "Fabrieck (Textielfabriek), MAN architecten", items, fabrieckText);
        pointLocationRepository.save(t2);


        PointLocation t3 = new TradePost(new Point(51.164086, 4.154452, PointType.TRADE_POST),
                "Interwaas", items, textielText);
        pointLocationRepository.save(t3);


        items = new ArrayList<>();
        items.add(bier);
        PointLocation t4 = new TradePost(new Point(51.161379, 4.135422, PointType.TRADE_POST),
                "Brouwerij den Dubbelen Arend", items, brouwerijText);
        pointLocationRepository.save(t4);


        items = new ArrayList<>();
        items.add(artDeco);
        PointLocation t5 = new TradePost(new Point(51.168943, 4.136236, PointType.TRADE_POST),
                "Villa Verbreyt", items, villaText);
        pointLocationRepository.save(t5);


        items = new ArrayList<>();
        items.add(ijs);
        PointLocation t6 = new TradePost(new Point(51.166148, 4.144850, PointType.TRADE_POST),
                " Francois", items, francoisText);
        pointLocationRepository.save(t6);


        items = new ArrayList<>();
        items.add(bloemen);
        PointLocation t7 = new TradePost(new Point(51.165651, 4.136857, PointType.TRADE_POST),
                "Bloemist Daniel Ost", items, bloemistText);
        pointLocationRepository.save(t7);


        items = new ArrayList<>();
        items.add(bakstenen);
        PointLocation t8 = new TradePost(new Point(51.165760, 4.129944, PointType.TRADE_POST),
                "SVK", items, svkText);
        pointLocationRepository.save(t8);


        items = new ArrayList<>();
        items.add(artDeco);
        PointLocation t9 = new TradePost(new Point(51.161186, 4.140271, PointType.TRADE_POST),
                "Woning Dirken Monsieur", items, dirkenText);
        pointLocationRepository.save(t9);


        PointLocation t10 = new TradePost(new Point(51.158817, 4.144397, PointType.TRADE_POST),
                "Art-Deco huis", items, artdecoText);
        pointLocationRepository.save(t10);


        tradePostList.add(t1);
        tradePostList.add(t2);
        tradePostList.add(t3);
        tradePostList.add(t4);
        tradePostList.add(t5);
        tradePostList.add(t6);
        tradePostList.add(t7);
        tradePostList.add(t8);
        tradePostList.add(t9);
        tradePostList.add(t10);
        defaultGame.setTradePosts(tradePostList);
    }

    public void init() {
        logger.debug("#########################################################################");
        logger.debug("################################ SEEDING ################################");
        logger.debug("#########################################################################");
        insertCustomColors(); //inserts all needed colors and removes unwanted
        insertDefaultAccount(); //redefines or adds a default account
        insertDefaultGame();
        //insertFirstGame();
    }

    public void insertDefaultGame() {
        List<Game> games = gameRepository.findGamesByRoomName("defaultGame");
        boolean areaOk = false, pointOk = false;
        if (games.size() == 1) {
            Game g = games.get(0);
            if (g.getMarkets().size()+g.getDistricts().size() == 13) areaOk = true;
            if (g.getTradePosts().size()+g.getBanks().size() == 13) pointOk = true;
        }

        if (areaOk&&pointOk) return;//all is good in the world

        gameRepository.deleteAll();
        pointLocationRepository.deleteAll();
        areaLocationRepository.deleteAll();
        logger.debug("Inserting Default game");
        defaultGame = gameRepository.insert(new Game(null, "defaultGame", 5, 2, "test"));
        insertDistricts();
        insertTradePosts();
        insertBanks();
        gameRepository.save(defaultGame);
    }
}