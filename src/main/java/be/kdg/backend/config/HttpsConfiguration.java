package be.kdg.backend.config;

import org.springframework.context.annotation.Configuration;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.config
 *
 * @author Gaston
 * @version 1.0
 * @since 18/03/2017
 */
@Configuration
public class HttpsConfiguration {
/*
    @Bean
    public EmbeddedServletContainerFactory servletContainer() {

        TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory() {
            @Override
            protected void postProcessContext(Context context) {
                SecurityConstraint securityConstraint = new SecurityConstraint();
                securityConstraint.setUserConstraint("CONFIDENTIAL");
                SecurityCollection collection = new SecurityCollection();
                collection.addPattern("/*");
                securityConstraint.addCollection(collection);
                context.addConstraint(securityConstraint);
            }
        };

        tomcat.addAdditionalTomcatConnectors(initiateHttpConnector());
        return tomcat;

    }

    private Connector initiateHttpConnector() {
        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        connector.setScheme("http");
        connector.setPort(8081);
        //connector.setPort(8090);

        connector.setSecure(false);
        //connector.setRedirectPort(8443);
        connector.setRedirectPort(8090);

        return connector;
    }
    */
}