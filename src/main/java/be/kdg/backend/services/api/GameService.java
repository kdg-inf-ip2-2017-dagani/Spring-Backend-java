package be.kdg.backend.services.api;

import be.kdg.backend.controllers.websocket.AbstractSessionHandler;
import be.kdg.backend.dom.account.Account;
import be.kdg.backend.dom.game.Game;
import be.kdg.backend.dom.game.GameState;
import be.kdg.backend.dom.game.Player;
import be.kdg.backend.dom.game.Team;
import be.kdg.backend.services.runners.GameRunner;
import be.kdg.backend.services.runners.messages.MessageWrapper;
import be.kdg.backend.services.runners.messages.WinningTeamMessage;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import java.util.List;

/**
 * Created by gasto
 * 10/02/2017
 */
@Service
public interface GameService {
    String addDistrict(String token, String gameId, String districtId);

    String addBank(String token, String gameId, String bankId);

    String addGame(Account token, String roomName, int teams, int players, String hostPassword);

    void closeGame(Game game);

    void deleteGameById(String token, String gameId);

    void deleteDistrict(String hostToken, String gameID, String id);

    Game findGameById(String id);

    void addPlayerPassword(String token, String gameID, String password);

    void addHostPassword(String token, String gameID, String password);

    Game findGameByRoomName(String roomName);

    Game findStagedGameByRoomName(String roomName);

    WinningTeamMessage getFinalGameStats(String gameId);

    String registerWebApp(String gameID);

    void removeGameRunner(String id);

    void restartGame(Account a, String gameId, int seconds);

    void saveGameSettings(String token, Game g);

    void startGame(String token, String gameID);


    String registerUser(String gameID, String clientID, String name, String password);

    Player findPlayerByClientId(String gameId, String clientID);

    void banPlayer(String token, String clientID, String gameId);

    void stopIfGameRunning(String gameId);

    void unregisterUser(String gameID, String clientID);

    void removeAllGames();

    String addTradePost(String token, String gameId, String tradePostId);

    void updateGame(String token, Game game);

    Game findRunningGameById(String gameID);

    Game findStagedGameById(String gameID);

    List<Game> findAllRunningGames();

    List<Game> findAllStagedGames();

    List<Game> findAllGames();

    void cancelGame(String hostToken, String id);

    void clearStagedGames();

    boolean isStaged(String gameID);

    String registerWebApp(String gameID, String token);

    boolean isRunning(String gameID);

    String getClientToken(String gameID, String clientID);

    AbstractSessionHandler handleNewSession(WebSocketSession session, MessageWrapper message);

    Team findTeamByClientId(String game1, String clientID);

    GameRunner findGameRunner(String gameID);

    void handlePlayerNameChange(String gameID, String clientID, String newName);

    void handlePlayerTeamChange(String gameID, String clientID, String newTeam);

    void changeDuration(String token, String gameId, int minutes);

    void kickPlayer(String token, String gameId, String playerToKick);

    GameState getGameState(String gameID);

    Game saveGame(Game game);
}
