package be.kdg.backend.services.api;


import be.kdg.backend.dom.account.AccessLevel;
import be.kdg.backend.dom.account.Account;

import java.util.List;

/**
 * Created by Nikita on 9/02/2017.
 */
public interface AccountService {
    String addAccount(Account account);

    Account findAccountByEmailAddress(String emailAddress);

    Account findAccountByCurrentDeviceId(String deviceId);

    void deleteAccountByEmailAddress(String emailAddress);

    List<Account> findAccounts();

    Account updateAccessLevel(String id, AccessLevel accessLevel);

    String checkLogin(String id, String password);

    Account findAccountByUsername(String username);

    Account validateToken(String token);

    void removeAllAccounts();
}
