package be.kdg.backend.services.api;

import be.kdg.backend.dom.game.Item;
import be.kdg.backend.dom.location.*;

import java.util.List;

/**
 * Created by gaston
 * 9/02/2017
 */
public interface LocationService {
    List<PointLocation> findPointsByType(PointType pointLocationType);

    String addAreaLocation(String name, Point[] points, AreaType type);

    List<AreaLocation> findAreas();

    List<AreaLocation> findAreasByType(AreaType areaType);


    //void updateAreaByID(String id, String name, Point[] points, AreaType type);

    //void deleteAreaByID(String id);

    String addPointLocation(String name, Point point, PointType pointType);

    PointLocation findPointByID(String id);

    /*PointLocation[] findPoints();

    void updatePointByID(String id, String name, Point point, PointType type);

    void deletePointByID(String id);*/

    String addTradePost(String name, Point point, List<Item> items, String flavorText);

    TradePost findTradepostById(String id);

    /*TradePost[] findTradeposts();

    void updateTradepostByID(String id, String name, Point point, List<Item> items);

    void deleteTradepostByID(String id);*/

    void removeAllAreaLocations();

    void removeAllPointLocations();

    void removeAllLocations();

    AreaLocation findAreaLocationById(String districtId);
}
