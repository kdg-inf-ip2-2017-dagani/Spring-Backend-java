package be.kdg.backend.services.api;

import be.kdg.backend.dom.game.Game;
import be.kdg.backend.dom.game.GameEventType;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.impl
 *
 * @author gasto
 * @version 1.0
 * @since 10/03/2017
 */
@Service
public interface RunningGameService {
    void stopGame(String token, String gameID);

    void updateLocation(String gameID, String clientID, double lat, double lon);

    void postEvent(String gameID, String clientID, GameEventType type, List<String> involvedPlayers, double moneyTransferred, Map<String, Integer> itemsTransferred, String locationID);

    void endGame(String hostToken, String id);

    void clearRunningGames();

    void addPlayerToDistrictConquer(String gameID, String clientID, String locationID);

    void removePlayerFromDistrictConquer(String gameID, String clientID, String locationID);

    Game findRunningGameByRoomName(String roomName);
}
