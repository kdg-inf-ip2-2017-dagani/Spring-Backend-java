package be.kdg.backend.services.api;

import be.kdg.backend.dom.util.CustomColor;

import java.util.List;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.api
 *
 * @author gaston
 * @version 1.0
 * @since 22/02/2017
 */
public interface ColorService {
    String addColor(CustomColor color);

    CustomColor findColorByColor(String color);

    List<CustomColor> findColors();

    void removeColor(CustomColor color);

    void removeAllColors();

}
