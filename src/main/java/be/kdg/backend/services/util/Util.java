package be.kdg.backend.services.util;

import be.kdg.backend.dom.game.Player;
import be.kdg.backend.dom.game.Team;
import be.kdg.backend.dom.location.Point;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.dom.util
 *
 * @author Gaston
 * @version 1.0
 * @since 25/02/2017
 */
public final class Util {
    private static final double R = 6371.0;

    private Util() {
    }

    /**
     * Source: http://stackoverflow.com/questions/4102520/how-to-transform-a-distance-from-degrees-to-metres
     *
     * @param p1 first point
     * @param p2 second point
     * @return distance between the points in meters
     */
    public static double haversineDistance(Point p1, Point p2) {
        double lat1, lat2, lon1, lon2, dLat, dLon, a, c, d;
        lat1 = p1.getLatitude();
        lat2 = p2.getLatitude();
        lon1 = p1.getLongitude();
        lon2 = p2.getLongitude();

        dLat = Math.toRadians(lat2 - lat1);
        dLon = Math.toRadians(lon2 - lon1);
        a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        d = R * c;
        return d * 1000;
    }

    public static double calculateTeamMoney(Team team) {
        double money = team.getBankAccount();
        money += team.getTreasury();
        for (Player player : team.getPlayers().values()) {
            money += player.getMoney();
        }
        return money;
    }
}