package be.kdg.backend.services.util;

import be.kdg.backend.dom.game.Player;
import be.kdg.backend.dom.game.Team;
import be.kdg.backend.dom.location.AreaLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gasto on 08/06/2017.
 */
public class JsonTeam {
    private List<AreaLocation> districts;
    private String teamName;
    private List<Player> players;
    private List<String> tradePosts;
    private double treasury;
    private double bankAccount;
    private String customColor;

    public JsonTeam(Team t) {
        districts = t.getDistricts();
        teamName = t.getTeamName();
        tradePosts = t.getTradePosts();
        treasury = t.getTreasury();
        bankAccount = t.getBankAccount();
        customColor = t.getCustomColor();
        players = new ArrayList<>(t.getPlayers().values());
    }
}
