package be.kdg.backend.services.exceptions;

/**
 * Created by Nikita on 9/02/2017.
 */
public class AccountServiceException extends RuntimeException {
    public AccountServiceException(String message) {
        super(message);
    }
}
