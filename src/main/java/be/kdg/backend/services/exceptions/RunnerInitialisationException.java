package be.kdg.backend.services.exceptions;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.runners
 *
 * @author Gaston
 * @version 1.0
 * @since 28/02/2017
 */
public class RunnerInitialisationException extends RuntimeException {
    public RunnerInitialisationException() {
    }

    public RunnerInitialisationException(String message) {
        super(message);
    }

    public RunnerInitialisationException(String message, Throwable cause) {
        super(message, cause);
    }

    public RunnerInitialisationException(Throwable cause) {
        super(cause);
    }
}