package be.kdg.backend.services.exceptions;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.runners
 *
 * @author Gaston
 * @version 1.0
 * @since 22/02/2017
 */
public class WebRunnerException extends RuntimeException{
    public WebRunnerException() {
        super();
    }

    public WebRunnerException(String message) {
        super(message);
    }

    public WebRunnerException(String message, Throwable cause) {
        super(message, cause);
    }

    public WebRunnerException(Throwable cause) {
        super(cause);
    }

    protected WebRunnerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}