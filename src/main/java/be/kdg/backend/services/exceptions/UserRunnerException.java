package be.kdg.backend.services.exceptions;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.exceptions
 *
 * @author Gaston
 * @version 1.0
 * @since 22/02/2017
 */
public class UserRunnerException extends RuntimeException {
    public UserRunnerException() {
        super();
    }

    public UserRunnerException(String message) {
        super(message);
    }

    public UserRunnerException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserRunnerException(Throwable cause) {
        super(cause);
    }

    protected UserRunnerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}