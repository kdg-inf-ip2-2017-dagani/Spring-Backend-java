package be.kdg.backend.services.exceptions;

/**
 * Created by Gaston
 * 10/02/2017
 */
public class GameException extends RuntimeException {
    public GameException(String s) {
        super(s);
    }

    public GameException(String message, Throwable cause) {
        super(message, cause);
    }
}
