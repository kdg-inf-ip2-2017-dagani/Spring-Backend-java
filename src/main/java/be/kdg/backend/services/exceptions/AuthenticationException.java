package be.kdg.backend.services.exceptions;

/**
 * Created by Nikita on 18/03/2017.
 */
public class AuthenticationException extends RuntimeException  {
    public AuthenticationException(String s) {
        super(s);
    }

}
