package be.kdg.backend.services.exceptions;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.impl
 *
 * @author Gaston
 * @version 1.0
 * @since 22/02/2017
 */
public class WrongPasswordException extends RuntimeException {
}