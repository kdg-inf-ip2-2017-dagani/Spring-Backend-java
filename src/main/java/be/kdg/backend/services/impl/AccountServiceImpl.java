package be.kdg.backend.services.impl;

import be.kdg.backend.dom.account.AccessLevel;
import be.kdg.backend.dom.account.Account;
import be.kdg.backend.dom.account.Token;
import be.kdg.backend.persistence.api.AccountRepository;
import be.kdg.backend.services.api.AccountService;
import be.kdg.backend.services.exceptions.AccountServiceException;
import be.kdg.backend.services.exceptions.AuthenticationException;
import be.kdg.backend.services.exceptions.InvalidTokenException;
import be.kdg.backend.services.exceptions.TokenExpiredException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Nikita on 9/02/2017.
 */
@Service("accountService")
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }


    @Override
    public String addAccount(Account account) {
        if (account.getEmailAddress() != null && accountRepository.findByEmailAddress(account.getEmailAddress()) != null)
            throw new AccountServiceException("An account with this email address already exists!");
        if (account.getUsername() != null && accountRepository.findAccountByUsername(account.getUsername()) != null)
            throw new AccountServiceException("An account with this username already exists!");
        if (account.getCurrentDeviceId() != null && accountRepository.findAccountByCurrentDeviceId(account.getCurrentDeviceId()) != null)
            throw new AccountServiceException("An account with this deviceId already exists!");

        //todo hash password
        return accountRepository.save(account).getId();
    }

    @Override
    public Account findAccountByCurrentDeviceId(String deviceId) {
        Account a = accountRepository.findAccountByCurrentDeviceId(deviceId);
        if (a == null) {
            throw new AccountServiceException("Account not found");
        }
        return a;
    }

    @Override
    public Account findAccountByEmailAddress(String emailAddress) {
        Account a = accountRepository.findByEmailAddress(emailAddress);
        if (a == null) {
            throw new AccountServiceException("Account not found");
        }
        return a;
    }

    @Override
    public void deleteAccountByEmailAddress(String emailAddress) {
        accountRepository.deleteAccountByEmailAddress(emailAddress);
    }

    @Override
    public List<Account> findAccounts() {
        return accountRepository.findAll();
    }

    @Override
    public Account updateAccessLevel(String id, AccessLevel accessLevel) {
        Account a = accountRepository.findOne(id);
        a.setAccessLevel(accessLevel);
        return accountRepository.save(a);
    }

    @Override
    public String checkLogin(String id, String password) {
        Account a = accountRepository.findOne(id);
        if (a == null || !a.getPassword().equals(password)) {
            throw new AuthenticationException("Username or password is wrong!");
        }
        Token token = new Token(a);
        a.setToken(token);
        accountRepository.save(a);
        return token.getToken();
    }

    @Override
    public Account findAccountByUsername(String username) {
        return accountRepository.findAccountByUsername(username);
    }

    @Override
    public Account validateToken(String token) {
        Account a = accountRepository.findUserByTokenToken(token);
        if (a == null) throw new InvalidTokenException();
        if (a.getToken().getValidUntil().isBefore(LocalDateTime.now())) throw new TokenExpiredException();
        a.getToken().refresh();
        return accountRepository.save(a);
    }

    @Override
    public void removeAllAccounts() {
        accountRepository.deleteAll();
    }
}
