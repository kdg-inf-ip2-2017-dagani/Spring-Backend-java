package be.kdg.backend.services.impl;

import be.kdg.backend.dom.account.Account;
import be.kdg.backend.dom.game.*;
import be.kdg.backend.dom.location.AreaLocation;
import be.kdg.backend.dom.location.Point;
import be.kdg.backend.dom.location.PointLocation;
import be.kdg.backend.dom.location.TradePost;
import be.kdg.backend.persistence.api.GameRepository;
import be.kdg.backend.services.api.AccountService;
import be.kdg.backend.services.api.LocationService;
import be.kdg.backend.services.api.RunningGameService;
import be.kdg.backend.services.exceptions.GameException;
import be.kdg.backend.services.runners.GameRunner;
import be.kdg.backend.services.runners.messages.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static be.kdg.StniklaasbackendApplication.GSON;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.impl
 *
 * @author gasto
 * @version 1.0
 * @since 10/03/2017
 */
@Service
public class RunningGameServiceImpl implements RunningGameService {
    private static Map<String, GameRunner> runningGames = new HashMap<>();
    private static Logger logger = Logger.getLogger(RunningGameServiceImpl.class);


    private LocationService locationService;

    private AccountService accountService;

    private GameRepository gameRepository;

    @Autowired
    public RunningGameServiceImpl(LocationService locationService, AccountService accountService, GameRepository gameRepository) {
        this.locationService = locationService;
        this.accountService = accountService;
        this.gameRepository = gameRepository;
    }

    //########################################## DELEGATED MAP METHODS #################################################

    public static boolean containsKey(String key) {
        return runningGames.containsKey(key);
    }

    public static GameRunner get(String key) {
        return runningGames.get(key);
    }

    public static GameRunner put(String key, GameRunner value) {
        return runningGames.put(key, value);
    }

    public static GameRunner remove(String key) {
        return runningGames.remove(key);
    }

    public static Collection<GameRunner> values() {
        return runningGames.values();
    }

    //###################################################### SERVICE METHODS ###########################################
    private Game bankTransaction(Game game, Team t, Player p, String locationId, double moneyTransferred, boolean isDeposit) {
        if (isDeposit) {
            if (p.getMoney() < moneyTransferred) {
                logger.warn("Not enough money");
                throw new GameException("Not enough money!");
            }
            p.setMoney(p.getMoney() - moneyTransferred);
            double money = t.getBankAccount();
            money += moneyTransferred;
            t.setBankAccount(money);
        } else {
            if (t.getBankAccount() < moneyTransferred) {
                logger.warn("Not enough money");
                throw new GameException("Not enough money!");
            }
            p.setMoney(p.getMoney() + moneyTransferred);
            double money = t.getBankAccount();
            money -= moneyTransferred;
            t.setBankAccount(money);
        }
        return game;
    }

    private Game districtConquered(Game g, Player p, Team t, String locationID) {
        //remove district other team
        for (Team team : g.getTeams()) {
            for (int i = 0; i < team.getDistricts().size(); i++) {
                if (team.getDistricts().get(i).getId().equals(locationID)) {
                    /* todo move this check to the adding of players
                    if (p.getTaggedByTeams().contains(team.getTeamName())) {
                        logger.warn("You've already been tagged by this team!");
                        throw new GameException("You've already been tagged by this team!");
                    }
                    */
                    team.getDistricts().remove(i);
                }
            }
        }

        AreaLocation district = locationService.findAreaLocationById(locationID);
        t.getDistricts().add(district);
        return g;
    }

    private Game tagPlayer(Game g, Player p, String clientID, Team t, List<String> involvedPlayers, String districtId) {
        double moneyStolen = 0.0;
        Map<String, Integer> itemsIllegalStolen = new HashMap<>();
        Map<String, Integer> itemsLegalStolen = new HashMap<>();


        boolean isDistrictFromTeam = false;
        for (AreaLocation a : t.getDistricts()) {
            if (a.getId().equals(districtId)) {
                isDistrictFromTeam = true;
            }
        }
        for (String involvedPlayer : involvedPlayers) {
            for (Team team : g.getTeams()) {
                if (team.getPlayers().containsKey(involvedPlayer)) {
                    Player player = team.getPlayers().get(involvedPlayer);
                    itemsIllegalStolen.putAll(player.getIllegalItems());
                    player.setIllegalItems(new HashMap<>());
                    if (isDistrictFromTeam) {
                        player.getTaggedByTeams().add(t.getTeamName());
                        itemsLegalStolen.putAll(player.getLegalItems());
                        player.setLegalItems(new HashMap<>());
                        moneyStolen += player.getMoney();
                        player.setMoney(0);
                    }
                    runningGames.get(g.getId()).pushTagNotification(player, GSON.toJson(new TagNotification(p.getClientID(), isDistrictFromTeam, t.getTeamName())));
                    runningGames.get(g.getId()).pushPlayerNotification(player, GSON.toJson(new PlayerNotification(player.getLegalItems(), player.getIllegalItems(), player.getMoney(), player.getVisitedTradeposts())));
                }
            }
        }
        p.setMoney(p.getMoney() + moneyStolen);
        p.getIllegalItems().putAll(itemsIllegalStolen);
        p.getLegalItems().putAll(itemsLegalStolen);
        runningGames.get(g.getId()).pushTagNotification(p, GSON.toJson(new TagNotification(p.getClientID(), false, t.getTeamName())));
        runningGames.get(g.getId()).pushPlayerNotification(p, GSON.toJson(new PlayerNotification(p.getLegalItems(), p.getIllegalItems(), p.getMoney(), p.getVisitedTradeposts())));
        return g;
    }

    private Game tradePostTransfer(Game game, Player p, String clientId, Team t, Map<String, Integer> items, String locationID, GameEventType type) {
        Map<String, Item> itemObjects = new HashMap<>();

        if (type.toString().toLowerCase().endsWith("_sale")) {//todo tradepost sale forbidden
            for (PointLocation pointLocation : game.getTradePosts()) {
                TradePost tradePost = (TradePost) pointLocation;
                for (Item item : tradePost.getItems()) {
                    itemObjects.put(item.getName(), item);
                }
            }
            switch (type) {
                case TRADEPOST_LEGAL_SALE:
                    for (Map.Entry<String, Integer> entry : items.entrySet()) {
                        String itemName = entry.getKey();
                        p.getLegalItems().put(itemName, p.getLegalItems().get(itemName) - entry.getValue());
                        p.setMoney(p.getMoney() + (itemObjects.get(itemName).getLegalSales() * (double) entry.getValue()));
                        if (p.getLegalItems().get(itemName) == 0) {
                            p.getLegalItems().remove(itemName);
                        }
                    }
                    t.getTradePosts().add(locationID);
                    break;
                case TRADEPOST_ILLEGAL_SALE:
                    for (Map.Entry<String, Integer> entry : items.entrySet()) {
                        String itemName = entry.getKey();
                        p.getIllegalItems().put(itemName, p.getIllegalItems().get(itemName) - entry.getValue());
                        p.setMoney(p.getMoney() + (itemObjects.get(itemName).getIllegalSales() * (double) entry.getValue()));
                        if (p.getIllegalItems().get(itemName) == 0) {
                            p.getIllegalItems().remove(itemName);
                        }
                    }
                    t.getTradePosts().add(locationID);
                    break;
                case TRADEPOST_ALL_SALE:
                    for (Map.Entry<String, Integer> entry : p.getLegalItems().entrySet()) {
                        String itemName = entry.getKey();
                        p.setMoney(p.getMoney() + (itemObjects.get(itemName).getLegalSales() * (double) entry.getValue()));
                        p.getLegalItems().put(itemName, p.getLegalItems().get(itemName) - entry.getValue());
                        if (p.getLegalItems().get(itemName) == 0) {
                            p.getLegalItems().remove(itemName);
                        }
                    }
                    for (Map.Entry<String, Integer> entry : p.getIllegalItems().entrySet()) {
                        String itemName = entry.getKey();
                        p.setMoney(p.getMoney() + (itemObjects.get(itemName).getIllegalSales() * (double) entry.getValue()));
                        p.getIllegalItems().put(itemName, p.getIllegalItems().get(itemName) - entry.getValue());
                        if (p.getIllegalItems().get(itemName) == 0) {
                            p.getIllegalItems().remove(itemName);
                        }
                    }
                    break;
            }

        } else {
            TradePost tradePost = locationService.findTradepostById(locationID);
            /*TODO debug this, code appears to do a second pass via here
            if (t.getTradePosts().contains(locationID)) {
                logger.warn("Your team has already bought something in this trade post!");
                throw new GameException("Your team has already bought something in this trade post!");
            }
            */
            if (tradePost == null) {
                throw new GameException("Tradepost not found!");
            }
            if (!t.getVisitedTradeposts().containsKey(locationID)) {
                //team has not visited this tradepost -> player also hasn't visited

                for (String itemName : items.keySet()) {
                    for (Item i : tradePost.getItems()) {
                        if (i.getName().equals(itemName)) {
                            itemObjects.put(i.getName(), i);
                        }
                    }
                }
                for (Map.Entry<String, Integer> entry : items.entrySet()) {
                    String itemName = entry.getKey();
                    int quantity = entry.getValue();
                    switch (type) {
                        case TRADEPOST_LEGAL_PURCHASE:
                            if (!p.getLegalItems().containsKey(itemName)) {
                                p.getLegalItems().put(itemName, quantity);
                            } else {
                                p.getLegalItems().put(itemName, p.getLegalItems().get(itemName) + quantity);
                            }
                            p.setMoney(p.getMoney() - (itemObjects.get(itemName).getLegalPurchase() * quantity));
                            t.getTradePosts().add(locationID);

                            //lock tradepost
                            get(game.getId()).startTradepostTimer(t,p,locationID);

                            break;
                        case TRADEPOST_ILLEGAL_PURCHASE:
                            if (!p.getIllegalItems().containsKey(itemName)) {
                                p.getIllegalItems().put(itemName, quantity);
                            } else {
                                p.getIllegalItems().put(itemName, p.getIllegalItems().get(itemName) + quantity);
                            }
                            p.setMoney(p.getMoney() - (itemObjects.get(itemName).getIllegalPurchase() * quantity));
                            t.getTradePosts().add(locationID);

                            //lock tradepost
                            get(game.getId()).startTradepostTimer(t,p,locationID);

                            break;
                    }
                }

            }//else{/*do nothing*/}
        }
        return game;
    }

    private Team treasuryRobbery(Game g, Player p, Team t, String locationID) {
        Team otherTeam = null;
        for (Team team : g.getTeams()) {
            if (team.getDistricts().get(0).getId().equals(locationID)) {
                otherTeam = team;
            }
        }
        if (otherTeam == null) {
            logger.warn("This isn't a main district!");
            throw new GameException("This isn't a main district!");
        }
        if (p.getTaggedByTeams().contains(otherTeam.getTeamName())) {
            logger.warn("You were tagged by this team!");
            throw new GameException("You were tagged by this team!");
        }
        p.setMoney(p.getMoney() + otherTeam.getTreasury());
        otherTeam.setTreasury(0.0);
        return otherTeam;
    }

    private Game treasuryWithdrawal(Game g, Player p, Team t, String locationID, double moneyTransferred) {
        if (!t.getDistricts().get(0).getId().equals(locationID)) {
            logger.warn("This isn't the treasury of team " + t.getTeamName());
            throw new GameException("This isn't the treasury of team " + t.getTeamName());
        }
        if (false && !g.isTreasuriesOpen()) {//TODO: reinstate this check
            logger.debug("Treasuries are not open");
            throw new GameException("Treasuries are not open");
        }
        if (t.getTreasury() < moneyTransferred) {
            logger.warn("Not enough money in the treasury!");
            throw new GameException("Not enough money in the treasury!");
        }
        t.setTreasury(t.getTreasury() - moneyTransferred);
        p.setMoney(p.getMoney() + moneyTransferred);
        return g;
    }

    @Override
    public void stopGame(String token, String gameID) {
        Account host = accountService.validateToken(token);//throws exceptions if token not valid
        RunningGameServiceImpl.runningGames.get(gameID).stopGame();
        //gameRepository.save(RunningGameServiceImpl.runningGames.get(gameID).getGame()); todo done elsewhere?
        RunningGameServiceImpl.runningGames.remove(gameID);
    }

    @Override
    public void updateLocation(String gameID, String clientID, double lat, double lon) {
        if (!RunningGameServiceImpl.containsKey(gameID)) {
            logger.warn("Game is not running");
            throw new GameException("Game is not running");
        }

        Game g = RunningGameServiceImpl.runningGames.get(gameID).getGame();
        if (g == null) {
            RunningGameServiceImpl.logger.warn("Game could not be found, session will disconnect");
            throw new GameException("Game not found");
        }
        Player p = null;
        for (Team team : g.getTeams()) {
            if (team.getPlayers().containsKey(clientID)) {
                p = team.getPlayers().get(clientID);
            }
        }
        if (p == null) {
            logger.warn("UpdateLocation could not find player, session will disconnect");
            throw new GameException("Player not found");
        }
        p.setLocation(new Point(lat, lon));
    }

    @Override
    public void postEvent(String gameID, String clientID, GameEventType type, List<String> involvedPlayers, double moneyTransferred, Map<String, Integer> itemsTransferred, String locationID) {
        if (!runningGames.containsKey(gameID)) {
            logger.warn("Game not found");
            throw new GameException("Game not found");
        }
        Game g = runningGames.get(gameID).getGame();
        Player p = null;
        Team t = null;
        for (Team team : g.getTeams()) {
            if (team.getPlayers().containsKey(clientID)) {
                p = team.getPlayers().get(clientID);
                t = team;
            }
        }
        if (p == null) {
            logger.warn("Player not found");
            throw new GameException("Player not found");
        }
        String playerTeam = t.getTeamName();

        Set<String> involvedTeams = new HashSet<>();
        for (String player : involvedPlayers) {
            for (Team team : g.getTeams()) {
                Map<String, Player> ployers = team.getPlayers();
                for (String id : ployers.keySet()) {
                    if (id.equals(player)) involvedTeams.add(team.getTeamName());
                }
            }
        }
        GameEvent event = new GameEvent(System.currentTimeMillis(), type, involvedPlayers, new ArrayList<>(involvedTeams), moneyTransferred, clientID, playerTeam, locationID, itemsTransferred);
        g.getEvents().add(event);
        GameRunner runner = runningGames.get(gameID);
        switch (type) {
            case BANK_DEPOSIT:
                g = bankTransaction(g, t, p, locationID, moneyTransferred, true);
                runner.pushTeamNotification(t, new TeamNotification(t.getDistricts(), t.getTreasury(), t.getBankAccount(), t.getTotalPlayerMoney(), t.getTradePosts(), t.getVisitedTradeposts()));
                runner.pushPlayerNotification(p, new PlayerNotification(p.getLegalItems(), p.getIllegalItems(), p.getMoney(), p.getVisitedTradeposts()));
                break;
            case BANK_WITHDRAWAL:
                g = bankTransaction(g, t, p, locationID, moneyTransferred, false);
                runner.pushTeamNotification(t, new TeamNotification(t.getDistricts(), t.getTreasury(), t.getBankAccount(), t.getTotalPlayerMoney(), t.getTradePosts(), t.getVisitedTradeposts()));
                runner.pushPlayerNotification(p, new PlayerNotification(p.getLegalItems(), p.getIllegalItems(), p.getMoney(), p.getVisitedTradeposts()));
                break;
            case TRADEPOST_LEGAL_PURCHASE:
                g = tradePostTransfer(g, p, clientID, t, itemsTransferred, locationID, GameEventType.TRADEPOST_LEGAL_PURCHASE);
                runner.pushPlayerNotification(p, new PlayerNotification(p.getLegalItems(), p.getIllegalItems(), p.getMoney(), p.getVisitedTradeposts()));
                runner.pushTeamNotification(t, new TeamNotification(t.getDistricts(), t.getTreasury(), t.getBankAccount(), t.getTotalPlayerMoney(), t.getTradePosts(), t.getVisitedTradeposts()));
                break;
            case TRADEPOST_ILLEGAL_SALE:
                g = tradePostTransfer(g, p, clientID, t, itemsTransferred, locationID, GameEventType.TRADEPOST_ILLEGAL_SALE);
                runner.pushPlayerNotification(p, new PlayerNotification(p.getLegalItems(), p.getIllegalItems(), p.getMoney(), p.getVisitedTradeposts()));
                runner.pushTeamNotification(t, new TeamNotification(t.getDistricts(), t.getTreasury(), t.getBankAccount(), t.getTotalPlayerMoney(), t.getTradePosts(), t.getVisitedTradeposts()));
                break;
            case TRADEPOST_LEGAL_SALE:
                g = tradePostTransfer(g, p, clientID, t, itemsTransferred, locationID, GameEventType.TRADEPOST_LEGAL_SALE);
                runner.pushPlayerNotification(p, new PlayerNotification(p.getLegalItems(), p.getIllegalItems(), p.getMoney(), p.getVisitedTradeposts()));
                runner.pushTeamNotification(t, new TeamNotification(t.getDistricts(), t.getTreasury(), t.getBankAccount(), t.getTotalPlayerMoney(), t.getTradePosts(), t.getVisitedTradeposts()));
                break;
            case TRADEPOST_ALL_SALE:
                g = tradePostTransfer(g, p, clientID, t, itemsTransferred, locationID, GameEventType.TRADEPOST_ALL_SALE);
                runner.pushPlayerNotification(p, new PlayerNotification(p.getLegalItems(), p.getIllegalItems(), p.getMoney(), p.getVisitedTradeposts()));
                runner.pushTeamNotification(t, new TeamNotification(t.getDistricts(), t.getTreasury(), t.getBankAccount(), t.getTotalPlayerMoney(), t.getTradePosts(), t.getVisitedTradeposts()));
                break;
            case TRADEPOST_ILLEGAL_PURCHASE:
                g = tradePostTransfer(g, p, clientID, t, itemsTransferred, locationID, GameEventType.TRADEPOST_ILLEGAL_PURCHASE);
                runner.pushPlayerNotification(p, new PlayerNotification(p.getLegalItems(), p.getIllegalItems(), p.getMoney(), p.getVisitedTradeposts()));
                runner.pushTeamNotification(t, new TeamNotification(t.getDistricts(), t.getTreasury(), t.getBankAccount(), t.getTotalPlayerMoney(), t.getTradePosts(), t.getVisitedTradeposts()));
                break;
            case PLAYER_TAGGED:
                g = tagPlayer(g, p, clientID, t, involvedPlayers, locationID);
                break;
            case DISTRICT_CONQUERED:
                g = districtConquered(g, p, t, locationID);
                runner.pushTeamNotification(t, new TeamNotification(t.getDistricts(), t.getTreasury(), t.getBankAccount(), t.getTotalPlayerMoney(), t.getTradePosts(), t.getVisitedTradeposts()));
                runner.pushDistrictNotification(new DistrictNotification(playerTeam, locationID));
                break;
            case TREASURY_WITHDRAWAL:
                g = treasuryWithdrawal(g, p, t, locationID, moneyTransferred);
                runner.pushTeamNotification(t, new TeamNotification(t.getDistricts(), t.getTreasury(), t.getBankAccount(), t.getTotalPlayerMoney(), t.getTradePosts(), t.getVisitedTradeposts()));
                runner.pushPlayerNotification(p, new PlayerNotification(p.getLegalItems(), p.getIllegalItems(), p.getMoney(), p.getVisitedTradeposts()));
                break;
            case TREASURY_ROBBERY:
                Team otherTeam = treasuryRobbery(g, p, t, locationID);
                ArrayList<Team> notificationTeams = new ArrayList<>();
                notificationTeams.add(t);
                notificationTeams.add(otherTeam);
                runner.pushInfoNotification(notificationTeams, new InfoNotification(GameEventType.TREASURY_ROBBERY, otherTeam.getTeamName(), locationID));
                runner.pushPlayerNotification(p, new PlayerNotification(p.getLegalItems(), p.getIllegalItems(), p.getMoney(), p.getVisitedTradeposts()));
                break;
        }
        //gameRepository.save(g);
    }

    @Override
    public void endGame(String hostToken, String id) {
        Account host = accountService.validateToken(hostToken);//throws exceptions if token not valid
        //check game exists
        Game game = gameRepository.findOne(id);
        if (game == null) {
            logger.warn("Game not found");
            throw new GameException("Game not found");
        }

        // check host hosts game
        if (!game.getHost().equals(host)) {
            logger.warn("Host does not host this game");
            throw new GameException("Host does not host this game");
        }

        if (!RunningGameServiceImpl.runningGames.containsKey(game.getId())) {
            logger.warn("Game is not running");
            throw new GameException("Game is not running");
        }
        GameRunner runner = RunningGameServiceImpl.runningGames.get(id);
        game = runner.getGame();
        long endTime = System.currentTimeMillis();
        runner.stopGame();
        game.setEndTime(endTime);
        gameRepository.save(game);
        RunningGameServiceImpl.runningGames.remove(id);
    }

    @Override
    public void clearRunningGames() {
        for (GameRunner gameRunner : RunningGameServiceImpl.runningGames.values()) {
            gameRunner.stopGame();
        }
        RunningGameServiceImpl.runningGames.clear();
    }

    @Override
    public void addPlayerToDistrictConquer(String gameID, String clientID, String locationID) {
        if (!runningGames.containsKey(gameID)) {
            logger.warn("Game not found");
            throw new GameException("Game not found");
        }
        Game g = runningGames.get(gameID).getGame();
        Player p = null;
        Team t = null;
        for (Team team : g.getTeams()) {
            if (team.getPlayers().containsKey(clientID)) {
                p = team.getPlayers().get(clientID);
                t = team;
            }
        }
        if (p == null) {
            logger.warn("Player not found");
            throw new GameException("Player not found");
        }
        GameRunner runner = runningGames.get(gameID);
        runner.addPlayerToDistrictConquer(t.getTeamName(), p, locationID);
    }

    @Override
    public void removePlayerFromDistrictConquer(String gameID, String clientID, String locationID) {
        if (!runningGames.containsKey(gameID)) {
            logger.warn("Game not found");
            throw new GameException("Game not found");
        }
        Game g = runningGames.get(gameID).getGame();
        Player p = null;
        Team t = null;
        for (Team team : g.getTeams()) {
            if (team.getPlayers().containsKey(clientID)) {
                p = team.getPlayers().get(clientID);
                t = team;
            }
        }
        if (p == null) {
            logger.warn("Player not found");
            throw new GameException("Player not found");
        }
        GameRunner runner = runningGames.get(gameID);
        runner.removePlayerFromDistrictConquer(t.getTeamName(), p, locationID);
    }

    @Override
    public Game findRunningGameByRoomName(String roomName) {
        for (GameRunner runner : runningGames.values()) {
            if (runner.getGame().getRoomName().equalsIgnoreCase(roomName)) return runner.getGame();
        }
        throw new GameException("Game does not exist");
    }
}
