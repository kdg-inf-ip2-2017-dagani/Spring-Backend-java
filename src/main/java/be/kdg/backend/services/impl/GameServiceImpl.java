package be.kdg.backend.services.impl;

import be.kdg.backend.Seed;
import be.kdg.backend.controllers.websocket.AbstractSessionHandler;
import be.kdg.backend.controllers.websocket.GameWebSocketHandler;
import be.kdg.backend.dom.account.AccessLevel;
import be.kdg.backend.dom.account.Account;
import be.kdg.backend.dom.game.Game;
import be.kdg.backend.dom.game.GameState;
import be.kdg.backend.dom.game.Player;
import be.kdg.backend.dom.game.Team;
import be.kdg.backend.dom.location.*;
import be.kdg.backend.dom.util.CustomColor;
import be.kdg.backend.persistence.api.AreaLocationRepository;
import be.kdg.backend.persistence.api.GameRepository;
import be.kdg.backend.services.api.AccountService;
import be.kdg.backend.services.api.ColorService;
import be.kdg.backend.services.api.GameService;
import be.kdg.backend.services.api.LocationService;
import be.kdg.backend.services.exceptions.DuplicateUserException;
import be.kdg.backend.services.exceptions.GameException;
import be.kdg.backend.services.exceptions.WrongPasswordException;
import be.kdg.backend.services.runners.GameRunner;
import be.kdg.backend.services.runners.messages.MessageWrapper;
import be.kdg.backend.services.runners.messages.WinningTeamMessage;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import java.time.Duration;
import java.util.*;

/**
 * Created by Nikita on 10/02/2017.
 */
@Service
public final class GameServiceImpl implements GameService {
    private static final String DEFAULT_GAME_NAME = "defaultGame";
    private static Map<String, GameRunner> stagedGames = new HashMap<>();
    private final Logger logger = Logger.getLogger(GameServiceImpl.class);
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private AreaLocationRepository areaLocationRepository;
    @Autowired
    private AccountService accountService;
    @Autowired
    private LocationService locationService;
    @Autowired
    private ColorService colorService;
    @Autowired
    private GameWebSocketHandler gameWebSocketHandler;
    @Autowired
    private RunningGameServiceImpl runningGameService;
    @Autowired
    private Seed seed;

    private Game authenticateGame(String token, String gameId) {
        Account host = accountService.validateToken(token);//throws exceptions if token not valid
        //check game exists
        Game game = gameRepository.findOne(gameId);
        if (game == null) throw new GameException("404: Game not found");

        // check host hosts game
        if (!game.getHost().equals(host)) throw new GameException("Host does not host this game");

        if (RunningGameServiceImpl.containsKey(game.getId())) {
            return RunningGameServiceImpl.get(game.getId()).getGame();
        }
        if (!stagedGames.containsKey(game.getId())) throw new GameException("Game is not staged");
        return stagedGames.get(game.getId()).getGame();
    }

    private Player registerHost(Game g, String clientID, String name) {
        for (Player player : g.getHosts()) {
            if (player.getClientID().equals(clientID))
                throw new DuplicateUserException();
        }
        Player p = new Player(clientID, name);
        g.getHosts().add(p);

        stagedGames.get(g.getId()).registerHost(p.getToken(), p.getClientID());
        return p;
    }

    private Player registerPlayer(Game g, String clientID, String name) {
        logger.debug(String.format("Registering client: %20s with id: %s for game %s", name, clientID, g.getId()));
        logger.debug("Checking for duplicates");
        for (Team team : g.getTeams()) {
            if (team.getPlayers().containsKey(clientID)) {
                Player p = team.getPlayers().get(clientID);
                stagedGames.get(g.getId()).registerPlayer(p.getToken(), p.getClientID());
                return p;
            }
        }

        logger.debug("Finding team with least players");
        Team team = g.getTeams().get(0);
        for (Team other : g.getTeams()) {
            if (other.getPlayers().size() <= team.getPlayers().size()) {
                team = other;
            }
        }

        logger.debug("Adding player to team");
        Player p = new Player(clientID, name);
        team.getPlayers().put(p.getClientID(), p);
        logger.debug(String.format("Player %s added to team %s", name, team.getTeamName()));
        stagedGames.get(g.getId()).registerPlayer(p.getToken(), p.getClientID());
        return p;
    }

    @Override
    public String addDistrict(String token, String gameId, String districtId) {
        Game game = authenticateGame(token, gameId);
        if (game == null) throw new GameException("Game authentication error");
        //check district exists
        AreaLocation location = areaLocationRepository.findOne(districtId);
        if (location == null || !(location.getType() == AreaType.DISTRICT_A || location.getType() == AreaType.DISTRICT_B))
            throw new GameException("Location is not a district");

        //add district
        game.getDistricts().add(location);
        saveGame(game);
        return districtId;
    }

    @Override
    public String addBank(String token, String gameId, String bankId) {
        Game game = authenticateGame(token, gameId);
        if (game == null) throw new GameException("Game authentication error");
        //check bank exists
        PointLocation bank = locationService.findPointByID(bankId);
        if (bank == null || !(bank.getType() == PointType.BANK))
            throw new GameException("Location is not a bank");
        saveGame(game);
        return bankId;
    }

    @Override
    public String addGame(Account host, String roomName, int teams, int players, String playerPW) {
        //if (playerPW.isEmpty() || playerPW.trim().isEmpty()) throw new GameException("Password can not be empty");
        if (roomName.isEmpty() || roomName.trim().isEmpty()) throw new GameException("Room name can not be empty");

        Game template = gameRepository.findGameByRoomName(DEFAULT_GAME_NAME);
        if (template == null) {
            logger.error("Template is null");
            //throw new GameException("Template is null");
            seed.insertDefaultGame();
            template = gameRepository.findGameByRoomName(DEFAULT_GAME_NAME);
        }
        //districts assign to game
        List<AreaLocation> districts = locationService.findAreasByType(AreaType.DISTRICT_A);
        districts.addAll(locationService.findAreasByType(AreaType.DISTRICT_B));
        template.setDistricts(districts);

        List<AreaLocation> copyDistricts = new ArrayList<>(districts);
        List<Team> teamsList = new ArrayList<>(0);
        List<CustomColor> colors = colorService.findColors();
        Random random = new Random();
        //CustomColor customColor;
        for (int i = 0; i < teams; i++) {
            Team team;
            if (copyDistricts.size() > 0 && colors.size() > 0) {
                //districts assign to teams + team color
                //int indexColor = random.nextInt(colors.size());
                int index = random.nextInt(copyDistricts.size());

                while (copyDistricts.get(index).getType() != AreaType.DISTRICT_A) {
                    index = random.nextInt(copyDistricts.size());
                }
                team = new Team("Team " + (i + 1), copyDistricts.get(index), colors.get(i).getColor());
                teamsList.add(team);
                copyDistricts.remove(index);
                //colors.remove(indexColor);
            } else {
                team = new Team("Team " + (i + 1));
                teamsList.add(team);
            }
        }

        Game g = new Game(template, host, playerPW, roomName);
        g.setMaxTeams(teams);
        g.setMaxPlayersPerTeam(players);
        g.setTeams(teamsList);
        g = gameRepository.insert(g);
        stagedGames.put(g.getId(), new GameRunner(this, gameRepository, g, this.runningGameService));
        return g.getId();
    }

    @Override
    public void closeGame(Game game) {
        saveGame(game);
    }

    @Override
    public void deleteGameById(String token, String gameId) {
        Game game = authenticateGame(token, gameId);
        if (stagedGames.containsKey(gameId)) {
            stagedGames.remove(gameId);
        }
        GameRunner gr = RunningGameServiceImpl.get(gameId);
        if (gr != null) {
            gr.stopGame();
            RunningGameServiceImpl.remove(gameId);
        }
        gameRepository.delete(gameId);
    }

    @Override
    public void deleteDistrict(String token, String gameId, String districtId) {
        Game game = authenticateGame(token, gameId);
        if (game == null) throw new GameException("Game authentication error");
        //check district exists
        AreaLocation location = areaLocationRepository.findOne(districtId);
        if (location == null || !(location.getType() == AreaType.DISTRICT_A || location.getType() == AreaType.DISTRICT_B))
            throw new GameException("Location is not a district");

        //delete district
        List<AreaLocation> locs = game.getDistricts();
        for (int i = 0; i < locs.size(); i++) {
            if (locs.get(i).getId().equals(location.getId())) {
                locs.remove(i);
            }
        }
        game.setDistricts(locs);
        saveGame(game);
    }

    @Override
    public Game findGameById(String id) {
        Game g;
        if (RunningGameServiceImpl.containsKey(id)) {
            g = RunningGameServiceImpl.get(id).getGame();
        } else if (stagedGames.containsKey(id)) {
            g = stagedGames.get(id).getGame();
        } else {
            g = gameRepository.findGameById(id);
        }
        return g;
    }

    @Override
    public void addPlayerPassword(String token, String gameID, String password) {
        Game g = authenticateGame(token, gameID);
        if (g == null) throw new GameException("Game authentication error");
        g.setPlayerPassword(password);
        saveGame(g);
    }

    @Override
    public void addHostPassword(String token, String gameID, String password) {
        Game g = authenticateGame(token, gameID);
        if (g == null) throw new GameException("Game authentication error");
        g.setHostPassword(password);
        saveGame(g);
    }

    @Override
    public Game findGameByRoomName(String roomName) {
        Game game = gameRepository.findGameByRoomName(roomName);
        if (game == null) {
            throw new GameException("404: Game not found");
        }
        return game;
    }

    @Override
    public Game findStagedGameByRoomName(String roomName) {
        GameRunner gr = stagedGames.values().stream().filter(g -> g.getGame().getRoomName().equalsIgnoreCase(roomName)).findFirst().orElse(null);
        if (gr == null) {
            throw new GameException("404: Game not found");
        }
        return gr.getGame();
    }

    @Override
    public WinningTeamMessage getFinalGameStats(String gameId) {
        Game g = findGameById(gameId);
        if (g == null) throw new GameException("Game not found");

        WinningTeamMessage winningTeamMessage = new WinningTeamMessage();
        for (Team team : g.getTeams()) {
            double score = 0.0;
            score += team.getBankAccount();
            score += team.getTreasury();
            score += team.getTotalPlayerMoney();
            WinningTeamMessage.TeamScore ts = new WinningTeamMessage.TeamScore(team.getTeamName(), score);
            winningTeamMessage.getScoreList().add(ts);
        }
        return winningTeamMessage;
    }

    @Override
    public String registerWebApp(String gameID) {
        if (gameID == null) {
            logger.warn("Method invoked with null parameter");
            throw new GameException("Method invoked with null parameter");
        }
        //check game exists
        Game game = gameRepository.findOne(gameID);
        if (game == null) throw new GameException("404: Game not found");
        // check host hosts game
        if (stagedGames.containsKey(gameID)) {
            return stagedGames.get(gameID).getGame().getWebAppToken();
        } else if (RunningGameServiceImpl.containsKey(gameID)) {
            return RunningGameServiceImpl.get(gameID).getGame().getWebAppToken();
        } else {
            throw new GameException("Game not running or staged");
        }
    }

    @Override
    public void removeGameRunner(String id) {
        if (stagedGames.containsKey(id)) {
            stagedGames.get(id).forceStopGame();
            stagedGames.remove(id);
        } else if (RunningGameServiceImpl.containsKey(id)) {
            RunningGameServiceImpl.get(id).forceStopGame();
            RunningGameServiceImpl.remove(id);
        } else {
            throw new GameException("Game runner not found");
        }
    }

    @Override
    public void restartGame(Account a, String gameId, int seconds) {
        Game g = gameRepository.findGameById(gameId);
        if (g == null) throw new GameException("Game not found");

        //if not admin and not original host error
        if (a.getAccessLevel() != AccessLevel.ADMIN && !a.getId().equals(g.getHost().getId())){
            throw new GameException("You are not admin or original host.");
        }

        g.setHost(a);
        g.setRestarts(g.getRestarts()+1);
        g.setDuration(Duration.ofSeconds(seconds));
        saveGame(g);
        stagedGames.put(g.getId(), new GameRunner(this, gameRepository, g, this.runningGameService));
    }

    @Override
    public void saveGameSettings(String token, Game g) {
        Game game = authenticateGame(token, g.getId());
        if (game == null) throw new GameException("Game authentication error");
        game.setTeams(g.getTeams());
        game.setHosts(g.getHosts());
        //stagedGames.put(g.getId(), new GameRunner(this, gameRepository, g, this.runningGameService));
        saveGame(game);
    }

    @Override
    public void startGame(String token, String gameID) {
        Game g = authenticateGame(token, gameID);
        if (g == null) throw new GameException("Game authentication error");
        long start = System.currentTimeMillis();
        long end = start + g.getDuration().toMillis();
        g.setStartTime(start);
        g.setEndTime(end);
        saveGame(g);

        GameRunner gr = stagedGames.get(g.getId());
        RunningGameServiceImpl.put(gr.getGame().getId(), gr);
        stagedGames.remove(g.getId());
        new Thread(gr).start();
    }

    @Override
    public String registerUser(String gameID, String clientID, String name, String password) {
        if (!stagedGames.containsKey(gameID)) throw new GameException("Game could not be found");
        Game g = stagedGames.get(gameID).getGame();
        if (clientID.isEmpty() || clientID.trim().isEmpty()) throw new GameException("Client id can not be empty");
        Player p;
        if (g.getHostPassword().equals(password)) {
            p = registerHost(g, clientID, name);
        } else if (g.getPlayerPassword().equals(password)) {
            p = registerPlayer(g, clientID, name);
        } else {
            throw new WrongPasswordException();
        }
        saveGame(g);
        return p.getToken();
    }

    @Override
    public Player findPlayerByClientId(String gameId, String clientID) {
        Game game = findGameById(gameId);
        if (game == null) throw new GameException("Game could not be found");
        Player player = null;
        for (Team t : game.getTeams()) {
            if (t.getPlayers().containsKey(clientID)) {
                player = t.getPlayers().get(clientID);
            }
        }
        if (player == null) throw new GameException("Player could not be found");
        return player;
    }

    @Override
    public void banPlayer(String token, String clientID, String gameId) {
        Game game = authenticateGame(token, gameId);
        if (game == null) throw new GameException("Game authentication error");
        Player removedPlayer = null;
        for (Team t : game.getTeams()) {
            removedPlayer = t.getPlayers().remove(clientID);
        }
        if (removedPlayer == null) throw new GameException("Player could not be found!");
        saveGameSettings(token, game);
    }

    /**
     * Stops the game if it is running
     * @param gameId
     */
    @Override
    public void stopIfGameRunning(String gameId) {
        GameRunner gr = null;
        if (RunningGameServiceImpl.containsKey(gameId)){
            gr= RunningGameServiceImpl.remove(gameId);
        } else if (stagedGames.containsKey(gameId)){
            gr=stagedGames.remove(gameId);
        }
        if (gr == null) return;
        gr.forceStopGame();
        saveGame(gr.getGame());
    }

    @Override
    public void unregisterUser(String gameID, String clientID) {
        Game g = findGameById(gameID);
        if (g == null) throw new GameException("Game not found");
        if (!stagedGames.containsKey(gameID)) throw new GameException("can only unregister from staged game");
        boolean removedOne = false;
        for (Team team : g.getTeams()) {
            if (team.getPlayers().containsKey(clientID)) {
                team.getPlayers().remove(clientID);
                removedOne = true;
            }
        }
        GameRunner runner = stagedGames.get(gameID);
        runner.sendLobbyInfo();
        if (!removedOne) throw new GameException("Player to unregister not found");
        saveGame(g);
    }

    @Override
    public void removeAllGames() {
        gameRepository.deleteAll();
    }

    @Override
    public String addTradePost(String token, String gameId, String tradePostId) {
        Game game = authenticateGame(token, gameId);
        if (game == null) throw new GameException("Game authentication error");
        TradePost tradePost = locationService.findTradepostById(tradePostId);
        game.getTradePosts().add(tradePost);
        saveGame(game);
        return tradePostId;
    }

    @Override
    public void updateGame(String token, Game game) {
        Game g = authenticateGame(token, game.getId());
        if (g == null) throw new GameException("Game authentication error");
        saveGame(game);
    }

    @Override
    public Game findRunningGameById(String gameID) {
        return RunningGameServiceImpl.get(gameID).getGame();
    }

    @Override
    public Game findStagedGameById(String gameID) {
        return stagedGames.get(gameID).getGame();
    }

    @Override
    public List<Game> findAllRunningGames() {
        List<Game> games = new ArrayList<>();
        for (GameRunner gameRunner : RunningGameServiceImpl.values()) {
            games.add(gameRunner.getGame());
        }
        return games;
    }

    @Override
    public List<Game> findAllStagedGames() {
        List<Game> games = new ArrayList<>();
        for (GameRunner gameRunner : stagedGames.values()) {
            games.add(gameRunner.getGame());
        }
        return games;
    }

    @Override
    public List<Game> findAllGames() {
        return gameRepository.findAll();
    }

    @Override
    public void cancelGame(String hostToken, String id) {
        Game g = authenticateGame(hostToken, id);
        if (g == null) throw new GameException("Game not found");
        long time = System.currentTimeMillis();
        g.setEndTime(time);
        g.setStartTime(time);
        saveGame(g);
        stagedGames.remove(id);
    }

    @Override
    public void clearStagedGames() {
        stagedGames.clear();
    }

    @Override
    public boolean isStaged(String gameID) {
        return stagedGames.containsKey(gameID);
    }

    @Override
    public String registerWebApp(String gameID, String token) {
        if (gameID == null || token == null) {
            logger.warn("Method invoked with null parameter");
            throw new GameException("Method invoked with null parameter");
        }
        Account host = accountService.validateToken(token);//throws exceptions if token not valid
        //check game exists
        Game game = gameRepository.findOne(gameID);
        if (game == null) throw new GameException("404: Game not found");
        // check host hosts game
        if (!game.getHost().equals(host)) throw new GameException("Host does not host this game");
        if (stagedGames.containsKey(gameID)) {
            return stagedGames.get(gameID).getGame().getWebAppToken();
        } else if (RunningGameServiceImpl.containsKey(gameID)) {
            return RunningGameServiceImpl.get(gameID).getGame().getWebAppToken();
        } else {
            throw new GameException("Game not running or staged");
        }
    }

    @Override
    public boolean isRunning(String gameID) {
        return RunningGameServiceImpl.containsKey(gameID);
    }

    @Override
    public String getClientToken(String gameID, String clientID) {
        Player p = findPlayerByClientId(gameID, clientID);
        if (p == null) throw new GameException("Player not found");
        return UUID.nameUUIDFromBytes(clientID.getBytes()).toString();
    }

    @Override
    public AbstractSessionHandler handleNewSession(WebSocketSession session, MessageWrapper message) {
        logger.debug("New session creating");

        boolean a = stagedGames.containsKey(message.getGameID());
        boolean b = RunningGameServiceImpl.containsKey(message.getGameID());
        System.out.println(a);
        System.out.println(b);
        if ((!a && !b) || (a && b)) {
            logger.warn("Game could not be found");
            throw new GameException("Game could not be found");
        }

        GameRunner gr;
        if (stagedGames.containsKey(message.getGameID())) {
            gr = stagedGames.get(message.getGameID());
        } else {
            gr = RunningGameServiceImpl.get(message.getGameID());
        }

        return gr.handleNewSession(session, message);
    }

    @Override
    public Team findTeamByClientId(String gameId, String clientID) {
        Game g = findGameById(gameId);
        if (g == null) throw new GameException("Game could not be found");

        for (Team t : g.getTeams()) {
            if (t.getPlayers().containsKey(clientID)) {
                return t;
            }
        }
        throw new GameException("Team could not be found");
    }

    @Override
    public GameRunner findGameRunner(String gameID) {
        if (stagedGames.containsKey(gameID)) {
            return stagedGames.get(gameID);
        } else if (RunningGameServiceImpl.containsKey(gameID)) {
            return RunningGameServiceImpl.get(gameID);
        }
        return null;
    }

    @Override
    public void handlePlayerNameChange(String gameID, String clientID, String newName) {
        if (!stagedGames.containsKey(gameID)) {
            logger.warn("Game not found");
            throw new GameException("Game not found: " + gameID);
        }
        GameRunner g = stagedGames.get(gameID);
        Player p = null;
        Team t = null;
        for (Team team : g.getGame().getTeams()) {
            if (team.getPlayers().containsKey(clientID)) {
                p = team.getPlayers().get(clientID);
                t = team;
            }
        }
        if (p == null) {
            logger.warn("Player not found");
            throw new GameException("Player not found");
        }

        //todo check for duplicate names
        p.setName(newName);
        g.sendLobbyInfo();
    }

    @Override
    public void handlePlayerTeamChange(String gameID, String clientID, String newTeam) {
        if (!stagedGames.containsKey(gameID)) {
            logger.warn("Game not found");
            throw new GameException("Game not found: " + gameID);
        }
        GameRunner g = stagedGames.get(gameID);
        Player p = null;
        Team t = null;
        for (Team team : g.getGame().getTeams()) {
            if (team.getPlayers().containsKey(clientID)) {
                p = team.getPlayers().get(clientID);
                t = team;
            }
        }
        if (p == null) {
            logger.warn("Player not found");
            throw new GameException("Player not found");
        }

        Optional<Team> oNextTeam = g.getGame().getTeams().stream().filter(nt -> nt.getTeamName().equals(newTeam)).findFirst();
        Team nextTeam = null;
        if (!oNextTeam.isPresent()) {
            logger.info("Requested team not found");
        } else {
            nextTeam = oNextTeam.get();
        }

        if (nextTeam == null || nextTeam.getPlayers().size() >= g.getGame().getMaxPlayersPerTeam()) {
            int index = 0; //find index of current team
            int size = g.getGame().getTeams().size();
            for (int i = 0; i < g.getGame().getTeams().size(); i++) {
                if (!g.getGame().getTeams().get(i).getTeamName().equals(t.getTeamName())) continue;
                index = i;
                break;
            }
            for (int i = (index + 1); i != index; i = (i + 1) % size) { //loops around all teams ignoring the one we're in
                if (g.getGame().getTeams().get(i).getPlayers().size() >= g.getGame().getMaxPlayersPerTeam()) continue;
                nextTeam = g.getGame().getTeams().get(i);
                break;
            }
        }
        if (nextTeam == null) {
            throw new GameException("No suitable teams found");//to prevent nullptr further down
        }

        t.getPlayers().remove(p.getClientID());
        nextTeam.getPlayers().put(p.getClientID(), p);

        g.sendLobbyInfo();
    }

    @Override
    public void changeDuration(String token, String gameId, int minutes) {
        Game g = authenticateGame(token, gameId);
        if (g == null) throw new GameException("Game authentication error");
        if (!stagedGames.containsKey(gameId)) throw new GameException("Game could not be found");
        GameRunner runner = stagedGames.get(gameId);
        g = runner.getGame();
        g.setDuration(Duration.ofMinutes(minutes));
        saveGame(g);
    }

    @Override
    public void kickPlayer(String token, String gameId, String playerToKick) {
        Game g = authenticateGame(token, gameId);
        if (g == null) throw new GameException("Game authentication error");
        if (!stagedGames.containsKey(gameId)) throw new GameException("Game could not be found");
        stagedGames.get(gameId).kickPlayer(playerToKick);
        unregisterUser(gameId, playerToKick);
        saveGame(g);
    }

    @Override
    public GameState getGameState(String gameID) {
        if (stagedGames.containsKey(gameID)) {
            return GameState.STAGED;
        }
        if (RunningGameServiceImpl.containsKey(gameID)) {
            return GameState.RUNNING;
        }
        if (gameRepository.findGameById(gameID) != null) {
            return GameState.FINISHED;
        }
        return null;
    }

    @Override
    public synchronized Game saveGame(Game game) {
        return gameRepository.save(game);
    }

}