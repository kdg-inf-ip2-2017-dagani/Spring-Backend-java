package be.kdg.backend.services.impl;

import be.kdg.backend.dom.util.CustomColor;
import be.kdg.backend.persistence.api.ColorRepository;
import be.kdg.backend.services.api.ColorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.impl
 *
 * @author gasto
 * @version 1.0
 * @since 22/02/2017
 */
@Service("ColorService")
public class ColorServiceImpl implements ColorService {
    @Autowired
    private ColorRepository colorRepository;

    @Override
    public String addColor(CustomColor color) {
        return colorRepository.save(color).getId();
    }

    @Override
    public CustomColor findColorByColor(String color) {
        return colorRepository.findOne(Example.of(new CustomColor(color,"")));
    }

    @Override
    public List<CustomColor> findColors() {
        return colorRepository.findAll();
    }

    @Override
    public void removeColor(CustomColor color) {
        colorRepository.delete(color);
    }

    @Override
    public void removeAllColors() {
        colorRepository.deleteAll();
    }
}
