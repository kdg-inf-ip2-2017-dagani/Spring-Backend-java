package be.kdg.backend.services.impl;

import be.kdg.backend.dom.game.Item;
import be.kdg.backend.dom.location.*;
import be.kdg.backend.persistence.api.AreaLocationRepository;
import be.kdg.backend.persistence.api.PointLocationRepository;
import be.kdg.backend.services.api.LocationService;
import be.kdg.backend.services.exceptions.LocationException;
import be.kdg.backend.services.exceptions.LocationNotFoundException;
import be.kdg.backend.services.exceptions.LocationTypeMismatchException;
import be.kdg.backend.services.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by gaston
 * 10/02/2017
 */
@Service
public class LocationServiceImpl implements LocationService {
    @Autowired
    private AreaLocationRepository areaLocationRepository;
    @Autowired
    private PointLocationRepository pointLocationRepository;

    @Override
    public String addAreaLocation(String name, Point[] points, AreaType type) {
        return areaLocationRepository.insert(new AreaLocation(points, type, name)).getId();
    }

    @Override
    public List<AreaLocation> findAreas() {
       return areaLocationRepository.findAll();
    }

    @Override
    public List<AreaLocation> findAreasByType(AreaType areaType) {
        if(areaType==null)
            return areaLocationRepository.findAll();
        return areaLocationRepository.findAreaLocationByType(areaType);
    }

    @Override
    public List<PointLocation> findPointsByType(PointType pointLocationType) {
        if (pointLocationType == null)
            return pointLocationRepository.findAll();
        return pointLocationRepository.findPointLocationsByPoint_Type(pointLocationType);
    }

    @Override
    public String addPointLocation(String name, Point point, PointType pointType) {
        PointLocation pl = new PointLocation(point,pointType,name);
        return pointLocationRepository.insert(pl).getId();
    }

    @Override
    public PointLocation findPointByID(String id) {
        return pointLocationRepository.findOne(id);
    }

   @Override
    public String addTradePost(String name, Point point, List<Item> items, String flavorText) {
        TradePost tradePost = new TradePost(point, name, items, flavorText);
         if (isLegalPointLocation(tradePost)){
             return pointLocationRepository.insert(tradePost).getId();
         }
         throw new LocationException("Point Location is not legal");
    }

    private boolean isLegalPointLocation(PointLocation pointLocation) {
        double threshold = 0.5;
        Point origin = pointLocation.getPoint();
        List<PointLocation> locations = pointLocationRepository.findAll();
        for (PointLocation location : locations) {
            Point testPoint = location.getPoint();
            if (Util.haversineDistance(origin,testPoint) <= threshold){
                return false;
            }
        }
        return true;
    }

    @Override
    public TradePost findTradepostById(String id) {
        PointLocation pl = pointLocationRepository.findOne(id);
        if (pl == null) throw new LocationNotFoundException();
        if (pl.getType() != PointType.TRADE_POST) throw new LocationTypeMismatchException();
        return (TradePost) pl;
    }

 /*   @Override
    public TradePost[] findTradeposts() {
        TradePost[] locs = pointLocationRepository.findPointLocationsByType(PointType.TRADE_POST);
        if (locs == null) throw new LocationNotFoundException();
        return locs;
    }

   @Override
    public void updateTradepostByID(String id, String name, Point point, List<Item> items) {
        PointLocation pl = pointLocationRepository.findOne(id);
        if (pl == null) throw new LocationNotFoundException();
        if (pl.getType() != PointType.TRADE_POST) throw new LocationTypeMismatchException();
        TradePost tp = (TradePost) pl;
        tp.setName(name);
        tp.setPoint(point);
        tp.setItems(items);
        pointLocationRepository.save(tp);
    }

    @Override
    public void deleteTradepostByID(String id) {
        PointLocation pl = pointLocationRepository.findOne(id);
        if (pl == null) throw new LocationNotFoundException();
        if (pl.getType() != PointType.TRADE_POST) throw new LocationTypeMismatchException();
        pointLocationRepository.delete(pl);
    }*/

    @Override
    public void removeAllAreaLocations() {
        areaLocationRepository.deleteAll();
    }

    @Override
    public void removeAllPointLocations() {
        pointLocationRepository.deleteAll();
    }

    @Override
    public void removeAllLocations() {
        removeAllAreaLocations();
        removeAllPointLocations();
    }

    @Override
    public AreaLocation findAreaLocationById(String districtId) {
        return areaLocationRepository.findAreaLocationById(districtId);
    }
}
