package be.kdg.backend.services.runners.messages.subclasses;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.runners.messages.subclasses
 *
 * @author gasto
 * @version 1.0
 * @since 28/06/2017
 */
public class TradepostLock {
    private String tradepostId;
    private long unlockMoment;

    public TradepostLock() {
    }

    public TradepostLock(String tradepostId, long unlockMoment) {
        this.tradepostId = tradepostId;
        this.unlockMoment = unlockMoment;
    }

    public String getTradepostId() {
        return tradepostId;
    }

    public void setTradepostId(String tradepostId) {
        this.tradepostId = tradepostId;
    }

    public long getUnlockMoment() {
        return unlockMoment;
    }

    public void setUnlockMoment(long unlockMoment) {
        this.unlockMoment = unlockMoment;
    }
}
