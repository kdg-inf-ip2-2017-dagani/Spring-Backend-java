package be.kdg.backend.services.runners.messages;

import be.kdg.backend.dom.game.Team;
import be.kdg.backend.services.util.JsonTeam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gasto on 05/06/2017.
 */
public class LobbyUpdateMessage {
    private List<JsonTeam> teams;
    private boolean isHost;

    public LobbyUpdateMessage(List<Team> teams) {
        this.teams = new ArrayList<>(teams.size());
        for (Team team : teams) {
            this.teams.add(new JsonTeam(team));
        }
    }

    public List<JsonTeam> getTeams() {
        return teams;
    }

    public void setTeams(List<JsonTeam> teams) {
        this.teams = teams;
    }

    public boolean isHost() {
        return isHost;
    }

    public void setHost(boolean host) {
        isHost = host;
    }
}
