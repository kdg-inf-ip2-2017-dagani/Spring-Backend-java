package be.kdg.backend.services.runners.messages;


/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.runners.messages
 *
 * @author Gaston
 * @version 1.0
 * @since 22/02/2017
 */
public class HeartbeatMessage {
    private long timestamp;


    public HeartbeatMessage() {
        timestamp = System.currentTimeMillis();
    }

    public HeartbeatMessage(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }
}