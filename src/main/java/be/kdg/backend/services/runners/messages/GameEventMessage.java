package be.kdg.backend.services.runners.messages;

import be.kdg.backend.dom.game.GameEventType;

import java.util.List;
import java.util.Map;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.runners.messages
 *
 * @author gasto
 * @version 1.0
 * @since 20/02/2017
 */
public class GameEventMessage {
    private GameEventType type;
    private List<String> players;
    private double moneyTransferred;
    private Map<String,Integer> items;
    private String tradePostId;

    public String getTradePostId() {
        return tradePostId;
    }

    public Map<String, Integer> getItems() {
        return items;
    }

    public GameEventType getType() {
        return type;
    }

    public List<String> getPlayers() {
        return players;
    }

    public double getMoneyTransferred() {
        return moneyTransferred;
    }
}
