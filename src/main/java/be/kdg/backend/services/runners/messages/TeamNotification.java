package be.kdg.backend.services.runners.messages;

import be.kdg.backend.dom.game.Team;
import be.kdg.backend.dom.location.AreaLocation;
import be.kdg.backend.services.runners.messages.subclasses.TradepostLock;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 11/03/2017.
 */
public class TeamNotification {
    private List<AreaLocation> districts;
    private double treasury;
    private double bankAccount;
    private double totalPlayerMoney;
    private List<String> tradeposts;
    private List<TradepostLock> visitedTradeposts;

    public TeamNotification(List<AreaLocation> districts, double treasury, double bankAccount, double totalPlayerMoney, List<String> tradeposts, Map<String, Long> visitedTradeposts) {
        this.districts = districts;
        this.treasury = treasury;
        this.bankAccount = bankAccount;
        this.totalPlayerMoney = totalPlayerMoney;
        this.tradeposts = tradeposts;
        this.visitedTradeposts = new ArrayList<>();

        for (Map.Entry<String, Long> stringLongEntry : visitedTradeposts.entrySet()) {
            this.visitedTradeposts.add(new TradepostLock(stringLongEntry.getKey(),stringLongEntry.getValue()));
        }
    }

    public TeamNotification(Team t) {
        this(t.getDistricts(),t.getTreasury(),t.getBankAccount(),t.getTotalPlayerMoney(),t.getTradePosts(),t.getVisitedTradeposts());
    }

    public List<String> getTradeposts() {
        return tradeposts;
    }

    public void setTradeposts(List<String> tradeposts) {
        this.tradeposts = tradeposts;
    }

    public List<AreaLocation> getDistricts() {
        return districts;
    }

    public void setDistricts(List<AreaLocation> districts) {
        this.districts = districts;
    }

    public double getTreasury() {
        return treasury;
    }

    public void setTreasury(double treasury) {
        this.treasury = treasury;
    }

    public double getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(double bankAccount) {
        this.bankAccount = bankAccount;
    }

    public double getTotalPlayerMoney() {
        return totalPlayerMoney;
    }

    public void setTotalPlayerMoney(double totalPlayerMoney) {
        this.totalPlayerMoney = totalPlayerMoney;
    }

    public List<TradepostLock> getVisitedTradeposts() {
        return visitedTradeposts;
    }

    public void setVisitedTradeposts(List<TradepostLock> visitedTradeposts) {
        this.visitedTradeposts = visitedTradeposts;
    }
}
