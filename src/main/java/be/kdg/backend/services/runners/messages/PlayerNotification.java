package be.kdg.backend.services.runners.messages;

import be.kdg.backend.dom.game.Player;
import be.kdg.backend.services.runners.messages.subclasses.TradepostLock;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 13/03/2017.
 */
public class PlayerNotification {
    private List<PlayerItemCount> legalItems;
    private List<PlayerItemCount> illegalItems;
    private double money;
    private List<TradepostLock> visitedTradeposts;

    public PlayerNotification(Map<String, Integer> legalItems, Map<String, Integer> illegalItems, double money, Map<String, Long> visitedTradeposts) {
        this.legalItems = new ArrayList<>();
        this.illegalItems = new ArrayList<>();
        this.visitedTradeposts = new ArrayList<>();
        this.money = money;


        for (Map.Entry<String, Integer> stringIntegerEntry : illegalItems.entrySet()) {
            this.illegalItems.add(new PlayerItemCount(stringIntegerEntry.getKey(),stringIntegerEntry.getValue()));
        }
        for (Map.Entry<String, Integer> stringIntegerEntry : legalItems.entrySet()) {
            this.legalItems.add(new PlayerItemCount(stringIntegerEntry.getKey(),stringIntegerEntry.getValue()));
        }
        for (Map.Entry<String, Long> stringLongEntry : visitedTradeposts.entrySet()) {
            this.visitedTradeposts.add(new TradepostLock(stringLongEntry.getKey(),stringLongEntry.getValue()));
        }
    }

    public PlayerNotification(Player p) {
        this(p.getLegalItems(),p.getIllegalItems(),p.getMoney(),p.getVisitedTradeposts());
    }

    public List<PlayerItemCount> getLegalItems() {
        return legalItems;
    }

    public List<PlayerItemCount> getIllegalItems() {
        return illegalItems;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public List<TradepostLock> getVisitedTradeposts() {
        return visitedTradeposts;
    }

    public void setVisitedTradeposts(List<TradepostLock> visitedTradeposts) {
        this.visitedTradeposts = visitedTradeposts;
    }

    private class PlayerItemCount {
        private String item;
        private int count;

        public PlayerItemCount(String item, int count) {
            this.item = item;
            this.count = count;
        }
    }
}
