package be.kdg.backend.services.runners.messages;

import be.kdg.backend.dom.game.GameEventType;

/**
 * Created by Nikita on 17/03/2017.
 */
public class InfoNotification {
    private GameEventType gameEventType; //taged or robbed
    private String by;
    private String locationId;

    public InfoNotification(GameEventType gameEventType, String by, String locationId) {
        this.gameEventType = gameEventType;
        this.by = by;
        this.locationId = locationId;
    }

    public GameEventType getGameEventType() {
        return gameEventType;
    }

    public void setGameEventType(GameEventType gameEventType) {
        this.gameEventType = gameEventType;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }
}
