package be.kdg.backend.services.runners.messages;

/**
 * Created by gasto on 07/06/2017.
 */
public class ConqueringUpdate {
    private double progress;
    private boolean isConqueringTeam;
    private boolean isDraw;

    public ConqueringUpdate(double progress, boolean isConqueringTeam, boolean isDraw) {
        this.progress = progress;
        this.isConqueringTeam = isConqueringTeam;
        this.isDraw = isDraw;
    }

    public ConqueringUpdate() {
    }


    public double getProgress() {
        return progress;
    }

    public void setProgress(double progress) {
        this.progress = progress;
    }

    public boolean isConqueringTeam() {
        return isConqueringTeam;
    }

    public void setConqueringTeam(boolean conqueringTeam) {
        isConqueringTeam = conqueringTeam;
    }

    public boolean isDraw() {
        return isDraw;
    }

    public void setDraw(boolean draw) {
        isDraw = draw;
    }
}
