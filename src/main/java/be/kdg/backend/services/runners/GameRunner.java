package be.kdg.backend.services.runners;

import be.kdg.backend.controllers.websocket.AbstractSessionHandler;
import be.kdg.backend.controllers.websocket.PlayerSessionHandler;
import be.kdg.backend.controllers.websocket.WebAppSessionHandler;
import be.kdg.backend.dom.game.Game;
import be.kdg.backend.dom.game.GameState;
import be.kdg.backend.dom.game.Player;
import be.kdg.backend.dom.game.Team;
import be.kdg.backend.dom.location.Point;
import be.kdg.backend.persistence.api.GameRepository;
import be.kdg.backend.services.api.GameService;
import be.kdg.backend.services.api.RunningGameService;
import be.kdg.backend.services.exceptions.GameException;
import be.kdg.backend.services.exceptions.InvalidTokenException;
import be.kdg.backend.services.exceptions.RunnerInitialisationException;
import be.kdg.backend.services.runners.messages.*;
import be.kdg.backend.services.util.Util;
import org.apache.log4j.Logger;
import org.springframework.web.socket.WebSocketSession;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static be.kdg.StniklaasbackendApplication.GSON;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.impl
 * <p>
 * Runnable class that handles setting up the main game loop, and handles the automatic ending of the game.
 * It also handles timed events like persisting money data.
 * <p>
 * It is called when a game moves from staging to running
 *
 * @author Gaston
 * @version 1.0
 * @since 18/02/2017
 */
public class GameRunner implements Runnable {
    private static final double TAG_DISTANCE = 10;
    private static final double VIEW_DISTANCE = 50;
    private final Logger logger = Logger.getLogger(GameRunner.class);
    private final double COIN_PER_DISTRICT = 20.0;

    //############################### TIME CONSTANTS
    private final long SECOND_TEN = 10000;
    private final long SECOND_ONE = 1000;
    private final long MINUTE = 60000;
    private final long LOCATION_REFRESH = SECOND_ONE * 2;
    private final long TRADEPOST_LOCK_TIME = MINUTE * 1;

    //############################### GAME VARIABLES
    private GameService gameService;
    private RunningGameService runningGameService;
    private GameRepository gameRepository;
    private Game game;
    private ConcurrentMap<String, AbstractSessionHandler> sessionHandlers;
    private boolean stop;
    private boolean allowTag;
    private Timer timer;
    private Map<String, DistrictCapturingInstance> currentConquerings;
    private GameState gameState;

    //########################### TIME VARIABLES
    private long nextLocationPush;
    private long nextCashDrop;
    private long treasuryCloseTime;
    private long treasuryNextOpen;
    private long nextConqueringCheck;
    private long conqueringDelta;


    public GameRunner(GameService gameService, GameRepository gameRepository, Game game, RunningGameService runningGameService) {
        if (gameService == null || gameRepository == null || game == null) {
            logger.warn("Error in initialising game");
            throw new RunnerInitialisationException("Parameters can not be null");
        }
        this.gameService = gameService;
        this.runningGameService = runningGameService;
        this.gameRepository = gameRepository;
        this.game = game;
        sessionHandlers = new ConcurrentHashMap<>();
        currentConquerings = new ConcurrentHashMap<>();
        stop = false;
        allowTag = false;
        sessionHandlers.put(game.getWebAppToken(), new WebAppSessionHandler(game.getId()));
        timer = new Timer();
    }

    private void executeCashDrop() {
        List<Team> teams = game.getTeams();
        for (Team team : teams) {
            double coin = team.getTreasury();
            double drop = (team.getDistricts().size() * COIN_PER_DISTRICT);
            coin += drop;
            team.setTreasury(coin);
            pushTeamNotification(team, new TeamNotification(team.getDistricts(), team.getTreasury(), team.getBankAccount(), team.getTotalPlayerMoney(), team.getTradePosts(), team.getVisitedTradeposts()));
        }
        nextCashDrop = System.currentTimeMillis() + MINUTE;
    }

    private void HandleDistrictCapturing() {

    }

    private void broadcastLocations() {
        logger.debug("Broadcasting");
        Map<String, Point> locations = new HashMap<>();
        for (Team team : game.getTeams()) {
            Map<String, Player> players = team.getPlayers();
            for (String id : players.keySet()) {
                if (players.get(id).getLocation() == null) continue;
                locations.put(id, players.get(id).getLocation());
            }
        }
        if (locations.isEmpty()) {
            logger.warn("Game:" + game.getId() + " : No locations to broadcast");
        } else {
            BulkLocationMessage blm = new BulkLocationMessage(locations);
            for (AbstractSessionHandler sessionHandler : sessionHandlers.values()) {
                AbstractSessionHandler.SessionLevel level = sessionHandler.getLevel();
                switch (level) {
                    case HOST:
                        logger.debug("To host");
                        sessionHandler.send(GameMessageType.BULK_LOCATION, blm.toJson());
                        break;
                    case PLAYER:
                        logger.debug("to player");
                        BulkLocationMessage message = filterLocations(blm, sessionHandler);
                        if (message != null) {
                            logger.debug("sending bulk location to players");
                            sessionHandler.send(GameMessageType.BULK_LOCATION, message.toJson());
                        }
                        break;
                    case WEB:
                        logger.debug("to web");
                        sessionHandler.send(GameMessageType.BULK_LOCATION, blm.toJson());
                        break;
                    default:
                        logger.warn("Session handler without valid level detected");
                        throw new GameException("Session handler without valid level detected");
                }
            }
        }
        nextLocationPush = System.currentTimeMillis() + LOCATION_REFRESH;
    }

    private void checkConquers() {
        for (String locationId : currentConquerings.keySet()) {
            currentConquerings.get(locationId).update();
        }
    }

    private void closeTreasuries() {
        game.closeTreasuries();
        pushPlayerNotification(GameMessageType.TREASURIES_CLOSE, "");
    }

    //todo optimise
    private BulkLocationMessage filterLocations(BulkLocationMessage blm, AbstractSessionHandler sessionHandler) {
        PlayerSessionHandler psh = (PlayerSessionHandler) sessionHandler;
        Map<String, Point> locs = new HashMap<>();
        Map<String, Point> tags = new HashMap<>();
        Point location = null;
        Team playerTeam = null;
        Map<String, String> players = new HashMap<>();
        for (Team team : game.getTeams()) {
            for (Map.Entry<String, Player> entry : team.getPlayers().entrySet()) {
                players.put(entry.getKey(), team.getTeamName());
            }
            if (team.getPlayers().containsKey(psh.getPlayerID())) {
                playerTeam = team;
                location = team.getPlayers().get(psh.getPlayerID()).getLocation();
            }
        }

        // if (location == null) throw new GameException("could not filter locations: player location unknown");
        if (location != null) {
            for (String id : blm.getLocations().keySet()) {
                double distance = Util.haversineDistance(location, blm.getLocations().get(id));
                String otherTeam = players.get(id);

                if (playerTeam.getTeamName().equalsIgnoreCase(otherTeam)) {
                    if (id.equals(psh.getPlayerID())) continue;
                    locs.put(id, blm.getLocations().get(id));
                } else {
                    if (id.equals(psh.getPlayerID())) continue;
                    if (distance > VIEW_DISTANCE) continue;
                    if (distance < TAG_DISTANCE) {
                        tags.put(id, blm.getLocations().get(id));
                    } else {
                        locs.put(id, blm.getLocations().get(id));
                    }
                }
            }
            //if (locs.size() == 0) return null;
            return new BulkLocationMessage(locs, tags);
        }
        return null;
    }

    private void openTreasuries() {
        game.openTreasuries();
        treasuryCloseTime = System.currentTimeMillis() + MINUTE;
        treasuryNextOpen = System.currentTimeMillis() + (10 * MINUTE);
        pushPlayerNotification(GameMessageType.TREASURIES_OPEN, MINUTE + "ms");
    }

    /**
     * Pushes a message to all connected sessions
     *
     * @param type    message type
     * @param message message to send
     */
    private void pushNotification(GameMessageType type, String message) {
        for (AbstractSessionHandler sessionHandler : sessionHandlers.values()) {
            sessionHandler.send(type, message);
        }
    }

    /**
     * pushes a message to all players
     *
     * @param type    message type
     * @param message the message toe send
     */
    private void pushPlayerNotification(GameMessageType type, String message) {
        for (Team team : game.getTeams()) {
            for (Player player : team.getPlayers().values()) {
                if (sessionHandlers.get(player.getToken()) != null) {
                    sessionHandlers.get(player.getToken()).send(type, message);
                } else {
                    logger.warn("Player " + player.getName() + " did not connect via websocket");
                }
            }
        }
    }

    private void pushTeamNotification(Team team, GameMessageType type, String message) {
        for (Player player : team.getPlayers().values()) {
            sessionHandlers.get(player.getToken()).send(type, message);
        }
    }

    void sendConqueringUpdate(Player player, ConqueringUpdate cu) {
        String message = GSON.toJson(cu);
        if (sessionHandlers.get(player.getToken()) != null) {
            sessionHandlers.get(player.getToken()).send(GameMessageType.CONQUERING_UPDATE, message);
        } else {
            logger.warn("Player " + player.getName() + " did not connect via websocket");
        }

    }

    public void addPlayerToDistrictConquer(String teamName, Player p, String locationID) {
        if (game.getDistricts().stream().anyMatch(d -> d.getId().equals(locationID))) {
            if (currentConquerings.containsKey(locationID)) {
                currentConquerings.get(locationID).addPlayerToPoint(teamName, p);
            } else {
                currentConquerings.put(locationID, new DistrictCapturingInstance(runningGameService, locationID, this));
                currentConquerings.get(locationID).addPlayerToPoint(teamName, p);
            }
        }

        if (currentConquerings.size() > 0) {
            conqueringDelta = SECOND_ONE;
        } else {
            conqueringDelta = SECOND_TEN;
        }
    }

    public void forceStopGame() {
        stop = true;
        timer.cancel();
        timer.purge();
        if (game.getStartTime() == 0){
            game.setStartTime(System.currentTimeMillis());
        }
        game.setEndTime(System.currentTimeMillis());
        if (sessionHandlers != null) {
            for (AbstractSessionHandler session : sessionHandlers.values()) {
                session.send(GameMessageType.GAME_STOP, "");
            }
            for (AbstractSessionHandler sessionHandler : sessionHandlers.values()) {
                sessionHandler.close();
            }
        }
        sessionHandlers = null;
    }

    public Game getGame() {
        return game;
    }

    public GameState getGameState() {
        return gameState;
    }

    public double getTreasuryByDistrict(String clientToken, String districtId) {
        if (!sessionHandlers.containsKey(clientToken)) throw new InvalidTokenException();
        Team otherTeam = null;
        for (Team team : game.getTeams()) {
            if (team.getDistricts().get(0).getId().equals(districtId)) {
                otherTeam = team;
            }
        }
        if (otherTeam == null) throw new GameException("District not found");
        return otherTeam.getTreasury();
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public Team getWinningTeam() {
        double money = 0;
        Team t = game.getTeams().get(0);
        for (Team team : game.getTeams()) {
            double moneys = Util.calculateTeamMoney(team);
            if (moneys > money) {
                money = moneys;
                t = team;
            }
        }
        return t;
    }

    public void handle(WebSocketSession session, MessageWrapper messageWrapper) {
        if (!sessionHandlers.containsKey(messageWrapper.getToken())) throw new GameException("Token not accepted");
        if (sessionHandlers.get(messageWrapper.getToken()).getSession() != null && sessionHandlers.get(messageWrapper.getToken()).getSession().equals(session)) { //todo check this
            sessionHandlers.get(messageWrapper.getToken()).handle(messageWrapper);
        } else {//new client or reconnect
            handleNewSession(session, messageWrapper);
            sendLobbyInfo();
        }
    }

    public AbstractSessionHandler handleNewSession(WebSocketSession session, MessageWrapper message) {
        logger.debug("Runner handling new session");
        AbstractSessionHandler sessionHandler = null;
        if (message.getToken().equals(game.getWebAppToken())) {
            logger.debug("webapp session");
            sessionHandler = sessionHandlers.get(game.getWebAppToken());
        } else {
            logger.debug("Session is not webapp");
            for (Player player : game.getHosts()) {
                if (player.getToken().equals(message.getToken())) {
                    logger.debug("host session");
                    sessionHandler = sessionHandlers.get(message.getToken());
                }
            }
            if (sessionHandler == null) {
                for (Team team : game.getTeams()) {
                    for (Player player : team.getPlayers().values()) {
                        if (player.getToken().equals(message.getToken())) {
                            logger.debug("player session");
                            sessionHandler = sessionHandlers.get(message.getToken());
                        }
                    }
                }
            }
        }
        if (sessionHandler == null) {
            logger.warn("User not found");
            throw new GameException("User Not found");
        }
        sessionHandler.setSession(session);
        logger.debug("handling message");
        sessionHandler.handle(message);
        return sessionHandler;
    }

    public void kickPlayer(String playerToKick) {
        Player p = null;
        for (Team team : game.getTeams()) {
            if (!team.getPlayers().containsKey(playerToKick)) continue;
            p = team.getPlayers().get(playerToKick);
            break;
        }
        if (p == null) throw new GameException("Player not found");
        sessionHandlers.get(p.getToken()).send(GameMessageType.PLAYER_KICKED, "");
    }

    public void pushDistrictNotification(DistrictNotification notification) {
        for (AbstractSessionHandler sessionHandler : sessionHandlers.values()) {
            sessionHandler.send(GameMessageType.DISTRICT_NOTIFICATION, GSON.toJson(notification));
        }
    }

    public void pushInfoNotification(List<Team> teams, InfoNotification infoNotification) {
        for (Team team : teams) {
            for (Player player : team.getPlayers().values()) {
                sessionHandlers.get(player.getToken()).send(GameMessageType.INFO_NOTIFICATION, GSON.toJson(infoNotification));
            }
        }
    }

    public void pushPlayerNotification(Player player, PlayerNotification playerNotification) {
        if (sessionHandlers.get(player.getToken()) != null) {
            sessionHandlers.get(player.getToken()).send(GameMessageType.PLAYER_NOTIFICATION, GSON.toJson(playerNotification));
        } else {
            logger.warn("Player " + player.getName() + " did not connect via websocket");
        }
    }

    public void pushPlayerNotification(Player player, String message) {
        if (sessionHandlers.get(player.getToken()) != null) {
            sessionHandlers.get(player.getToken()).send(GameMessageType.PLAYER_NOTIFICATION, message);
        } else {
            logger.warn("Player " + player.getName() + " did not connect via websocket");
        }
    }

    public void pushTagNotification(Player player, String message) {
        if (sessionHandlers.get(player.getToken()) != null) {
            sessionHandlers.get(player.getToken()).send(GameMessageType.TAG_NOTIFICATION, message);
        } else {
            logger.warn("Player " + player.getName() + " did not connect via websocket");
        }
    }

    public void pushTeamNotification(Team team, TeamNotification notificationMessage) {
        for (Player player : team.getPlayers().values()) {
            if (sessionHandlers.get(player.getToken()) != null) {
                sessionHandlers.get(player.getToken()).send(GameMessageType.TEAM_NOTIFICATION, GSON.toJson(notificationMessage));
            } else {
                logger.warn("Player " + player.getName() + " did not connect via websocket");
            }
        }
    }

    public void registerHost(String token, String clientID) {
        AbstractSessionHandler sessionHandler = new PlayerSessionHandler(runningGameService, gameService, game.getId(), clientID, AbstractSessionHandler.SessionLevel.HOST);
        sessionHandlers.put(token, sessionHandler);
    }

    public void registerPlayer(String token, String clientID) {
        AbstractSessionHandler sessionHandler = new PlayerSessionHandler(runningGameService, gameService, game.getId(), clientID, AbstractSessionHandler.SessionLevel.PLAYER);
        sessionHandlers.put(token, sessionHandler);
    }

    public void removePlayerFromDistrictConquer(String teamName, Player p, String locationID) {
        if (game.getDistricts().stream().anyMatch(d -> d.getId().equals(locationID))) {
            if (currentConquerings.containsKey(locationID)) {
                currentConquerings.get(locationID).removePlayerFromPoint(teamName, p);
            }
        }

//todo clear not needed conquerings

        if (currentConquerings.size() > 0) {
            conqueringDelta = SECOND_ONE;
        } else {
            conqueringDelta = SECOND_TEN;
        }
    }

    @Override
    public void run() {
        //Game stop timer
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                allowTag = false;
                stopGame();
            }
        }, game.getEndTime() - System.currentTimeMillis());

        //Allow tag timer
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                allowTag = true;
                pushNotification(GameMessageType.TAG_PERMITTED, "");
            }
        }, MINUTE);

        openTreasuries();

        nextLocationPush = System.currentTimeMillis();
        nextCashDrop = System.currentTimeMillis() + MINUTE;

        for (AbstractSessionHandler abstractSessionHandler : sessionHandlers.values()) {
            abstractSessionHandler.send(GameMessageType.GAME_START, "");
        }
        for (Team team : game.getTeams()) {
            for (Player player : team.getPlayers().values()) {
                pushPlayerNotification(player, new PlayerNotification(player.getLegalItems(), player.getIllegalItems(), player.getMoney(), player.getVisitedTradeposts()));
            }
        }

        while (!stop) {
            if (System.currentTimeMillis() > nextLocationPush) {
                broadcastLocations();
            }
            if (System.currentTimeMillis() > nextCashDrop) {
                //every minute
                executeCashDrop();
                backupGame();
            }
            if (System.currentTimeMillis() > treasuryCloseTime && game.isTreasuriesOpen()) {
                closeTreasuries();
            }
            if (System.currentTimeMillis() > treasuryNextOpen) {
                openTreasuries();
            }
            if (System.currentTimeMillis() > nextConqueringCheck) {
                nextConqueringCheck = System.currentTimeMillis() + conqueringDelta;
                checkConquers();
            }

            for (AbstractSessionHandler sessionHandler : sessionHandlers.values()) {
                if (sessionHandler.hasQueuedMessages()) {
                    sessionHandler.send(); //to clear the buffer once buffering has started
                }
            }
        }
    }

    private void backupGame() {
        gameService.saveGame(game);
    }

    public void sendLobbyInfo() {
        LobbyUpdateMessage lobbyUpdate = new LobbyUpdateMessage(game.getTeams());
        for (Team team : game.getTeams()) {
            for (Player p : team.getPlayers().values()) {
                if (sessionHandlers.get(p.getToken()) != null) {
                    if (p.getClientID().equals(game.getHost().getCurrentDeviceId())) {
                        lobbyUpdate.setHost(true);
                    } else {
                        lobbyUpdate.setHost(false);
                    }
                    sessionHandlers.get(p.getToken()).send(GameMessageType.LOBBY_UPDATE, GSON.toJson(lobbyUpdate));
                } else {
                    logger.warn("Player " + p.getName() + " did not connect via websocket");
                }

            }
        }
    }

    public void startTradepostTimer(Team t, Player p, String locationID) {
        long unlock = System.currentTimeMillis() + TRADEPOST_LOCK_TIME;
        t.getVisitedTradeposts().put(locationID, unlock);
        p.getVisitedTradeposts().put(locationID, unlock);

        TimerTask tt = new TimerTask() {
            @Override
            public void run() {
                //unlock
                if (t.getVisitedTradeposts().containsKey(locationID)) t.getVisitedTradeposts().remove(locationID);
                if (p.getVisitedTradeposts().containsKey(locationID)) p.getVisitedTradeposts().remove(locationID);
                pushPlayerNotification(p, new PlayerNotification(p));
                pushTeamNotification(t, new TeamNotification(t));
            }
        };
        timer.schedule(tt, TRADEPOST_LOCK_TIME);
    }

    public void stopGame() {
        stop = true;
        timer.cancel();
        timer.purge();
        WinningTeamMessage winningTeamMessage = new WinningTeamMessage();

        for (Team team : game.getTeams()) {
            double score = 0.0;
            score += team.getBankAccount();
            score += team.getTreasury();
            score += team.getTotalPlayerMoney();
            WinningTeamMessage.TeamScore ts = new WinningTeamMessage.TeamScore(team.getTeamName(), score);
            winningTeamMessage.getScoreList().add(ts);
        }


        pushNotification(GameMessageType.WINNING_TEAM, GSON.toJson(winningTeamMessage));
        for (AbstractSessionHandler session : sessionHandlers.values()) {
            session.send(GameMessageType.GAME_STOP, "");
        }
        for (AbstractSessionHandler sessionHandler : sessionHandlers.values()) {
            sessionHandler.close();
        }
        gameService.closeGame(game);
        sessionHandlers = null;
        gameService.removeGameRunner(game.getId());
    }
}