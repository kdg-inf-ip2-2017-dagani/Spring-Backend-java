package be.kdg.backend.services.runners.messages;

/**
 * Created by gasto on 07/06/2017.
 */
public class ConquerMessage {
    private String LocationID;

    public ConquerMessage() {
    }

    public ConquerMessage(String locationID) {
        LocationID = locationID;
    }


    public String getLocationID() {
        return LocationID;
    }

    public void setLocationID(String locationID) {
        LocationID = locationID;
    }
}
