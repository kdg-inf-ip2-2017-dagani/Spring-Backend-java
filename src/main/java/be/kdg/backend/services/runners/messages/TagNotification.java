package be.kdg.backend.services.runners.messages;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.runners.messages
 *
 * @author gaston
 * @version 1.0
 * @since 16/03/2017
 */
public class TagNotification {
    private final boolean legalLost;
    private String taggedBy;
    private String taggedTeamName;

    public TagNotification(String taggedBy, boolean legalItemsLost, String taggedTeamName) {
        this.taggedBy = taggedBy;
        this.legalLost = legalItemsLost;
        this.taggedTeamName = taggedTeamName;
    }
}
