package be.kdg.backend.services.runners.messages;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gasto on 19/06/2017.
 */
public class WinningTeamMessage {
    private List<TeamScore> scoreList;

    public WinningTeamMessage() {
        scoreList = new ArrayList<>();
    }

    public static class TeamScore {
        private String name;
        private double score;

        public TeamScore() {
        }

        public TeamScore(String name, double score) {
            this.name = name;
            this.score = score;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getScore() {
            return score;
        }

        public void setScore(double score) {
            this.score = score;
        }
    }

    public List<TeamScore> getScoreList() {
        return scoreList;
    }

    public void setScoreList(List<TeamScore> scoreList) {
        this.scoreList = scoreList;
    }
}
