package be.kdg.backend.services.runners;

import be.kdg.backend.dom.game.GameEventType;
import be.kdg.backend.dom.game.Player;
import be.kdg.backend.services.api.RunningGameService;
import be.kdg.backend.services.runners.messages.ConqueringUpdate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gasto on 06/06/2017.
 */
public class DistrictCapturingInstance {
    private final long TotalCaptureTime = 60000;

    private RunningGameService runningGameService;

    private boolean isActive;
    private String districtId;
    private GameRunner gameRunner;
    private Map<String, List<Player>> playersPerTeam;
    private Map<String, Double> progressPctPerTeam;
    private String currentTeam;
    private long currentTeamCapturingStart;
    private boolean isDraw;

    public DistrictCapturingInstance(RunningGameService runningGameService, String districtId, GameRunner gameRunner) {
        this.runningGameService = runningGameService;
        this.districtId = districtId;
        this.gameRunner = gameRunner;
        playersPerTeam = new HashMap<>();
        progressPctPerTeam = new HashMap<>();
        isActive = false;
    }

    public void addPlayerToPoint(String teamName, Player p) {
        isActive = true;
        if (!playersPerTeam.containsKey(teamName)) {
            playersPerTeam.put(teamName, new ArrayList<>());
            playersPerTeam.get(teamName).add(p);
            progressPctPerTeam.put(teamName, 0.0);
        } else {
            playersPerTeam.get(teamName).add(p);
        }
        checkCurrentCapturingTeam();
    }

    public void removePlayerFromPoint(String teamName, Player p) {
        if (playersPerTeam.containsKey(teamName)) {
            playersPerTeam.get(teamName).remove(p);
            //todo check if no players -> if so clear this instance
            checkCurrentCapturingTeam();
        }
    }

    private void checkCurrentCapturingTeam() {
        //todo replace this method with the update as well?
        if (currentTeam != null) {
            //Update the currently capturing team status
            long currTime = System.currentTimeMillis();
            long timePassed = currTime - currentTeamCapturingStart;
            double pctPassed = ((double) timePassed) / ((double) TotalCaptureTime);
            pctPassed += progressPctPerTeam.get(currentTeam);
            progressPctPerTeam.put(currentTeam, pctPassed);
            currentTeamCapturingStart = currTime;

            //Detect if a new team should be updating
            String newTeam = currentTeam;
            int newMax = playersPerTeam.get(currentTeam).size();

            //get theoretical maximum
            for (String team : playersPerTeam.keySet()) {
                if (playersPerTeam.get(team).size() > playersPerTeam.get(newTeam).size()) {
                    newTeam = team;
                    newMax = playersPerTeam.get(team).size();
                }
            }

            //check for doubles
            byte doubles = 0;
            for (List<Player> players : playersPerTeam.values()) {
                if (newMax == players.size()) {
                    doubles++;
                }
            }
            boolean newDraw = doubles > 1;

            if (newDraw != isDraw) {
                update();
            } else if (!newTeam.equals(currentTeam)) {
                update();
            }
        } else {
            //Detect if a new team should be updating
            int newMax = 0;
            String newTeam = null;

            //get theoretical maximum
            for (String team : playersPerTeam.keySet()) {
                if (playersPerTeam.get(team).size() > newMax) {
                    newTeam = team;
                    newMax = playersPerTeam.get(team).size();
                }
            }

            //check for doubles
            byte doubles = 0;
            for (List<Player> players : playersPerTeam.values()) {
                if (newMax == players.size()) {
                    doubles++;
                }
            }

            boolean newDraw = doubles > 1;

            if (newDraw != isDraw) {
                update();
            } else if (newTeam == null && currentTeam != null) {
                update();
            } else if (newTeam != null && currentTeam == null) {
                update();
            } else if (currentTeam != null && newTeam != null && !newTeam.equals(currentTeam)) {
                update();
            }

        }
    }

    void update() {
        if (currentTeam != null) {
            //Update the currently capturing team status
            long currTime = System.currentTimeMillis();
            long timePassed = currTime - currentTeamCapturingStart;
            double pctPassed = ((double) timePassed) / ((double) TotalCaptureTime);
            pctPassed += progressPctPerTeam.get(currentTeam);
            progressPctPerTeam.put(currentTeam, pctPassed);
            currentTeamCapturingStart = currTime;

            //Detect if a new team should be updating
            String newTeam = currentTeam;
            int newMax = playersPerTeam.get(currentTeam).size();

            //get theoretical maximum
            for (String team : playersPerTeam.keySet()) {
                if (playersPerTeam.get(team).size() > playersPerTeam.get(newTeam).size()) {
                    newTeam = team;
                    newMax = playersPerTeam.get(team).size();
                }
            }

            //check for doubles
            byte doubles = 0;
            for (List<Player> players : playersPerTeam.values()) {
                if (newMax == players.size()) {
                    doubles++;
                }
            }
            isDraw = doubles > 1;

            if (isDraw) {
                currentTeam = null;
            } else {
                currentTeam = newTeam;
                currentTeamCapturingStart = System.currentTimeMillis();
            }
        } else {
            //Detect if a new team should be updating
            int newMax = 0;
            String newTeam = null;

            //get theoretical maximum
            for (String team : playersPerTeam.keySet()) {
                if (playersPerTeam.get(team).size() > newMax) {
                    newTeam = team;
                    newMax = playersPerTeam.get(team).size();
                }
            }

            //check for doubles
            byte doubles = 0;
            for (List<Player> players : playersPerTeam.values()) {
                if (newMax == players.size()) {
                    doubles++;
                }
            }

            isDraw = doubles > 1;
            if (isDraw) {
                currentTeam = null;
            } else {
                currentTeam = newTeam;
                currentTeamCapturingStart = System.currentTimeMillis();
            }
        }


        if (currentTeam != null && progressPctPerTeam.get(currentTeam) >= 1.0 && playersPerTeam.get(currentTeam).size() >0) {
            //todo conquering is done, delete this instance and lock the district for x amount of time
            //gameRunner.pushDistrictNotification(new DistrictNotification(currentTeam, districtId)); handled by runninggameservice
            runningGameService.postEvent(gameRunner.getGame().getId(),playersPerTeam.get(currentTeam).get(0).getClientID(), GameEventType.DISTRICT_CONQUERED,new ArrayList<>(),0,new HashMap<>(),districtId);
        } else {
            for (String team : playersPerTeam.keySet()) {
                boolean isCapturing = false;
                if (currentTeam != null && currentTeam.equals(team)) {
                    isCapturing = true;
                }

                for (Player player : playersPerTeam.get(team)) {
                    ConqueringUpdate cu = new ConqueringUpdate(progressPctPerTeam.get(team), isCapturing, isDraw);
                    gameRunner.sendConqueringUpdate(player, cu);
                }
            }
        }

    }
}
