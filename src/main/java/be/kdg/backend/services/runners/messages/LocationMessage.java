package be.kdg.backend.services.runners.messages;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.runners.messages
 *
 * @author Gaston
 * @version 1.0
 * @since 20/02/2017
 */
public class LocationMessage {
    private double latitude;
    private double longitude;



    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
