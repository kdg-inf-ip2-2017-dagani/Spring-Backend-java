package be.kdg.backend.services.runners.messages;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.runners.messages
 *
 * @author Gaston
 * @version 1.0
 * @since 13/03/2017
 */
public class ErrorExceptionMessage<T extends Exception> {
    private String exceptionClass;
    private String message;
    private String cause;

    public ErrorExceptionMessage(T e) {
        exceptionClass = e.getClass().getName();

        if (e.getMessage() != null) {
            message = e.getMessage();
        }

        if (e.getCause() != null) {
            cause = e.getCause().toString();
        }
    }
}
