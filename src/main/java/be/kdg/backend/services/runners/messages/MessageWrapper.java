package be.kdg.backend.services.runners.messages;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.runners.messages
 *
 * @author Gaston
 * @version 1.0
 * @since 22/02/2017
 */
public class MessageWrapper {
    private GameMessageType messageType;
    private String token;
    private String message;
    private String gameID;
    private String clientID;

    public MessageWrapper(GameMessageType messageType, String message, String gameID) {
        this.messageType = messageType;
        this.message = message;
        this.gameID = gameID;
    }

    public MessageWrapper(GameMessageType messageType, String message, String gameID, String clientID) {
        this(messageType, message, gameID);
        this.clientID = clientID;
    }

    public GameMessageType getMessageType() {
        return messageType;
    }

    public String getMessage() {
        return message;
    }

    public String getGameID() {
        return gameID;
    }

    public String getClientID() {
        return clientID;
    }

    public String getToken() {
        return token;
    }
}