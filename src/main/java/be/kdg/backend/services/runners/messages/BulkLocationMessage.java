package be.kdg.backend.services.runners.messages;

import be.kdg.backend.dom.location.Point;
import be.kdg.backend.services.exceptions.GameException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Map;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.runners.messages
 *
 * @author gaston
 * @version 1.0
 * @since 24/02/2017
 */
public class BulkLocationMessage {
    private Map<String, Point> locations;
    private Map<String, Point> taggable;

    public BulkLocationMessage(Map<String, Point> locations) {
        this.locations = locations;
        taggable = null;
    }

    public BulkLocationMessage(Map<String, Point> locations, Map<String, Point> taggable) {
        this.locations = locations;
        this.taggable = taggable;
    }

    public Map<String, Point> getLocations() {
        return locations;
    }


    public String toJson() {
        if (taggable != null){
            return taggingBulk();
        } else {
            return normalBulk();
        }
    }

    private String taggingBulk() {
        JSONObject bulklocation = new JSONObject();
        try {
            JSONArray locs = new JSONArray();
            for (Map.Entry<String, Point> entry : locations.entrySet()) {
                JSONObject point = new JSONObject();
                point.put("lat", entry.getValue().getLatitude());
                point.put("lng", entry.getValue().getLongitude());
                JSONObject location = new JSONObject();
                location.put("key", entry.getKey());
                location.put("location", point);
                location.put("taggable", false);
                locs.put(location);
            }
            for (Map.Entry<String, Point> entry : taggable.entrySet()) {
                JSONObject point = new JSONObject();
                point.put("lat", entry.getValue().getLatitude());
                point.put("lng", entry.getValue().getLongitude());
                JSONObject location = new JSONObject();
                location.put("key", entry.getKey());
                location.put("location", point);
                location.put("taggable", true);
                locs.put(location);
            }

            bulklocation.put("locations", locs);
        } catch (Exception e) {
            throw new GameException("Error while creating json",e);
        }
        return bulklocation.toString();
    }

    private String normalBulk() {
        JSONObject bulklocation = new JSONObject();
        try {
            JSONArray jsonArray = new JSONArray();
            for (Map.Entry<String, Point> entry : locations.entrySet()) {
                JSONObject point = new JSONObject();
                point.put("lat", entry.getValue().getLatitude());
                point.put("lng", entry.getValue().getLongitude());
                JSONObject location = new JSONObject();
                location.put("key", entry.getKey());
                location.put("location", point);
                jsonArray.put(location);
            }

            bulklocation.put("locations", jsonArray);
        } catch (Exception e) {
            throw new GameException("Error while creating json",e);
        }
        return bulklocation.toString();
    }
}
