package be.kdg.backend.services.runners.messages;

/**
 * Created by Nikita on 16/03/2017.
 */
public class DistrictNotification {
    private String teamName;
    private String districtId;

    public DistrictNotification(String teamName, String districtId) {
        this.teamName = teamName;
        this.districtId = districtId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }
}
