package be.kdg.backend.services.runners.messages;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.runners.messages
 *
 * @author Gaston Lhoest
 * @version 1.0
 * @since 24/02/2017
 */
public enum GameMessageType {
    EVENT,
    HEARTBEAT,
    BULK_LOCATION,
    LOCATION,
    WEB_CONNECT,
    GAME_START,
    GAME_STOP,
    TEAM_NOTIFICATION,
    PLAYER_NOTIFICATION,
    ERROR_EXCEPTION,
    TAG_PERMITTED,
    TREASURIES_OPEN,
    TREASURIES_CLOSE,
    WINNING_TEAM,
    DISTRICT_NOTIFICATION,
    TAG_NOTIFICATION,
    INFO_NOTIFICATION,
    CONQUERING_UPDATE,
    CONQUERING_START,
    CONQUERING_END,
    LOBBY_UPDATE,
    PLAYER_UPDATE_NAME,
    PLAYER_UPDATE_TEAM,
    PLAYER_KICKED
}
