package be.kdg.backend.dom.account;

/**
 * Created by gaston
 * 9/02/2017
 */
public enum AccessLevel {
    ADMIN, HOST
}
