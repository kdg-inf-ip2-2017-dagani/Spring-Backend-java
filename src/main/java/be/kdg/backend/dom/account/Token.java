package be.kdg.backend.dom.account;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by gaston
 * 10/02/2017
 */
public class Token {
    private static final int VALID_SECONDS = 1800;
    private String token;
    private LocalDateTime validUntil;

    public Token(Account account) {
        token = encrypt("" + account.getId() + account.getUsername() + System.currentTimeMillis());
        validUntil = LocalDateTime.now().plusSeconds(VALID_SECONDS);
    }


    public Token() {
    }

    private String encrypt(String s) {
        Random random = new Random();
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char store = chars[i];
            int pos2 = random.nextInt(chars.length);
            chars[i] = chars[pos2];
            chars[pos2] = store;
        }

        return new String(chars);
    }

    public String getToken() {
        return token;
    }

    public LocalDateTime getValidUntil() {
        return validUntil;
    }

    public void refresh() {
        validUntil = LocalDateTime.now().plusSeconds(VALID_SECONDS);
    }
}
