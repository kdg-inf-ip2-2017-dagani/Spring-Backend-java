package be.kdg.backend.dom.account;

import org.springframework.data.annotation.Id;

/**
 * Created by Nikita on 9/02/2017.
 */
public class Account {
    @Id
    private String id;
    private String username;
    private String password;
    private String emailAddress;
    private Token token;
    private AccessLevel accessLevel;
    private String currentDeviceId;

    public Account(String currentDeviceId) {
        this.currentDeviceId = currentDeviceId;
        password = currentDeviceId.hashCode() + "";
        accessLevel = AccessLevel.HOST;
    }

    public Account(String username, String password, String emailAddress, AccessLevel accessLevel) {
        this.username = username;
        this.password = password;
        this.emailAddress = emailAddress;
        this.accessLevel = accessLevel;
    }

    public Account() {
    }

    public AccessLevel getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(AccessLevel accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getCurrentDeviceId() {
        return currentDeviceId;
    }

    public void setCurrentDeviceId(String currentDeviceId) {
        this.currentDeviceId = currentDeviceId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (emailAddress != null ? emailAddress.hashCode() : 0);
        result = 31 * result + (accessLevel != null ? accessLevel.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account)) return false;

        Account user = (Account) o;

        if (id != null ? !id.equals(user.id) : user.id != null) return false;
        if (username != null ? !username.equals(user.username) : user.username != null) return false;
        if (emailAddress != null ? !emailAddress.equals(user.emailAddress) : user.emailAddress != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null)
            return false;
        return accessLevel == user.accessLevel;

    }
}
