package be.kdg.backend.dom.location;

/**
 * Created by gaston
 * 9/02/2017
 */
public class Point {
    private double latitude;
    private double longitude;
    private PointType type;
    private int index;

    public Point() {
    }

    public Point(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        type = PointType.COORDINATE;
    }

    public Point(double latitude, double longitude, int index) {
        this(latitude, longitude);
        this.index = index;
    }

    public Point(double latitude, double longitude, PointType type) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.type = type;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getIndex() {
        return index;
    }

    public void setType(PointType type) {
        this.type = type;
    }

    public PointType getType() {
        return type;
    }
}
