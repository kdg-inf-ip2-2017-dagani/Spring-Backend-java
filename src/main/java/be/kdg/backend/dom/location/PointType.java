package be.kdg.backend.dom.location;

/**
 * Created by gaston
 * 9/02/2017
 */
public enum PointType {
    BANK, TRADE_POST, TREASURE, COORDINATE
}
