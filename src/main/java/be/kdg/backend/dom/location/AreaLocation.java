package be.kdg.backend.dom.location;

import org.springframework.data.annotation.Id;

/**
 * Created by Nikita on 9/02/2017.
 */
public class AreaLocation {
    @Id
    private String id;
    private Point[] points;
    private AreaType type;
    private String name;

    public AreaLocation(Point[] points, AreaType type, String name) {
        this.points = points;
        this.type = type;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public Point[] getPoints() {
        return points;
    }

    public AreaType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public void setType(AreaType type) {
        this.type = type;
    }
}
