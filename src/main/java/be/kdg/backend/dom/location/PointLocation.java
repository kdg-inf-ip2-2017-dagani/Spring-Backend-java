package be.kdg.backend.dom.location;

import org.springframework.data.annotation.Id;

/**
 * Created by Nikita on 9/02/2017.
 */
public class PointLocation {
    @Id
    private String id;
    private Point point;
    private String name;

    public PointLocation() {
    }

    public PointLocation(Point point, PointType type, String name) {
        this.point = point;
        this.point.setType(type);
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public Point getPoint() {
        return point;
    }

    public PointType getType() {
        return point.getType();
    }

    public String getName() {
        return name;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public void setName(String name) {
        this.name = name;
    }
}
