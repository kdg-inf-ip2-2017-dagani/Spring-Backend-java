package be.kdg.backend.dom.location;

import be.kdg.backend.dom.game.Item;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by gaston
 * 13/02/2017
 */
@Document(collection = "pointLocation")
public class TradePost extends PointLocation {
    private List<Item> items;
    private String flavorText;

    public TradePost() {
        super();
    }

    public TradePost(Point point, String name, List<Item> items, String flavorText) {
        super(point, PointType.TRADE_POST, name);
        this.items = items;
        this.flavorText = flavorText;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getFlavorText() {
        return flavorText;
    }

    public void setFlavorText(String flavorText) {
        this.flavorText = flavorText;
    }
}
