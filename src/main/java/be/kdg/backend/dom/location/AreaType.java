package be.kdg.backend.dom.location;

/**
 * Created by gaston
 * 9/02/2017
 */
public enum AreaType {
    //SAFE_ZONE,
    DISTRICT_A, DISTRICT_B, MARKET
}
