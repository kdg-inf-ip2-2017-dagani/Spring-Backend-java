package be.kdg.backend.dom.game;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.dom.game
 *
 * @author Gaston
 * @version 1.0
 * @since 18/02/2017
 */
public class Item {
    private String name;
    private double legalSales;
    private double illegalSales;
    private double legalPurchase;
    private double illegalPurchase;

    public Item(String name, double legalSales, double illegalSales, double legalPurchase, double illegalPurchase) {
        this.name = name;
        this.legalSales = legalSales;
        this.illegalSales = illegalSales;
        this.legalPurchase = legalPurchase;
        this.illegalPurchase = illegalPurchase;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLegalSales() {
        return legalSales;
    }

    public void setLegalSales(double legalSales) {
        this.legalSales = legalSales;
    }

    public double getIllegalSales() {
        return illegalSales;
    }

    public void setIllegalSales(double illegalSales) {
        this.illegalSales = illegalSales;
    }

    public double getLegalPurchase() {
        return legalPurchase;
    }

    public void setLegalPurchase(double legalPurchase) {
        this.legalPurchase = legalPurchase;
    }

    public double getIllegalPurchase() {
        return illegalPurchase;
    }

    public void setIllegalPurchase(double illegalPurchase) {
        this.illegalPurchase = illegalPurchase;
    }
}
