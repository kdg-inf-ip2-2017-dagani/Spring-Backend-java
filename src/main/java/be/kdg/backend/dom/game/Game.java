package be.kdg.backend.dom.game;

import be.kdg.backend.dom.account.Account;
import be.kdg.backend.dom.location.AreaLocation;
import be.kdg.backend.dom.location.PointLocation;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Nikita on 9/02/2017.
 */

public class Game {
    private static final long DURATION_MINUTES = 60;


    @Id
    private String id;
    @DBRef
    private Account host;
    private String roomName;
    @DBRef
    private List<AreaLocation> districts;
    @DBRef
    private List<AreaLocation> markets;
    @DBRef
    private List<PointLocation> tradePosts;
    @DBRef
    private List<PointLocation> banks;

    private List<Team> teams;
    private List<Player> hosts;
    private List<GameEvent> events;
    private String playerPassword;
    private String hostPassword;
    private String webAppToken;
    private Duration duration;
    private boolean treasuriesOpen;
    private long startTime;
    private long endTime;
    private int maxPlayersPerTeam;
    private int maxTeams;
    private int restarts;

    /**
     * to initialize game without template
     *
     * @param host
     * @param roomName
     * @param maxPlayersPerTeam
     * @param maxTeams
     * @param password
     */
    public Game(Account host, String roomName, int maxPlayersPerTeam, int maxTeams, String password) {
        this();
        this.host = host;
        this.roomName = roomName;
        this.maxPlayersPerTeam = maxPlayersPerTeam;
        this.maxTeams = maxTeams;
        this.playerPassword = password;
        this.hostPassword = password + "HOST";
        for (int i = 0; i < maxTeams; i++) {
            teams.add(new Team("Team " + (i + 1)));
        }
    }

    public Game(Game template, Account host, String password, String roomName) {
        this();
        this.host = host;
        this.roomName = roomName;
        this.hostPassword = password + "HOST";
        this.playerPassword = password;
        districts = template.districts;
        markets = template.markets;
        tradePosts = template.tradePosts;
        banks = template.banks;
        maxPlayersPerTeam = template.maxPlayersPerTeam;
        maxTeams = template.maxTeams;
        teams = template.teams;
        duration = template.duration;
    }

    public Game() {
        super();
        duration = Duration.ofMinutes(DURATION_MINUTES);
        startTime = 0;
        endTime = 0;
        hostPassword = "";
        playerPassword = "";
        districts = new ArrayList<>();
        markets = new ArrayList<>();
        tradePosts = new ArrayList<>();
        teams = new ArrayList<>();
        events = new ArrayList<>();
        hosts = new ArrayList<>();
        webAppToken = UUID.randomUUID().toString();
        treasuriesOpen = false;
    }

    public void closeTreasuries() {
        treasuriesOpen = false;
    }

    public List<PointLocation> getBanks() {
        return banks;
    }

    public void setBanks(List<PointLocation> banks) {
        this.banks = banks;
    }

    public List<AreaLocation> getDistricts() {
        return districts;
    }

    public void setDistricts(List<AreaLocation> districts) {
        this.districts = districts;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public List<GameEvent> getEvents() {
        return events;
    }

    public Account getHost() {
        return host;
    }

    public void setHost(Account host) {
        this.host = host;
    }

    public int getRestarts() {
        return restarts;
    }

    public void setRestarts(int restarts) {
        this.restarts = restarts;
    }

    public String getHostPassword() {
        return hostPassword;
    }

    public void setHostPassword(String hostPassword) {
        this.hostPassword = hostPassword;
    }

    public List<Player> getHosts() {
        return hosts;
    }

    public void setHosts(List<Player> hosts) {
        this.hosts = hosts;
    }

    public String getId() {
        return id;
    }

    public List<AreaLocation> getMarkets() {
        return markets;
    }

    public int getMaxPlayersPerTeam() {
        return maxPlayersPerTeam;
    }

    public void setMaxPlayersPerTeam(int maxPlayersPerTeam) {
        this.maxPlayersPerTeam = maxPlayersPerTeam;
    }

    public int getMaxTeams() {
        return maxTeams;
    }

    public void setMaxTeams(int maxTeams) {
        this.maxTeams = maxTeams;
    }

    public String getPlayerPassword() {
        return playerPassword;
    }

    public void setPlayerPassword(String playerPassword) {
        this.playerPassword = playerPassword;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public List<PointLocation> getTradePosts() {
        return tradePosts;
    }

    public void setTradePosts(List<PointLocation> tradePosts) {
        this.tradePosts = tradePosts;
    }

    public String getWebAppToken() {
        return webAppToken;
    }

    public boolean isTreasuriesOpen() {
        return treasuriesOpen;
    }

    public void openTreasuries() {
        treasuriesOpen = true;
    }
}
