package be.kdg.backend.dom.game;

import be.kdg.backend.dom.location.AreaLocation;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gaston
 * 9/02/2017
 */
public class Team {
    private static final double INITIAL_COIN = 1000.0;
    @DBRef
    private List<AreaLocation> districts;
    private String teamName;
    private Map<String,Player> players;
    private List<String> tradePosts;
    private double treasury;
    private double bankAccount;
    private String customColor;
    @Transient
    private Map<String,Long> visitedTradeposts;

    public Team() {
        bankAccount = 0;
        treasury = 0;
        treasury = INITIAL_COIN;
        players = new HashMap<>(0);
        districts = new ArrayList<>(0);
        tradePosts = new ArrayList<>(0);
        visitedTradeposts = new HashMap<>();
    }

    public Team(String teamName) {
        this();
        this.teamName = teamName;
    }

    public Team(String teamName, AreaLocation startDistrict,String customColor) {
        this(teamName);
        districts.add(startDistrict);
        this.customColor=customColor;
    }

    public void setTradePosts(List<String> tradePosts) {
        this.tradePosts = tradePosts;
    }

    public List<String> getTradePosts() {
        return tradePosts;
    }

    public List<AreaLocation> getDistricts() {
        return districts;
    }

    public String getTeamName() {
        return teamName;
    }

    public Map<String, Player> getPlayers() {
        return players;
    }

    public double getTreasury() {
        return treasury;
    }

    public void setTreasury(double treasury) {
        this.treasury = treasury;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public double getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(double bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getCustomColor() {
        return customColor;
    }

    public void setCustomColor(String customColor) {
        this.customColor = customColor;
    }

    public double getTotalPlayerMoney() {
        double playerMoney = 0;
        for (Player player : players.values()) {
            playerMoney+=player.getMoney();
        }
        return playerMoney;
    }

    public Map<String, Long> getVisitedTradeposts() {
        return visitedTradeposts;
    }

    public void setVisitedTradeposts(Map<String, Long> visitedTradeposts) {
        this.visitedTradeposts = visitedTradeposts;
    }
}
