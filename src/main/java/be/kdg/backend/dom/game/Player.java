package be.kdg.backend.dom.game;

import be.kdg.backend.dom.location.Point;
import be.kdg.backend.services.exceptions.GameException;
import org.springframework.data.annotation.Transient;

import java.util.*;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.dom.game
 *
 * @author Gaston
 * @version 1.0
 * @since 18/02/2017
 */
public class Player {
    private final int INITIAL_MONEY = 300;

    private String name;
    @Transient
    private Point location;
    private double money;
    private String clientID;
    //private List<Item> legalItems;
    //private List<Item> illegalItems;
    private Map<String,Integer> legalItems;
    private Map<String,Integer> illegalItems;

    @Transient
    private String token;
    @Transient
    private List<String> taggedByTeams;
    @Transient
    private Map<String,Long> visitedTradeposts;


    public Player(String clientID, String name) {
        this.clientID = clientID;
        this.name = name;
        illegalItems=new HashMap<>();
        legalItems=new HashMap<>();
        visitedTradeposts = new HashMap<>();
        //legalItems =new ArrayList<>();
        //illegalItems =new ArrayList<>();
        token = UUID.randomUUID().toString();
        taggedByTeams=new ArrayList<>();
        money = INITIAL_MONEY;
    }

    public Map<String, Integer> getLegalItems() {
        return legalItems;
    }

    public void setLegalItems(Map<String, Integer> legalItems) {
        this.legalItems = legalItems;
    }

    public Map<String, Integer> getIllegalItems() {
        return illegalItems;
    }

    public void setIllegalItems(Map<String, Integer> illegalItems) {
        this.illegalItems = illegalItems;
    }

    public String getName() {
        return name;
    }

    public Point getLocation() {
        return location;
    }

    public double getMoney() {
        return money;
    }

    public String getClientID() {
        return clientID;
    }

    public void setLocation(Point location) {
        this.location = location;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getToken() {
        return token;
    }

    public List<String> getTaggedByTeams() {
        return taggedByTeams;
    }

    public void setTaggedByTeams(List<String> taggedByTeams) {
        this.taggedByTeams = taggedByTeams;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        return getClientID().equals(player.getClientID());
    }

    @Override
    public int hashCode() {
        return getClientID().hashCode();
    }

    public Map<String, Long> getVisitedTradeposts() {
        return visitedTradeposts;
    }

    public void setVisitedTradeposts(Map<String, Long> visitedTradeposts) {
        this.visitedTradeposts = visitedTradeposts;
    }
}
