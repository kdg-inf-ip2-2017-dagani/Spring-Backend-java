package be.kdg.backend.dom.game;

import java.util.List;
import java.util.Map;

/**
 * Created by gasto
 * 9/02/2017
 */
public class GameEvent {
    private long timestamp;
    private GameEventType type;
    private List<String> involvedPlayers;
    private List<String> involvedTeams;
    private double moneyTransferred;
    private String initiatingPlayer;
    private String initiatingTeam;
    private String locationID;
    private Map<String,Integer> itemTransferred;

    public GameEvent(long timestamp, GameEventType type, List<String> involvedPlayers, List<String> involvedTeams, double moneyTransferred, String initiatingPlayer, String initiatingTeam, String locationID, Map<String,Integer> itemTransferred) {
        this.timestamp = timestamp;
        this.type = type;
        this.involvedPlayers = involvedPlayers;
        this.involvedTeams = involvedTeams;
        this.moneyTransferred = moneyTransferred;
        this.initiatingPlayer = initiatingPlayer;
        this.initiatingTeam = initiatingTeam;
        this.locationID = locationID;
        this.itemTransferred = itemTransferred;
    }
}
