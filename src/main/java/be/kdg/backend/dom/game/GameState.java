package be.kdg.backend.dom.game;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.services.runners
 *
 * @author Gaston Lhoest
 * @version 1.0
 * @since 08/08/2017
 */
public enum GameState {
    STAGED,RUNNING,FINISHED
}
