package be.kdg.backend.dom.util;

import org.springframework.data.annotation.Id;

/**
 * Project: stniklaasbackend
 * Package: be.kdg.backend.dom.util
 *
 * @author gaston
 * @version 1.0
 * @since 22/02/2017
 */
public class CustomColor {
    @Id
    private String id;
    private String color;
    private String name;

    public CustomColor(String color, String name) {
        this.color = color;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
