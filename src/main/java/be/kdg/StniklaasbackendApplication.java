package be.kdg;

import be.kdg.backend.Seed;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@SpringBootApplication
public class StniklaasbackendApplication implements CommandLineRunner {
    public static final Gson GSON = new Gson();
    public static final boolean PERF_LOG = false;
    //public static LineAppender PERF_APPENDER;
    private final Logger logger = Logger.getLogger(StniklaasbackendApplication.class);
    @Autowired
    private Seed seed;

    public static void main(String[] args) {
        SpringApplication.run(StniklaasbackendApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {

        try {
            Process p = Runtime.getRuntime().exec("keytool -genkey -alias bookmarks -keyalg RSA -keystore src/main/resources/tomcat.keystore");

        } catch (Exception e){
            logger.warn("Exception while generating SSL key");
        }
        /*
        if (PERF_LOG) {
            try {
                PERF_APPENDER = IOFactory.createLineAppender(
                        "stNiklaas_perf",
                        "perf_" +
                                LocalDate.now().getDayOfMonth() + "_" +
                                LocalDate.now().getMonthValue() + "_" +
                                LocalDate.now().getYear() + "_" +
                                LocalTime.now().format(DateTimeFormatter.ofPattern("H_m_s")) +
                                ".log",
                        false);
            } catch (Exception e) {
                logger.warn("Failed to initialize performance logger");
            }
        }
        */
        seed.init();
    }
}
